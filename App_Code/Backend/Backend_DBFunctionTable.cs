﻿using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.ComponentModel;

public class Backend_DBFunctionTable
{
    #region operatorgroup/edit  listbox功能使用
    /// <summary>
    /// 取得單個群組的未選擇「功能」資訊
    /// </summary>
    /// <param name="oid"></param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataTable get_unselect_funlist(string oid)
    {
        Database db = DBHelper.GetDatabase();
        string sql = @"
            -- 6. 最後再把目前「群組可用的所有功能編號列表」，在「所有功能列表中做排除(留意Not)」，即可得知「單個群組的未選擇功能資訊」
            SELECT 
	            * 
            FROM 
	            [functiontable]
            WHERE 
	            [Serno] Not IN (
		            -- 5. 如以一來就可以看到：目前「群組」可用的所有功能編號列表，已經變成rows了，沒有逗號了
		            SELECT 
			            [Serno] 
		            FROM 
			            [functiontable] 
		            AS 
			            [functiontable_1]
		            WHERE 
			            -- 4. 接著比對功能表中的所有功能編號，若目前這個 [,編號,] 出現在「目前使用中的功能編號串」中，也就是如下的文字出現位置大於0的時候，才過濾這個功能編號出來
			            CHARINDEX(
				            -- 3. 這樣無論如何都可以找到類似 [,某功能編號,] 這樣的字串了，不會在前或後缺少逗點
				            (
					            ',' + 
					            CAST([Serno] as varchar(10)) + 
					            ','
				            ),
				            -- 2. 因為「目前使用中的功能編號串」的頭尾可能有逗號，所以不失一般性，還是在前後各加一個逗號
				            (
					            ',' +  
					            (
						            -- 1. 顯示「單個群組的未選擇功能資訊」依據是，利用目前「群組」的「群組id」取得「目前使用中的功能編號串」(逗號分隔)
						            SELECT 
							            [FunList] 
						            FROM 
							            [funoperatorgroup] 
						            WHERE 
							            [Serno]=[[oid]]
						            -- ;
					            ) + 
					            ','
				            )
			            ) > 0 
		            -- ;
	            )
            ;
        ";
        sql = sql.Replace("[[oid]]", oid);
        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "oid", DbType.Int32, Convert.ToInt32(oid));
        DataTable dt = new DataTable();
        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }
        return dt;
    }

    /// <summary>
    /// 取得單個群組的已選擇「功能」資訊
    /// </summary>
    /// <param name="oid"></param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataTable get_selected_funlist(string oid)
    {
        Database db = DBHelper.GetDatabase();
        string sql = @"
            -- 6. 最後再把目前「群組可用的所有功能編號列表」，在「所有功能列表中做過濾(沒有Not)」，即可得知「單個群組的已選擇功能資訊」
            SELECT 
	            * 
            FROM 
	            [functiontable]
            WHERE 
	            [Serno] IN (
		            -- 5. 如以一來就可以看到：目前「群組」可用的所有功能編號列表，已經變成rows了，沒有逗號了
		            SELECT 
			            [Serno] 
		            FROM 
			            [functiontable] 
		            AS 
			            [functiontable_1]
		            WHERE 
			            -- 4. 接著比對功能表中的所有功能編號，若目前這個 [,編號,] 出現在「目前使用中的功能編號串」中，也就是如下的文字出現位置大於0的時候，才過濾這個功能編號出來
			            CHARINDEX(
				            -- 3. 這樣無論如何都可以找到類似 [,某功能編號,] 這樣的字串了，不會在前或後缺少逗點
				            (
					            ',' + 
					            CAST([Serno] as varchar(10)) + 
					            ',' 
				            ),
				            -- 2. 因為「目前使用中的功能編號串」的頭尾可能有逗號，所以不失一般性，還是在前後各加一個逗號
				            (
					            ',' + 
					            (
						            -- 1. 顯示「單個群組的已選擇功能資訊」依據是，利用目前「群組」的「群組id」取得「目前使用中的功能編號串」(逗號分隔)
						            SELECT 
							            [FunList] 
						            FROM 
							            [funoperatorgroup] 
						            WHERE 
							            [Serno]=[[oid]]
						            -- ;
					            ) + 
					            ','
				            )
			            ) > 0 
		            -- ;
	            )
            ;
        ";
        sql = sql.Replace("[[oid]]", oid);
        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "oid", DbType.Int32, Convert.ToInt32(oid));
        DataTable dt = new DataTable();
        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }
        return dt;
    }

    /// <summary>
    /// 取得單個群組的未選擇「對象」資訊
    /// </summary>
    /// <param name="oid"></param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataTable get_unselect_usrlist(string oid)
    {
        Database db = DBHelper.GetDatabase();
        string sql = @"
            -- 6. 最後再把目前「群組適用的所有對象編號列表」，在「所有對象(使用者)列表中做排除(留意Not)」，即可得知「單個群組的未選擇對象資訊」
            SELECT 
	            * 
            FROM 
	            [funoperator]
            WHERE 
	            [Serno] Not IN (
		            -- 5. 如以一來就可以看到：目前「群組」適用的所有對象編號列表，已經變成rows了，沒有逗號了
		            SELECT 
			            [Serno] 
		            FROM 
			            [funoperator] 
		            AS 
			            [Funoperator_1]
		            WHERE 
			            -- 4. 接著比對使用者表中的所有對象(使用者)編號，若目前這個 [,編號,] 出現在「目前適用中的對象編號串」中，也就是如下的文字出現位置大於0的時候，才過濾這個對象編號出來
			            CHARINDEX(
				            -- 3. 這樣無論如何都可以找到類似 [,某對象編號,] 這樣的字串了，不會在前或後缺少逗點
				            (
					            ',' + 
					            CAST([Serno] as varchar(10)) + 
					            ','
				            ),
				            -- 2. 因為「目前適用中的對象編號串」的頭尾可能有逗號，所以不失一般性，還是在前後各加一個逗號
				            (
					            ',' + 
					            (
						            -- 1. 顯示「單個群組的未選擇對象資訊」依據是，利用目前「群組」的「群組id」取得「目前適用中的對象編號串」(逗號分隔)
						            SELECT 
							            [UsrList] 
						            FROM 
							            [funoperatorgroup] 
						            WHERE 
							            [Serno]=[[oid]]
						            -- ;
					            ) + 
					            ','
				            )
			            ) > 0 
		            -- ;
	            )
            ;
        ";
        sql = sql.Replace("[[oid]]", oid);
        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "oid", DbType.Int32, Convert.ToInt32(oid));
        DataTable dt = new DataTable();
        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }
        return dt;
    }

    /// <summary>
    /// 取得單個群組的已選擇「對象」資訊
    /// </summary>
    /// <param name="oid"></param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataTable get_selected_usrlist(string oid)
    {
        Database db = DBHelper.GetDatabase();
        string sql = @"
            -- 6. 最後再把目前「群組適用的所有對象編號列表」，在「所有對象(使用者)列表中做過濾(沒有Not)」，即可得知「單個群組的已選擇對象資訊」
            SELECT 
	            * 
            FROM 
	            [funoperator]
            WHERE 
	            [Serno] IN (
		            -- 5. 如以一來就可以看到：目前「群組」適用的所有對象編號列表，已經變成rows了，沒有逗號了
		            SELECT 
			            [Serno] 
		            FROM 
			            [funoperator] 
		            AS 
			            [Funoperator_1]
		            WHERE 
			            -- 4. 接著比對使用者表中的所有對象(使用者)編號，若目前這個 [,編號,] 出現在「目前適用中的對象編號串」中，也就是如下的文字出現位置大於0的時候，才過濾這個對象編號出來
			            CHARINDEX(
				            -- 3. 這樣無論如何都可以找到類似 [,某對象編號,] 這樣的字串了，不會在前或後缺少逗點
				            (
					            ',' + 
					            CAST([Serno] as varchar(10)) + 
					            ',' 
				            ),
				            -- 2. 因為「目前適用中的對象編號串」的頭尾可能有逗號，所以不失一般性，還是在前後各加一個逗號
				            (
					            ',' + 
					            (
						            -- 1. 顯示「單個群組的已選擇對象資訊」依據是，利用目前「群組」的「群組id」取得「目前適用中的對象編號串」(逗號分隔)
						            SELECT 
							            [UsrList] 
						            FROM 
							            [funoperatorgroup] 
						            WHERE 
							            [Serno]=[[oid]]
						            -- ;
					            ) + 
					            ','
				            )
			            ) > 0 
		            -- ;
	            )
            ;
        ";
        sql = sql.Replace("[[oid]]", oid);
        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "oid", DbType.Int32, Convert.ToInt32(oid));
        DataTable dt = new DataTable();
        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }
        return dt;
    }
    #endregion
}