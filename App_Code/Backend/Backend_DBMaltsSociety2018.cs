﻿using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Web;
using System.ComponentModel;

public class Backend_DBMaltsSociety2018
{
    #region backend/maltssociety/list
    /// <summary>
    /// 取得列表
    /// </summary>
    /// <param name="startrows"></param>
    /// <param name="maxrows"></param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataTable GetList(int startrows, int maxrows, string qSession, string qGivenName, string qEmail, string sDate, string eDate,string qSort)
    {
        Database db = DBHelper.GetDatabase();

        string sql = string.Empty;
        string conditionSQL = @"SELECT  ROW_NUMBER() OVER ";

        if (qSort.Equals("desc"))
            conditionSQL += " (ORDER BY CDate desc) AS RowNum, * FROM [guest]";
        else
            conditionSQL += " (ORDER BY CDate asc) AS RowNum, * FROM [guest]";


        if (!string.IsNullOrEmpty(sDate))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [CDate] >= @sDate ";
        }

        if (!string.IsNullOrEmpty(eDate))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [CDate] < @eDate ";
        }

        if (!string.IsNullOrEmpty(qEmail))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [Email] like '%' + @qEmail + '%'";
        }

        if (!string.IsNullOrEmpty(qGivenName))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [GivenName] like '%' + @qGivenName + '%' OR [FamilyName] like '%' + @qGivenName + '%' OR [FamilyName]+[GivenName] like '%' + @qGivenName + '%'";
        }

        if (!string.IsNullOrEmpty(qSession))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [Session]=@qSession";
        }

        sql += String.Format(@"SELECT *
                 FROM ({0})
                 AS NewTable
                 WHERE RowNum >= {1} AND RowNum <= {2}", conditionSQL, (startrows + 1).ToString(), (maxrows + startrows).ToString());

        sql += " order by RowNum asc";

        DbCommand dbComm = db.GetSqlStringCommand(sql);

        if (!string.IsNullOrEmpty(sDate))
        {
            db.AddInParameter(dbComm, "sDate", DbType.String, Convert.ToString(sDate));
        };
        if (!string.IsNullOrEmpty(eDate))
        {
            //結束日多加一天
            string newEndDate = Convert.ToDateTime(eDate).AddDays(1).ToString("yyyy/MM/dd");

            db.AddInParameter(dbComm, "eDate", DbType.String, Convert.ToString(newEndDate));
        };

        if (!string.IsNullOrEmpty(qEmail))
            db.AddInParameter(dbComm, "qEmail", DbType.String, Convert.ToString("%" + qEmail + "%"));

        if (!string.IsNullOrEmpty(qSession))
            db.AddInParameter(dbComm, "qSession", DbType.String, Convert.ToString(qSession));

        if (!string.IsNullOrEmpty(qGivenName))
            db.AddInParameter(dbComm, "qGivenName", DbType.String, Convert.ToString("%" + qGivenName + "%"));

        if (!string.IsNullOrEmpty(qSort))
            db.AddInParameter(dbComm, "qSort", DbType.String, Convert.ToString(qSort));


        DataTable dt = new DataTable();
        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return dt;
    }

    /// <summary>
    /// 取得Select總筆數
    /// </summary>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCounts(string qSession,string qGivenName, string qEmail, string sDate, string eDate, string qSort)
    {
        Database db = DBHelper.GetDatabase();

        string conditionSQL = "SELECT Count(1) FROM [guest] ";

        if (!string.IsNullOrEmpty(sDate))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [CDate] >= @sDate ";
        }

        if (!string.IsNullOrEmpty(eDate))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [CDate] < @eDate ";
        }

        if (!string.IsNullOrEmpty(qEmail))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [Email] like '%' + @qEmail + '%'";
        }

        if (!string.IsNullOrEmpty(qGivenName))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [GivenName] like '%' + @qGivenName + '%' OR [FamilyName] like '%' + @qGivenName + '%' OR [FamilyName]+[GivenName] like '%' + @qGivenName + '%'";
        }

        if (!string.IsNullOrEmpty(qSession))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [Session]=@qSession";
        }


        DbCommand dbComm = db.GetSqlStringCommand(conditionSQL);

        if (!string.IsNullOrEmpty(sDate))
        {
            db.AddInParameter(dbComm, "sDate", DbType.String, Convert.ToString(sDate));
        };
        if (!string.IsNullOrEmpty(eDate))
        {
            //結束日多加一天
            string newEndDate = Convert.ToDateTime(eDate).AddDays(1).ToString("yyyy/MM/dd");

            db.AddInParameter(dbComm, "eDate", DbType.String, Convert.ToString(newEndDate));
        };

        if (!string.IsNullOrEmpty(qEmail))
            db.AddInParameter(dbComm, "qEmail", DbType.String, Convert.ToString("%" + qEmail + "%"));

        if (!string.IsNullOrEmpty(qSession))
            db.AddInParameter(dbComm, "qSession", DbType.String, Convert.ToString(qSession));

        if (!string.IsNullOrEmpty(qGivenName))
            db.AddInParameter(dbComm, "qGivenName", DbType.String, Convert.ToString("%" + qGivenName + "%"));

        if (!string.IsNullOrEmpty(qSort))
            db.AddInParameter(dbComm, "qSort", DbType.String, Convert.ToString(qSort));

        DataTable dt = new DataTable();

        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return Convert.ToInt32(dt.Rows[0][0]);
    }

	public static DataTable Export()
	{
		Database db = DBHelper.GetDatabase();

		string sql = string.Empty;

		sql = "SELECT * FROM guest ORDER BY Serno ASC";

		DbCommand dbComm = db.GetSqlStringCommand(sql);


		DataTable dt = new DataTable();
		try
		{
			dt = db.ExecuteDataSet(dbComm).Tables[0];
		}
		catch (Exception ex)
		{
			throw (ex);
		}

		return dt;
	}
	
	public static DataTable ExportGenerally()
	{
		Database db = DBHelper.GetDatabase();

		string sql = string.Empty;

		sql = @"
	    SELECT [Serno]
          ,[FamilyName]
          ,[GivenName]
          ,[Sex]
          ,[Birthday]
          ,[Phone]
          ,[Email]
          ,[Income]
          ,[Job]
          ,[Session]
          ,[Referrer]
          ,[utm_source]
          ,[utm_medium]
          ,[utm_campaign]
          ,[ip]
          ,[CDate]
          ,[MDate]
        FROM [demo_mortlach].[dbo].[guest]
        ORDER BY [Serno] ASC";

		DbCommand dbComm = db.GetSqlStringCommand(sql);


		DataTable dt = new DataTable();
		try
		{
			dt = db.ExecuteDataSet(dbComm).Tables[0];
		}
		catch (Exception ex)
		{
			throw (ex);
		}

		return dt;
	}

    public static DataTable GetSessionList()
    {
        Database db = DBHelper.GetDatabase();

        string sql = string.Empty;
        string conditionSQL = "SELECT [Session] FROM [dbo].[guest] GROUP BY Session";

        sql += conditionSQL;


        DbCommand dbComm = db.GetSqlStringCommand(sql);

        DataTable dt = new DataTable();
        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return dt;
    }
    public static DataTable GetExcel(string qSession, string qGivenName, string qEmail, string sDate, string eDate,string qSort)
    {
        Database db = DBHelper.GetDatabase();

        string sql = string.Empty;
        string conditionSQL = @"SELECT [Serno] as #
                              ,[FamilyName] as 姓
                              ,[GivenName] as 名
                              ,[Sex] as 性別
                              ,[Birthday] as 生日
                              ,[Phone] as 手機號碼
                              ,[Email]
                              ,[Income] as 收入
                              ,[Job] as 職業
                              ,[Session] as 時段
                              ,[Referrer] as 推薦人
                              ,[utm_source]
                              ,[utm_medium]
                              ,[utm_campaign]
                              ,[CDate] as 建立日期
                            FROM [guest]";

        if (!string.IsNullOrEmpty(sDate))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [CDate] >= @sDate ";
        }

        if (!string.IsNullOrEmpty(eDate))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [CDate] < @eDate ";
        }

        if (!string.IsNullOrEmpty(qEmail))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [Email] like '%' + @qEmail + '%'";
        }

        if (!string.IsNullOrEmpty(qGivenName))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [GivenName] like '%' + @qGivenName + '%' OR [FamilyName] like '%' + @qGivenName + '%' OR [FamilyName]+[GivenName] like '%' + @qGivenName + '%'";
        }

        if (!string.IsNullOrEmpty(qSession))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [Session]=@qSession";
        }

        sql += conditionSQL;

        //sql += " order by [Serno] asc";

        if (qSort.Equals("desc"))
        {
            sql += " order by [CDate] desc";
        }
        else
        {
            sql += " order by [CDate] asc";
        }

        DbCommand dbComm = db.GetSqlStringCommand(sql);

        if (!string.IsNullOrEmpty(sDate))
        {
            db.AddInParameter(dbComm, "sDate", DbType.String, Convert.ToString(sDate));
        };
        if (!string.IsNullOrEmpty(eDate))
        {
            //結束日多加一天
            string newEndDate = Convert.ToDateTime(eDate).AddDays(1).ToString("yyyy/MM/dd");

            db.AddInParameter(dbComm, "eDate", DbType.String, Convert.ToString(newEndDate));
        };

        if (!string.IsNullOrEmpty(qEmail))
            db.AddInParameter(dbComm, "qEmail", DbType.String, Convert.ToString("%" + qEmail + "%"));

        if (!string.IsNullOrEmpty(qSession))
            db.AddInParameter(dbComm, "qSession", DbType.String, Convert.ToString(qSession));

        if (!string.IsNullOrEmpty(qGivenName))
            db.AddInParameter(dbComm, "qGivenName", DbType.String, Convert.ToString("%" + qGivenName + "%"));


        DataTable dt = new DataTable();
        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return dt;
    }

    public static DataTable GetClientExcel(string qSession, string qGivenName, string qEmail, string sDate, string eDate, string qSort)
    {
        Database db = DBHelper.GetDatabase();

        string sql = string.Empty;
        string conditionSQL = @"SELECT 
                                ([FamilyName] + [GivenName]) as 名稱
                                ,[FamilyName] as 姓氏
                                ,[GivenName] as 名字
                                ,convert(varchar, [Birthday], 111) as 生日
                                ,REPLACE(REPLACE([Sex],'M','男'),'F','女') as 性別
                                ,[Phone] as 行動電話
                                ,[Email] as 電子郵件
                                ,REPLACE(REPLACE([Income],' - ','-'),'以上',' 以上') as 月收入
                                ,[Job] as 職業
                                ,'' as 常喝的威士忌品牌
                                ,'' as 威士忌飲用頻次
                                ,'' as 是否願意收到活動訊息
                                ,[utm_campaign] as 活動名稱
                                ,[utm_source] as 活動來源
                                ,[utm_medium] as 活動媒介
                                ,'' as 活動內容
                                ,convert(varchar, [CDate], 120) as 報名時間
                                ,'' as 場次編號
                                ,'' as 場次名稱
                                ,'是' as 是否為匯入資料
                                ,'' as 報名狀況
                                ,'' as 繳費狀況
                                ,'' as 入席狀況
                            FROM [guest]";

        if (!string.IsNullOrEmpty(sDate))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [CDate] >= @sDate ";
        }

        if (!string.IsNullOrEmpty(eDate))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [CDate] < @eDate ";
        }

        if (!string.IsNullOrEmpty(qEmail))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [Email] like '%' + @qEmail + '%'";
        }

        if (!string.IsNullOrEmpty(qGivenName))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [GivenName] like '%' + @qGivenName + '%' OR [FamilyName] like '%' + @qGivenName + '%' OR [FamilyName]+[GivenName] like '%' + @qGivenName + '%'";
        }

        if (!string.IsNullOrEmpty(qSession))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [Session]=@qSession";
        }

        sql += conditionSQL;

        //sql += " order by [Serno] asc";

        if (qSort.Equals("desc"))
        {
            sql += " order by [CDate] desc";
        }
        else
        {
            sql += " order by [CDate] asc";
        }

        DbCommand dbComm = db.GetSqlStringCommand(sql);

        if (!string.IsNullOrEmpty(sDate))
        {
            db.AddInParameter(dbComm, "sDate", DbType.String, Convert.ToString(sDate));
        };
        if (!string.IsNullOrEmpty(eDate))
        {
            //結束日多加一天
            string newEndDate = Convert.ToDateTime(eDate).AddDays(1).ToString("yyyy/MM/dd");

            db.AddInParameter(dbComm, "eDate", DbType.String, Convert.ToString(newEndDate));
        };

        if (!string.IsNullOrEmpty(qEmail))
            db.AddInParameter(dbComm, "qEmail", DbType.String, Convert.ToString("%" + qEmail + "%"));

        if (!string.IsNullOrEmpty(qSession))
            db.AddInParameter(dbComm, "qSession", DbType.String, Convert.ToString(qSession));

        if (!string.IsNullOrEmpty(qGivenName))
            db.AddInParameter(dbComm, "qGivenName", DbType.String, Convert.ToString("%" + qGivenName + "%"));


        DataTable dt = new DataTable();
        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return dt;
    }

    #endregion
}