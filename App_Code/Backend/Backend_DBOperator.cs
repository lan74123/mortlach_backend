﻿using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Web;
using System.ComponentModel;

public class Backend_DBOperator
{
    #region backend/operator/list
    /// <summary>
    /// 取得列表
    /// </summary>
    /// <param name="name"></param>
    /// <param name="valid"></param>
    /// <param name="startrows"></param>
    /// <param name="maxrows"></param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataTable GetList(string name, string valid, int startrows, int maxrows)
    {
        Database db = DBHelper.GetDatabase();

        string sql = string.Empty;
        string conditionSQL = "SELECT  ROW_NUMBER() OVER (ORDER BY Serno desc) AS RowNum, * FROM [funoperator] ";

        if (!string.IsNullOrEmpty(name))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [name] like '%' + @Name + '%'";
        }
        if (!string.IsNullOrEmpty(valid))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [valid]=@Valid";
        }

        sql += String.Format(@"SELECT *
                 FROM ({0})
                 AS NewTable
                 WHERE RowNum >= {1} AND RowNum <= {2}", conditionSQL, (startrows + 1).ToString(), (maxrows + startrows).ToString());

        sql += " order by Serno desc";

        DbCommand dbComm = db.GetSqlStringCommand(sql);

        if (!string.IsNullOrEmpty(valid)) db.AddInParameter(dbComm, "Valid", DbType.String, Convert.ToString(valid));
        if (!string.IsNullOrEmpty(name)) db.AddInParameter(dbComm, "Name", DbType.String, Convert.ToString("%" + name + "%"));
        

        DataTable dt = new DataTable();
        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return dt;
    }

    /// <summary>
    /// 取得Select總筆數
    /// </summary>
    /// <param name="name"></param>
    /// <param name="valid"></param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCounts(string name, string valid)
    {
        Database db = DBHelper.GetDatabase();

        string conditionSQL = "SELECT Count(1) FROM [funoperator] ";

        if (!string.IsNullOrEmpty(name))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [name] like '%' + @Name + '%'";
        }
        if (!string.IsNullOrEmpty(valid))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [valid]=@Valid";
        }

        DbCommand dbComm = db.GetSqlStringCommand(conditionSQL);

        if (!string.IsNullOrEmpty(name))
        {
            db.AddInParameter(dbComm, "Name", DbType.String, Convert.ToString("%" + name + "%"));
        }
        if (!string.IsNullOrEmpty(valid))
        {
            db.AddInParameter(dbComm, "Valid", DbType.String, Convert.ToString(valid));
        }
        


        DataTable dt = new DataTable();

        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return Convert.ToInt32(dt.Rows[0][0]);
    }

    /// <summary>
    /// operatorlist.aspx 更新有效狀態
    /// </summary>
    /// <param name="serno"></param>
    [DataObjectMethod(DataObjectMethodType.Update)]
    public static void UpdateValid(int serno)
    {
        Database db = DBHelper.GetDatabase();
        string sql = "update [funoperator] set [valid]=@valid,mdate=@mdate,oid=@oid where serno=@serno";

        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "serno", DbType.Int32, serno);
        db.AddInParameter(dbComm, "valid", DbType.String, 0);
        db.AddInParameter(dbComm, "mdate", DbType.DateTime, DateTime.Now);
        db.AddInParameter(dbComm, "oid", DbType.Int32, BackendBasePage.GetUserSerno());
        try
        {
            db.ExecuteNonQuery(dbComm);
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }

    #endregion

    #region backend/operator/edit
    /// <summary>
    /// operatoredit.aspx 取得詳細資訊
    /// </summary>
    /// <param name="serno"></param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataTable GetListBySerno(string serno)
    {
        Database db = DBHelper.GetDatabase();

        string sql = "select * from [funoperator]";

        if (!string.IsNullOrEmpty(serno))
        {
            sql += DBHelper.GetSqlWhereOrAnd(sql) + " [serno]=@serno";
        }

        DbCommand dbComm = db.GetSqlStringCommand(sql);
        if (!string.IsNullOrEmpty(serno))
        {
            db.AddInParameter(dbComm, "serno", DbType.Int32, Convert.ToInt32(serno));
        }
        
        DataTable dt = new DataTable();
        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return dt;
    }


    /// <summary>
    /// 新增
    /// </summary>
    /// <param name="name"></param>
    /// <param name="username"></param>
    /// <param name="password"></param>
    /// <param name="valid"></param>
    [DataObjectMethod(DataObjectMethodType.Insert)]
    public static void Insert(string name, string username, string password, string valid)
    {
        //處理預設值不能為Null的問題
        name = (name == null ? "" : name);
        username = (username == null ? "" : username);
        password = (password == null ? "" : password);

        Database db = DBHelper.GetDatabase();
        string sql = "insert into [funoperator] ([name],username,password,valid,CDate,MDate,oid)" + " values (@name,@username,@password,@valid,@CDate,@MDate,@oid)";

        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "name", DbType.String, name);
        db.AddInParameter(dbComm, "username", DbType.String, username);
        db.AddInParameter(dbComm, "password", DbType.String, password);
        db.AddInParameter(dbComm, "CDate", DbType.DateTime, DateTime.Now);
        db.AddInParameter(dbComm, "MDate", DbType.DateTime, DateTime.Now);
        db.AddInParameter(dbComm, "valid", DbType.Int32, Convert.ToInt32(valid));
        db.AddInParameter(dbComm, "oid", DbType.Int32, BackendBasePage.GetUserSerno());
        try
        {
            db.ExecuteNonQuery(dbComm);
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }

    /// <summary>
    /// 修改
    /// </summary>
    /// <param name="serno"></param>
    /// <param name="name"></param>
    /// <param name="password"></param>
    /// <param name="valid"></param>
    [DataObjectMethod(DataObjectMethodType.Update)]
    public static void Update(int serno, string name, string password, string valid)
    {
        Database db = DBHelper.GetDatabase();
        string sql = "update [funoperator] set [name]=@name,password=@password,valid=@valid,mdate=@mdate,oid=@oid where serno=@serno";

        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "serno", DbType.Int32, serno);
        db.AddInParameter(dbComm, "name", DbType.String, name);
        db.AddInParameter(dbComm, "password", DbType.String, password);
        db.AddInParameter(dbComm, "valid", DbType.Int32, Convert.ToInt32(valid));
        db.AddInParameter(dbComm, "mdate", DbType.DateTime, DateTime.Now);
        db.AddInParameter(dbComm, "oid", DbType.Int32, BackendBasePage.GetUserSerno());

        try
        {
            db.ExecuteNonQuery(dbComm);
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }

    /// <summary>
    /// 刪除
    /// </summary>
    /// <param name="serno"></param>
    [DataObjectMethod(DataObjectMethodType.Delete)]
    public static void Delete(int serno)
    {
        Database db = DBHelper.GetDatabase();
        string sql = "delete from [funoperator] where serno=@serno";

        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "serno", DbType.Int32, serno);
        try
        {
            db.ExecuteNonQuery(dbComm);
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }

    #endregion

    #region backend/operator/password
    /// <summary>
    /// 更新密碼
    /// </summary>
    /// <param name="_password"></param>
    public void UpdatePassword(string _password)
    {
        Database db = DBHelper.GetDatabase();
        string sql = "update [funoperator] set [password]=@password,mdate=@mdate,oid=@oid where serno=@serno";

        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "serno", DbType.Int32, BackendBasePage.GetUserSerno() );
        db.AddInParameter(dbComm, "password", DbType.String, _password);
        db.AddInParameter(dbComm, "mdate", DbType.DateTime, DateTime.Now);
        db.AddInParameter(dbComm, "oid", DbType.Int32, BackendBasePage.GetUserSerno());
        try
        {
            db.ExecuteNonQuery(dbComm);
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }
    #endregion

    #region Common Block (共用方法)

    /// <summary>
    /// 取得使用者名字
    /// </summary>
    /// <param name="_serno"></param>
    /// <returns></returns>
    public static string GetNameBySerno(string _serno)
    {
        string returnValue = string.Empty;

        if (!string.IsNullOrEmpty(_serno))
        {
            Database db = DatabaseFactory.CreateDatabase("ConnString");
            string sql = "select name from [funoperator] where [serno]=@serno";
            DbCommand dbComm = db.GetSqlStringCommand(sql);
            db.AddInParameter(dbComm, "serno", DbType.Int32, Convert.ToInt32(_serno));
            DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];

            if (dt.Rows.Count > 0)
            {
                returnValue = dt.Rows[0]["name"].ToString();
            }
        }

        return returnValue;
    }

    /// <summary>
    /// 取得使用者帳號
    /// </summary>
    /// <param name="_serno"></param>
    /// <returns></returns>
    public string GetAccountBySerno(string _serno)
    {
        string returnValue = string.Empty;

        if (!string.IsNullOrEmpty(_serno))
        {
            Database db = DatabaseFactory.CreateDatabase("ConnString");
            string sql = "select Username from [funoperator] where [serno]=@serno";
            DbCommand dbComm = db.GetSqlStringCommand(sql);
            db.AddInParameter(dbComm, "serno", DbType.Int32, Convert.ToInt32(_serno));
            DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];
            if (dt.Rows.Count > 0)
            {
                returnValue = dt.Rows[0]["Username"].ToString();
            }
        }

        return returnValue;
    }

    /// <summary>
    /// 取得使用者Email
    /// </summary>
    /// <param name="_serno"></param>
    /// <returns></returns>
    public string GetEmailBySerno(string _serno)
    {
        string returnValue = string.Empty;

        if (!string.IsNullOrEmpty(_serno))
        {
            Database db = DatabaseFactory.CreateDatabase("ConnString");
            string sql = "select Email from [funoperator] where [serno]=@serno";
            DbCommand dbComm = db.GetSqlStringCommand(sql);
            db.AddInParameter(dbComm, "serno", DbType.Int32, Convert.ToInt32(_serno));
            DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];

            if (dt.Rows.Count > 0)
            {
                returnValue = dt.Rows[0]["Email"].ToString();
            }
        }

        return returnValue;
    }

    /// <summary>
    /// 判斷是否帳號已經存在了
    /// </summary>
    /// <param name="_username"></param>
    /// <param name="_serno">空值或0代表新增</param>
    /// <returns></returns>
    public bool IsExistAccount(string _username, string _serno)
    {
        bool returnValue = true;

        if (!string.IsNullOrEmpty(_username))
        {
            Database db = DatabaseFactory.CreateDatabase("ConnString");
            string sql = "select username from [funoperator] where [username]=@username";

            if (string.IsNullOrEmpty(_serno) || _serno=="0")
            {
                //新增
            }
            else
            {
                //修改
                sql += " and serno<>@serno";
            }

            DbCommand dbComm = db.GetSqlStringCommand(sql);
            db.AddInParameter(dbComm, "username", DbType.String, _username);
            if (!string.IsNullOrEmpty(_serno)) db.AddInParameter(dbComm, "serno", DbType.Int32, Convert.ToInt32(_serno));
            DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];

            if (dt.Rows.Count == 0)
            {
                returnValue = false;
            }
        }

        return returnValue;
    }

    /// <summary>
    /// 是否為有效
    /// </summary>
    /// <param name="_serno"></param>
    /// <returns></returns>
    public bool IsValid(string _serno)
    {
        Database db = DatabaseFactory.CreateDatabase("ConnString");
        string sql = "select valid from [funoperator] where [serno]=@serno and valid=1";
        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "serno", DbType.Int32, Convert.ToInt32(_serno));
        DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];
        if (dt.Rows.Count > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// 驗證使用者
    /// </summary>
    /// <param name="_account"></param>
    /// <param name="_password"></param>
    /// <param name="_sysmsg"></param>
    /// <param name="_operator"></param>
    /// <returns></returns>
    public static bool VerifyUser(string _account, string _password, out string _sysmsg, out COperator _operator)
    {
        bool returnValue = false;
        _sysmsg = string.Empty;
        _operator = new COperator() { Serno = 0, Name = string.Empty };

        Database db = DBHelper.GetDatabase();
        string sql = "select * from funoperator where username=@username";
        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "username", DbType.String, _account);

        try
        {
            DataSet ds = db.ExecuteDataSet(dbComm);
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count <= 0)
            {
                _sysmsg = "錯誤!!『帳號』不正確!!";
            }
            else
            {
                DataRow dr = dt.Rows[0];
                string pwd = ValidateHelper.DecryptString(dr["password"].ToString());

                if (_password != pwd.Trim())
                {
                    _sysmsg = "Error!! Password is not correct!!";
                }
                else
                {
                    if (dr["valid"].ToString() == "0")
                    {
                        _sysmsg = "注意!! 此帳號已被停用!!";
                    }
                    else
                    {
                        _operator.Serno = Convert.ToInt32(dr["Serno"].ToString());
                        _operator.Name = dr["Name"].ToString();
                        returnValue = true;
                    }
                }
            }
        }
        catch { }

        return returnValue;
    }
    #endregion

}

/// <summary>
/// 操作人員
/// </summary>
public class COperator
{
    public int Serno { get; set; }
    public string Name { get; set; }
}