﻿using Microsoft.VisualBasic;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Web;
using System.ComponentModel;

public class Backend_DBOperatorGroup
{
    #region backend/operatorgroup/list
    /// <summary>
    /// 取得列表
    /// </summary>
    /// <param name="name"></param>
    /// <param name="valid"></param>
    /// <param name="startrows"></param>
    /// <param name="maxrows"></param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataTable GetList(string name, string valid, int startrows, int maxrows)
    {
        Database db = DBHelper.GetDatabase();

        string sql = string.Empty;
        string conditionSQL = "SELECT  ROW_NUMBER() OVER (ORDER BY Serno desc) AS RowNum, * FROM [funoperatorgroup] ";

        if (!string.IsNullOrEmpty(name))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [name] like '%' + @Name + '%'";
        }
        if (!string.IsNullOrEmpty(valid))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [valid]=@Valid";
        }

        sql += String.Format(@"SELECT *
                 FROM ({0})
                 AS NewTable
                 WHERE RowNum >= {1} AND RowNum <= {2}", conditionSQL, (startrows + 1).ToString(), (maxrows + startrows).ToString());

        sql += " order by Serno desc";

        DbCommand dbComm = db.GetSqlStringCommand(sql);

        if (!string.IsNullOrEmpty(valid)) db.AddInParameter(dbComm, "Valid", DbType.String, Convert.ToString(valid));
        if (!string.IsNullOrEmpty(name)) db.AddInParameter(dbComm, "Name", DbType.String, Convert.ToString("%" + name + "%"));


        DataTable dt = new DataTable();
        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return dt;
    }

    /// <summary>
    /// 取得Select總筆數
    /// </summary>
    /// <param name="name"></param>
    /// <param name="valid"></param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static int GetCounts(string name, string valid)
    {
        Database db = DBHelper.GetDatabase();

        string conditionSQL = "SELECT Count(1) FROM [funoperatorgroup] ";

        if (!string.IsNullOrEmpty(name))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [name] like '%' + @Name + '%'";
        }
        if (!string.IsNullOrEmpty(valid))
        {
            conditionSQL += DBHelper.GetSqlWhereOrAnd(conditionSQL) + " [valid]=@Valid";
        }

        DbCommand dbComm = db.GetSqlStringCommand(conditionSQL);

        if (!string.IsNullOrEmpty(name))
        {
            db.AddInParameter(dbComm, "Name", DbType.String, Convert.ToString("%" + name + "%"));
        }
        if (!string.IsNullOrEmpty(valid))
        {
            db.AddInParameter(dbComm, "Valid", DbType.String, Convert.ToString(valid));
        }



        DataTable dt = new DataTable();

        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return Convert.ToInt32(dt.Rows[0][0]);
    }

    /// <summary>
    /// //切換有無效
    /// </summary>
    /// <param name="serno"></param>
    [DataObjectMethod(DataObjectMethodType.Update)]
    public static void UpdateValid(int serno)
    {
        Database db = DBHelper.GetDatabase();
        string sql = "update [funoperatorgroup] set [valid]=@valid,mdate=@mdate,oid=@oid where serno=@serno";

        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "serno", DbType.Int32, serno);
        db.AddInParameter(dbComm, "valid", DbType.String, 0);
        db.AddInParameter(dbComm, "mdate", DbType.DateTime, DateTime.Now);
        db.AddInParameter(dbComm, "oid", DbType.Int32, BackendBasePage.GetUserSerno());
        try
        {
            db.ExecuteNonQuery(dbComm);
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }

    #endregion

    #region backend/operatorgroup/edit
    
    /// <summary>
    ///顯示單一群組資料
    /// </summary>
    /// <param name="serno"></param>
    /// <returns></returns>
    [DataObjectMethod(DataObjectMethodType.Select)]
    public static DataTable GetListBySerno(string serno)
    {
        Database db = DBHelper.GetDatabase();

        string sql = "select * from [funoperatorgroup]";
        sql += DBHelper.GetSqlWhereOrAnd(sql) + " [serno]=@serno";
        
        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "serno", DbType.Int32, Convert.ToInt32(serno));
        
        DataTable dt = new DataTable();
        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        return dt;
    }

    /// <summary>
    /// 新增
    /// </summary>
    /// <param name="name"></param>
    /// <param name="funlist"></param>
    /// <param name="usrlist"></param>
    /// <param name="valid"></param>
    [DataObjectMethod(DataObjectMethodType.Insert)]
    public static void Insert(string name, string funlist, string usrlist, string valid)
    {
        //處理預設值不能為Null的問題
        name = (name == null ? "" : name);
        funlist = (funlist == null ? "" : funlist);
        usrlist = (usrlist == null ? "" : usrlist);
        valid = (valid == null ? "" : valid);
        //SQL區段
        Database db = DBHelper.GetDatabase();
        string sql = @"
            INSERT INTO [funoperatorgroup]
                ([Name], [FunList], [UsrList], [Valid], [Oid])
            VALUES
                (@name,  @funlist,  @usrlist,  @valid,  @oid)
            ;
        ";
        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "name", DbType.String, name);
        db.AddInParameter(dbComm, "funlist", DbType.String, funlist);
        db.AddInParameter(dbComm, "usrlist", DbType.String, usrlist);
        db.AddInParameter(dbComm, "valid", DbType.Int32, Convert.ToInt32(valid));
        db.AddInParameter(dbComm, "oid", DbType.Int32, BackendBasePage.GetUserSerno());
        try
        {
            db.ExecuteNonQuery(dbComm);
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }

    /// <summary>
    /// 更新
    /// </summary>
    /// <param name="serno"></param>
    /// <param name="name"></param>
    /// <param name="funlist"></param>
    /// <param name="usrlist"></param>
    /// <param name="valid"></param>
    [DataObjectMethod(DataObjectMethodType.Update)]
    public static void Update(int serno, string name, string funlist, string usrlist, string valid)
    {
        //處理預設值不能為Null的問題
        name = (name == null ? "" : name);
        funlist = (funlist == null ? "" : funlist);
        usrlist = (usrlist == null ? "" : usrlist);
        valid = (valid == null ? "" : valid);
        //SQL區段
        Database db = DBHelper.GetDatabase();
        string sql = @"
            UPDATE [funoperatorgroup] SET
                [Name]=@name,
                [FunList]=@funlist,
                [UsrList]=@usrlist,
                [Valid]=@valid,
                [MDate]=@mdate,
                [Oid]=@oid
            WHERE
                [Serno]=@serno
            ;
        ";
        DbCommand dbComm = db.GetSqlStringCommand(sql);
        db.AddInParameter(dbComm, "serno", DbType.Int32, serno);
        db.AddInParameter(dbComm, "name", DbType.String, name);
        db.AddInParameter(dbComm, "funlist", DbType.String, funlist);
        db.AddInParameter(dbComm, "usrlist", DbType.String, usrlist);
        db.AddInParameter(dbComm, "valid", DbType.Int32, Convert.ToInt32(valid));
        db.AddInParameter(dbComm, "mdate", DbType.DateTime, DateTime.Now);
        db.AddInParameter(dbComm, "oid", DbType.Int32, BackendBasePage.GetUserSerno());
        try
        {
            db.ExecuteNonQuery(dbComm);
        }
        catch (Exception ex)
        {
            throw (ex);
        }
    }

    #endregion

    #region Common Block (共用方法)

	public static string GetGroupNameByUserSerno(string _serno)
	{
		string returnValue = string.Empty;

		if (!string.IsNullOrEmpty(_serno))
		{
			Database db = DatabaseFactory.CreateDatabase("ConnString");
			string sql = "select Name from [funoperatorgroup] where [UsrList] like @serno";
			DbCommand dbComm = db.GetSqlStringCommand(sql);
			db.AddInParameter(dbComm, "serno", DbType.String, "%" + _serno + ",%");
			DataTable dt = db.ExecuteDataSet(dbComm).Tables[0];

			if (dt.Rows.Count > 0)
			{
				returnValue = dt.Rows[0]["Name"].ToString();
			}
		}

		return returnValue;
	}

    #endregion
}