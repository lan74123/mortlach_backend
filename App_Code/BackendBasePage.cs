﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

/// <summary>
/// BackendBasePage 的摘要描述
/// </summary>
public class BackendBasePage : BasePage
{
    /// <summary>
    /// 系統訊息
    /// </summary>
    public string sys_msg = string.Empty;

    public static string sys_defaultpage = Config.BackendPath + "index.aspx";

    /// <summary>
    /// 前端彈出視窗
    /// </summary>
    /// <param name="_info">訊息</param>
    protected void JSOutPut(string _info)
    {
        string strScripts;
        strScripts = "<script language=javascript>";
        strScripts += "window.alert('" + _info + "');";
        strScripts += "</script>";
        ClientScript.RegisterStartupScript(this.GetType(), System.DateTime.Now.ToString(), strScripts);
    }

    /// <summary>
    /// 前端轉址
    /// </summary>
    /// <param name="_url">網址</param>
    protected void JSRedirect(string _url)
    {
        string strScripts;
        strScripts = "<script language=javascript>";
        strScripts += "location.href=\"" + _url + "\"";
        strScripts += "</script>";

        ClientScript.RegisterStartupScript(this.GetType(), System.DateTime.Now.ToString(), strScripts);
    }

    /// <summary>
    /// 前端彈出視窗並且轉址
    /// </summary>
    /// <param name="_info"></param>
    /// <param name="_url"></param>
    protected void JSOutPutAndRedirect(string _info, string _url)
    {
        string strScripts;
        strScripts = "<script language=javascript>";
        strScripts += "window.alert('" + _info + "');";
        strScripts += "location.href=\"" + _url + "\"";
        strScripts += "</script>";

        ClientScript.RegisterStartupScript(this.GetType(), System.DateTime.Now.ToString(), strScripts);
    }

    /// <summary>
    /// 前端彈出視窗並且關閉網頁 (好像會有問題)
    /// </summary>
    /// <param name="_info"></param>
    protected void JSOutPutAndClose(string _info)
    {
        string strScripts;
        strScripts = "<script language=javascript>";
        strScripts += "window.alert('" + _info + "');";
        strScripts += "window.opener=null; window.open('','_self');window.close();";
        strScripts += "</script>";

        ClientScript.RegisterStartupScript(this.GetType(), System.DateTime.Now.ToString(), strScripts);
    }

    /// <summary>
    /// 是否已通過驗證且登入
    /// </summary>
    /// <returns></returns>
    public static bool IsLogin()
    {
        if (HttpContext.Current.User.Identity.IsAuthenticated)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// 在已登入狀態下取得使用者流水號
    /// </summary>
    /// <returns></returns>
    public static int GetUserSerno()
    {
        int returnValue = 0;

        if(IsLogin())
        {
            returnValue = Convert.ToInt32((((FormsIdentity)HttpContext.Current.User.Identity).Ticket).UserData);
        }

        return returnValue;
    }

    /// <summary>
    /// 在已登入狀態下取得使用者名稱
    /// </summary>
    /// <returns></returns>
    public static string GetUserName()
    {
        string returnValue = string.Empty;

        if (IsLogin())
        {
            returnValue = HttpContext.Current.User.Identity.Name;
        }

        return returnValue;
    }
    
}