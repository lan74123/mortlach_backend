﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// BasePage 的摘要描述
/// </summary>
public class BasePage : System.Web.UI.Page
{
    public BasePage()
    {
        this.PreRender += new EventHandler(Page_PreRender);
    }


    private void Page_PreRender(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SetActionStamp();
        }

        ClientScript.RegisterHiddenField("actionStamp", Session["actionStamp"] as string);
    }

    protected override void OnLoad(EventArgs e)
    {
        //相容性設定，若前端有設定則會依照前端的設定
        Response.AddHeader("X-UA-Compatible", "IE=edge,chrome=1");

        // 是否為local端
        if (Request.ServerVariables["HTTP_HOST"] + "/" == Config.Domain || HttpContext.Current.Request.IsLocal.Equals(true))
        {
        }
        else
        {
            // 如果 client想訪問 /backend 目錄 
            // 會先檢查該ip是否為許可ip 若非則導向首頁

            if (Request.Url.Segments[1] == "backend/")
            {

                string client_ip = Request.ServerVariables["REMOTE_ADDR"];
                string test = client_ip.Substring(0, 8);
                string allow_ip = "122.147.";
                string allow_ip2 = "122.146."; //wifi

                if (!client_ip.Substring(0, 8).Contains(allow_ip) && !client_ip.Substring(0, 8).Contains(allow_ip2))
                {
                    Response.Redirect("../index.html");
                }
                else
                {
                    base.OnLoad(e);
                }
            }
        }
    }

    /// <summary>
    /// 設置戳記
    /// </summary>
    private void SetActionStamp()
    {
        Session["actionStamp"] = Server.UrlEncode(DateTime.Now.ToString());
    }

    /// <summary>
    /// 取得值，指出網頁是否經由重新整理動作回傳 (PostBack)
    /// </summary>
    protected bool IsRefresh
    {
        get
        {
            if (HttpContext.Current.Request["actionStamp"] as string == Session["actionStamp"] as string)
            {
                SetActionStamp();
                return false;
            }
            return true;
        }
    }
}