﻿using System;
using System.Configuration;
using System.IO;
using System.Web;

/// <summary>
/// Web.Config 參數設定
/// </summary>
public class Config
{
    static Config()
    {
        Value_Y_N Enum_IsSSL = Value_Y_N.N;
        Value_Y_N Enum_IsDebug = Value_Y_N.N;

        Site = ConfigurationManager.AppSettings["Site"].ToString();
        BackendPath = string.Format("/{0}", ConfigurationManager.AppSettings["BackendFolder"].ToString());

        Enum.TryParse(ConfigurationManager.AppSettings["IsSSL"].ToString(), out Enum_IsSSL);
        IsSSL = Enum_IsSSL;

        Enum.TryParse(ConfigurationManager.AppSettings["IsDebug"].ToString(), out Enum_IsDebug);
        IsDebug = Enum_IsDebug;

        if (HttpContext.Current.Request.IsLocal.Equals(true) || IsDebug == Value_Y_N.Y)
        {
            Domain = ConfigurationManager.AppSettings["TWebDomain"].ToString();
            ConnString = ConfigurationManager.ConnectionStrings["TConnString"].ConnectionString;
        }
        else
        {
            Domain = ConfigurationManager.AppSettings["WebDomain"].ToString();
            ConnString = ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString;
        }

    }

    /// <summary>
    /// DB連線字串
    /// </summary>
    public static string ConnString { get; private set; }
    /// <summary>
    /// Domain
    /// </summary>
    public static string Domain { get; private set; }
    /// <summary>
    /// 網站名稱
    /// </summary>
    public static string Site { get; private set; }
    /// <summary>
    /// 後台資料夾路徑
    /// </summary>
    public static string BackendPath { get; private set; }
    /// <summary>
    /// 是否啟用SSL
    /// </summary>
    public static Value_Y_N IsSSL { get; private set; }
    /// <summary>
    /// 是否為Debug模式
    /// </summary>
    public static Value_Y_N IsDebug { get; private set; }
}