﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Data;
using System.Data.SqlTypes;
using System.Web;

public class DBHelper
{
    /// <summary>
    /// 0、N、FALSE → false
    /// <para></para>
    /// 1、Y、TRUE → true
    /// </summary>
    /// <param name="_value"></param>
    /// <returns></returns>
    public static bool GetTrueOrFalse(string _value)
    {
        _value = _value.Trim().ToUpper();
        bool returnValue = false;

        switch (_value)
        {
            case "0":
            case "N":
            case "FALSE":
                returnValue = false;
                break;
            case "1":
            case "Y":
            case "TRUE":
                returnValue = true;
                break;
        }

        return returnValue;
    }

    /// <summary>
    /// 取得SQL條件串接字串
    /// </summary>
    /// <param name="_value"></param>
    /// <returns></returns>
    public static string GetSqlWhereOrAnd(string _value)
    {
        if (!string.IsNullOrEmpty(_value.ToLower().Trim()))
        {
            if (_value.IndexOf("where") >= 0)
            {
                return " and";
            }
            else
            {
                return " where";
            }
        }
        return "";
    }

    /// <summary>
    /// 取得資料庫
    /// </summary>
    /// <returns></returns>
    public static Database GetDatabase()
    {
        if (HttpContext.Current.Request.IsLocal.Equals(true) || Config.IsDebug == Value_Y_N.Y)
        {
            return DatabaseFactory.CreateDatabase("TConnString");
        }
        else
        {
            return DatabaseFactory.CreateDatabase("ConnString");
        }
        
    }
}