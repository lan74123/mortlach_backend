﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

/// <summary>
/// EncryptAES256 的摘要描述
/// </summary>
public class EncryptHelper
{
    public static string key = "ESixDiageoxxxxxxxxxxxxxxxxxxxxxx";
    public static string iv = "ESixDiageoxxxxxx";
    public static string salt = "ESixDiageo";
    //public static string salt = "ESixDiageoxxxxxxxxxxxx";
    public static string Encrypt(string source)
    {
        // source 為明文 ex:ESi201811071230
        // 編譯後結果為  EQLtvFTnfF4zJ6AQpJVeAQ==
        byte[] sourceBytes = Encoding.UTF8.GetBytes(source);
        var aes = new RijndaelManaged();
        //aes.KeySize = 256;
        aes.BlockSize = 128;
        aes.Key = Encoding.UTF8.GetBytes(key);
        aes.IV = Encoding.UTF8.GetBytes(iv);
        aes.Mode = CipherMode.CBC;
        aes.Padding = PaddingMode.PKCS7;

        ICryptoTransform transform = aes.CreateEncryptor();

        return Convert.ToBase64String(transform.TransformFinalBlock(sourceBytes, 0, sourceBytes.Length));
    }
    public static string sha256Encrypt(string source)
    {
        //byte[] bytes = Encoding.UTF8.GetBytes(source + salt);
        //SHA256Managed sHA256ManagedString = new SHA256Managed();
        //byte[] hash = sHA256ManagedString.ComputeHash(bytes);
        //return Convert.ToBase64String(hash);

        byte[] sha256Data = Encoding.UTF8.GetBytes(source+salt);
        SHA256Managed sha256 = new SHA256Managed();
        byte[] buffer = sha256.ComputeHash(sha256Data);
        // 可以根据需要处理加密后的字节数组，比如使用Base64.这里使用BitConverter转为64位字符。
        return BitConverter.ToString(buffer).Replace("-", "").ToLower();
    }
}