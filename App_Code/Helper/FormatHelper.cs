﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// FormatHelper 的摘要描述
/// </summary>
public class FormatHelper
{
    public static string formatSex(string _value)
    {
        if (string.Equals(_value, "M"))
            return "0";
        else if (string.Equals(_value, "F"))
            return "1";
        else
            return "0";
    }
    public static string formatMonthlyIncome(string _value)
    {
        if (string.Equals(_value, "20,000 以下"))
            return "0";
        else if (string.Equals(_value, "20,001 - 35,000"))
            return "1";
        else if (string.Equals(_value, "35,001 - 50,000"))
            return "2";
        else if (string.Equals(_value, "50,001 - 65,000"))
            return "3";
        else if (string.Equals(_value, "65,001 - 80,000"))
            return "4";
        else if (string.Equals(_value, "80,001以上"))
            return "5";
        else
            return "0";
    }
    public static string formatJobTitle(string _value)
    {
        if (string.Equals(_value, "經營∕行政∕總務"))
            return "0";
        else if (string.Equals(_value, "業務∕貿易∕銷售"))
            return "1";
        else if (string.Equals(_value, "人資∕法務∕智財"))
            return "2";
        else if (string.Equals(_value, "財務∕金融∕保險"))
            return "3";
        else if (string.Equals(_value, "客服∕門市"))
            return "4";
        else if (string.Equals(_value, "工程∕研發∕生技"))
            return "5";
        else if (string.Equals(_value, "品管∕製造∕環衛"))
            return "6";
        else if (string.Equals(_value, "技術∕維修∕操作"))
            return "7";
        else if (string.Equals(_value, "資訊∕軟體∕系統"))
            return "8";
        else if (string.Equals(_value, "營建∕製圖∕施作"))
            return "9";
        else if (string.Equals(_value, "新聞∕出版∕印刷"))
            return "10";
        else if (string.Equals(_value, "教育∕學術∕研究"))
            return "11";
        else if (string.Equals(_value, "物流∕運輸∕資材"))
            return "12";
        else if (string.Equals(_value, "旅遊∕餐飲∕休閒"))
            return "13";
        else if (string.Equals(_value, "醫療∕美容∕保健"))
            return "14";
        else if (string.Equals(_value, "保全∕軍警消"))
            return "15";
        else if (string.Equals(_value, "清潔∕家事∕保姆"))
            return "16";
        else if (string.Equals(_value, "農林漁牧相關"))
            return "17";
        else if (string.Equals(_value, "行銷∕企劃∕專案"))
            return "18";
        else if (string.Equals(_value, "其他類別"))
            return "19";
        else if (string.Equals(_value, "廣告∕公關∕設計"))
            return "20";
        else
            return "19";
    }
    public static string formatWhiskyBrand(string _value)
    {
        return "0";
    }
    public static string formatWhiskyFrequency(string _value)
    {
        if (string.Equals(_value, "每週1次或以上"))
            return "0";
        else if (string.Equals(_value, "每個月1-3次"))
            return "1";
        else if (string.Equals(_value, "每3個月1-3次"))
            return "1";
        else if (string.Equals(_value, "少於每3個月1次"))
            return "1";
        else
            return "4";
    }
}