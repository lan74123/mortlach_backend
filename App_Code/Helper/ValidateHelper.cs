﻿using System;
using System.Configuration;
using Microsoft.Security.Application;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.Text;

/// <summary>
/// ValidateHelper 的摘要描述
/// </summary>
public static class ValidateHelper
{
    public static Encryption.Crypt Crypt = new Encryption.Crypt();

    /// <summary>
    /// 過濾XSS (含去前後空白)
    /// </summary>
    /// <param name="_value"></param>
    public static string FilteXSSValue(string _value)
    {
        return Sanitizer.GetSafeHtmlFragment(_value.Trim());
    }

    /// <summary>
    /// 檢查是否為數字型態
    /// </summary>
    /// <param name="_value">數字字串</param>
    /// <returns>Y/N</returns>
    public static bool IsNumber(string _value)
    {
        Regex objNotNumberPattern = new Regex("[^0-9.-]");
        Regex objTwoDotPattern = new Regex("[0-9]*[.][0-9]*[.][0-9]*");
        Regex objTwoMinusPattern = new Regex("[0-9]*[-][0-9]*[-][0-9]*");
        string strValidRealPattern = "^([-]|[.]|[-.]|[0-9])[0-9]*[.]*[0-9]+$";
        string strValidIntegerPattern = "^([-]|[0-9])[0-9]*$";
        Regex objNumberPattern = new Regex("(" + strValidRealPattern + ")|(" + strValidIntegerPattern + ")");

        return !objNotNumberPattern.IsMatch(_value) & !objTwoDotPattern.IsMatch(_value) & !objTwoMinusPattern.IsMatch(_value) & objNumberPattern.IsMatch(_value);
    }

    /// <summary>
    /// 檢查是否為合格email 
    /// </summary>
    /// <param name="_value">Email</param>
    /// <returns>Y/N</returns>
    public static bool IsEmail(string _value)
    {
        if (!string.IsNullOrEmpty(_value))
        {
            Regex n = new Regex("[a-zA-Z0-9._%-]+@[a-zA-Z0-9._%-]+.[a-zA-Z]{2,4}");
            Match v = n.Match(_value);

            if (!v.Success || _value.Length != v.Length)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }

    }

    /// <summary>
    /// 檢查是否為手機號碼:要純數字,且 10 碼, 且 09開頭
    /// </summary>
    /// <param name="_value">手機字串</param>
    /// <returns>Y/N</returns>
    public static bool IsMobile(string _value)
    {
        if (!string.IsNullOrEmpty(_value))
        {
            Regex n = new Regex("^09\\d{8}$");
            Match v = n.Match(_value);

            if (!v.Success || _value.Length != v.Length)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// 檢查是否為日期格式
    /// </summary>
    /// <param name="_value">日期字串</param>
    /// <returns>Y/N</returns>
    public static bool IsDateTime(string _value)
    {
        bool blnIsDate = false;
        try
        {
            DateTime myDateTime = DateTime.Parse(_value);
            blnIsDate = true;
        }
        catch { }
        return blnIsDate;
    }

    /// <summary>
    /// 檢查參數長度是否介於 M to N ，(都設0可以偵測是否為空字串)
    /// </summary>
    /// <param name="_value">字串</param>
    /// <param name="m">小於等於</param>
    /// <param name="n">大於等於</param>
    /// <returns>Y/N</returns>
    public static bool IsLenMToN(string _value, int m, int n)
    {
        if (_value.Length >= m && _value.Length <= n)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

	/// <summary>
	/// 檢查是否為空
	/// </summary>
	/// <param name="_value"></param>
	/// <returns></returns>
	public static bool IsEmpty(string _value)
	{
		if (string.IsNullOrEmpty(_value))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

    /// <summary>
    /// 檢查是否為合法的身份證字號
    /// </summary>
    /// <param name="_value">身份證字號</param>
    /// <returns>Y/N</returns>
    public static bool IsIlleagleID(string _value)
    {
        if (string.IsNullOrEmpty(_value))
            return false;   //沒有輸入，回傳 ID 錯誤
        _value = _value.ToUpper();
        Regex regex = new Regex("^[A-Z]{1}[0-9]{9}$");
        if (!regex.IsMatch(_value))
            return false;   //Regular Expression 驗證失敗，回傳 ID 錯誤

        int[] seed = new int[10];       //除了檢查碼外每個數字的存放空間
        string[] charMapping = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "X", "Y", "W", "Z", "I", "O" };
        //A=10 B=11 C=12 D=13 E=14 F=15 G=16 H=17 J=18 K=19 L=20 M=21 N=22
        //P=23 Q=24 R=25 S=26 T=27 U=28 V=29 X=30 Y=31 W=32  Z=33 I=34 O=35
        string target = _value.Substring(0, 1);
        for (int index = 0; index < charMapping.Length; index++)
        {
            if (charMapping[index] == target)
            {
                index += 10;
                seed[0] = index / 10;       //10進制的高位元放入存放空間
                seed[1] = (index % 10) * 9; //10進制的低位元*9後放入存放空間
                break;
            }
        }
        for (int index = 2; index < 10; index++)
        {   //將剩餘數字乘上權數後放入存放空間
            seed[index] = Convert.ToInt32(_value.Substring(index - 1, 1)) * (10 - index);
        }
        //檢查是否符合檢查規則，10減存放空間所有數字和除以10的餘數的個位數字是否等於檢查碼
        //(10 - ((seed[0] + .... + seed[9]) % 10)) % 10 == 身分證字號的最後一碼
        int ss = 0;
        for (int i = 0; i <= 9; i++) ss += seed[i];
        return (10 - (ss % 10)) % 10 == Convert.ToInt32(_value.Substring(9, 1));
    }

    /// <summary>
    /// 檢查SQL Injection
    /// </summary>
    /// <param name="mystr">字串</param>
    /// <returns>True: 安全</returns>
    public static bool IsSQLInjection(string mystr)
    {
        bool rtn = false;
        if (FilteSqlInjection(mystr) == "")
        {
            rtn = true;  //安全的
        }
        else
        {
            rtn = false; //不安全的…含有injection的值
        }
        return rtn;
    }


    /// <summary>
    /// SQL Injection(如果參數存在不安全字符，則返回 不安全字符
    /// </summary>
    /// <param name="_value">檢驗字串</param>
    /// <returns>字串</returns>
    public static string FilteSqlInjection(string _value)
    {
        _value = _value + " ";
        _value = _value.Trim();

        string reStr = "";

        if (!string.IsNullOrEmpty(_value))
        {
            string SQL_injdata = "exec | insert | select | delete | update | count | chr | mid |master|truncate| char |declare|drop |creat | table| and |*|%|'|<|>";
            char[] tmp = { '|' };
            string[] sql_c = SQL_injdata.Split(tmp);
            //string sl = null;
            foreach (string sl in sql_c)
            {
                if (_value.ToLower().IndexOf(sl.TrimStart()) >= 0)
                {
                    reStr = sl;
                    break; // TODO: might not be correct. Was : Exit For
                }
            }
        }
        return reStr;
    }

    /// <summary>
    /// 字串加密
    /// </summary>
    /// <param name="_value"></param>
    /// <returns></returns>
    public static string EncryptString(string _value)
    {
        if (!string.IsNullOrEmpty(_value.Trim()))
        {
            return Crypt.EncryptStr(_value);
        }
        return "";
    }

    /// <summary>
    /// 字串解密
    /// </summary>
    /// <param name="_value"></param>
    /// <returns></returns>
    public static string DecryptString(string _value)
    {
        if (!string.IsNullOrEmpty(_value.Trim()))
        {
            return Crypt.Decrypt(_value);
        }
        return "";
    }

	/// <summary>
	/// 字串加密(AES)
	/// </summary>
	/// <param name="_value"></param>
	/// <returns></returns>
	public static string EncryptStringByAES(string _value)
	{
		string key = ConfigurationManager.AppSettings["AesKey"];
		string iv = ConfigurationManager.AppSettings["AesIV"];

		byte[] encryptBytes = Encoding.UTF8.GetBytes(_value);
		var aes = new RijndaelManaged();
		aes.Key = Encoding.UTF8.GetBytes(key);
		aes.IV = Encoding.UTF8.GetBytes(iv);
		aes.Mode = CipherMode.CBC;
		aes.Padding = PaddingMode.PKCS7;

		ICryptoTransform transform = aes.CreateEncryptor();

		return Convert.ToBase64String(transform.TransformFinalBlock(encryptBytes, 0, encryptBytes.Length));
	}

	/// <summary>
	/// 字串解密(AES)
	/// </summary>
	/// <param name="_value"></param>
	/// <returns></returns>
	public static string DecryptStringByAES(string _value)
	{
		string key = ConfigurationManager.AppSettings["AesKey"];
		string iv = ConfigurationManager.AppSettings["AesIV"];

		byte[] decryptBytes = Convert.FromBase64String(_value);
		var aes = new RijndaelManaged();
		aes.Key = Encoding.UTF8.GetBytes(key);
		aes.IV = Encoding.UTF8.GetBytes(iv);
		aes.Mode = CipherMode.CBC;
		aes.Padding = PaddingMode.PKCS7;

		ICryptoTransform transform = aes.CreateDecryptor();

		return Encoding.UTF8.GetString(transform.TransformFinalBlock(decryptBytes, 0, decryptBytes.Length));
	}

	/// <summary>
	/// 檢查是否為GUID格式
	/// </summary>
	/// <param name="_value">SystemID</param>
	/// <returns></returns>
	public static bool IsGUID(string _value)
	{
		if (!string.IsNullOrEmpty(_value))
		{
			Regex n = new Regex("[0-9a-zA-Z]{8}-([0-9a-zA-Z]{4}-){3}[0-9a-zA-Z]{12}");
			Match v = n.Match(_value);

			if (!v.Success || _value.Length != v.Length)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		else
		{
			return false;
		}
	}

	/// <summary>
	/// 檢查VIN碼是否有效
	/// </summary>
	/// <param name="_value">VIN</param>
	/// <returns></returns>
	public static bool IsCarVIN(string _value)
	{
		if(IsLenMToN(_value,17,17))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/// <summary>
	/// 檢查是否為車牌格式
	/// </summary>
	/// <param name="_value">LicensePlateNumber</param>
	/// <returns></returns>
	public static bool IsLicensePlateNumber(string _value)
	{
		/*
		if (!string.IsNullOrEmpty(_value))
		{
			//Regex n = new Regex("[A-Z]{2}-[0-9]{4}|[0-9]{4}-[A-Z]{2}|[A-Z]{3}-[0-9]{4}|[A-Z]{2}-[0-9]{3}|[0-9]{3}-[A-Z]{2}|[A-Z]{3}-[0-9]{3}");
			Regex n = new Regex("*"); //20160308 因格式不一先跳過檢查
			Match v = n.Match(_value);

			if (!v.Success || _value.Length != v.Length)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		else
		{
			return false;
		}
		*/
		return true;
	}

	/// <summary>
	/// 檢查是否為經緯度格式
	/// </summary>
	/// <param name="_value"></param>
	/// <returns></returns>
	public static bool IsLatLng(string _value)
	{
		if (!string.IsNullOrEmpty(_value))
		{
			Regex n = new Regex(@"((-|)\d*\.\d*)");
			//Regex n = new Regex("*"); //20160308 因格式不一先跳過檢查
			Match v = n.Match(_value);

			if (!v.Success || _value.Length != v.Length)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		else
		{
			return false;
		}
	}

	/// <summary>
	/// 檢查是否為16進制格式
	/// </summary>
	/// <param name="_value"></param>
	/// <returns></returns>
	public static bool IsHex16(string _value)
	{
		if (!string.IsNullOrEmpty(_value))
		{
			Regex n = new Regex(@"0[xX][0-9A-Fa-f]+");
			//Regex n = new Regex("*"); //20160308 因格式不一先跳過檢查
			Match v = n.Match(_value);

			if (!v.Success || _value.Length != v.Length)
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		else
		{
			return false;
		}
	}

	/// <summary>
	/// 配合高路規格檢查品牌代碼
	/// </summary>
	/// <param name="_value"></param>
	/// <returns></returns>
	public static bool IsInCarBrands(int _value)
	{
		if (_value < 26 || _value > 49)
		{
			if (_value == 206 || _value == 207)
			{
				return true;
			}
			return false;
		}
		else
		{
			return true;
		}
	}

	/// <summary>
	/// 配合高路規格檢查試駕中心代碼
	/// </summary>
	/// <param name="_value"></param>
	/// <returns></returns>
	public static bool IsInDrivePlace(string _value)
	{
		int n = 9999;

		int.TryParse(_value, out n);

		if ((n >= 50 && n <= 75) || n == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/// <summary>
	/// 配合高路規格檢查性別代碼
	/// </summary>
	/// <param name="_value"></param>
	/// <returns></returns>
	public static bool IsGender(int _value)
	{

		if (_value != 8 && _value != 9)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	/// <summary>
	/// 配合高路規格檢查年齡代碼
	/// </summary>
	/// <param name="_value"></param>
	/// <returns></returns>
	public static bool IsAge(int _value)
	{

		if (_value >= 76 && _value <= 83)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	/// <summary>
	/// 檢查FB ID(其實只是看有沒有英數以外的字元)
	/// </summary>
	/// <param name="word"></param>
	/// <returns></returns>
	public static bool IsNumandEGDOT(string _value)
	{
		Regex NumandEG = new Regex("[A-Za-z0-9.]");
		return NumandEG.IsMatch(_value);
	}
}