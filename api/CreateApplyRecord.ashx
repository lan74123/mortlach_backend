﻿<%@ WebHandler Language="C#" Class="CreateApplyRecord" %>

using System;
using System.Web;
using System.Net;
using System.Text;
using System.IO;
using System.Configuration;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;


using System.Net.Security;



public class CreateApplyRecord : IHttpHandler
{
    //public string FamilyName, GivenName, Sex, Birthday, Phone, Email, Income, Job, Session, Referrer,
    //        utm_source, utm_medium, utm_campaign, ip = "";

    private string conn = Config.ConnString;

    //private string conn = ConfigurationManager.ConnectionStrings["TConnString"].ToString();
    //demo 站是用下面的
    //private string conn = ConfigurationManager.ConnectionStrings["ConnString"].ToString();


    public string new_lastname, new_firstname, new_birthday, new_gender, mobilephone, new_mobilephone, new_email, new_monthlyincome,
            new_jobtitle, new_sessions, referencemobilephone, new_referencemobilephone, new_source, new_content, new_medium, new_campaign,
            new_whiskybrand, new_whiskyfrequency;
    //output
    private string _errcode = "";
    private string _errmsg = "";

    public void ProcessRequest(HttpContext context)
    {
        SslProtocols protocol = SslProtocols.Ssl2 | SslProtocols.Ssl3;
        context.Response.ContentType = "application/json";
        string url = context.Request.Url.AbsoluteUri;
        // ---- url ==> "http://localhost:1234/api/save_data.ashx"
        string baseUrl = context.Request.Url.Authority;
        // ---- baseUrl ==> "localhost:1234"

        //var request = (HttpWebRequest)WebRequest.Create("http://" + baseUrl + "/api/save_data.ashx");
        var request = (HttpWebRequest)WebRequest.Create("https://diageocrmapi.azurewebsites.net/DiageoApi/CreateApplyRecord");

        ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);
        ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;// SecurityProtocolType.Tls1.2;

        //request.ContentType = "application/x-www-form-urlencoded";
        //request.ContentType = "application/json";
        request.Method = "POST";
        //request.ContentLength = byteArray.Length;
        //data 從網頁抓

        //string postData = "Phone=0912345678&Email=hello@mail.com";
        //byte[] byteArray = Encoding.UTF8.GetBytes(postData);

        //FamilyName = string.IsNullOrEmpty(context.Request["FamilyName"]) ? "" : context.Request["FamilyName"];
        //GivenName = string.IsNullOrEmpty(context.Request["GivenName"]) ? "" : context.Request["GivenName"];
        //Sex = string.IsNullOrEmpty(context.Request["Sex"]) ? "" : context.Request["Sex"];
        //Birthday = string.IsNullOrEmpty(context.Request["Birthday"]) ? "" : context.Request["Birthday"];
        //Phone = string.IsNullOrEmpty(context.Request["Phone"]) ? "" : context.Request["Phone"];
        //Email = string.IsNullOrEmpty(context.Request["Email"]) ? "" : context.Request["Email"];
        //Income = string.IsNullOrEmpty(context.Request["Income"]) ? "" : context.Request["Income"];
        //Job = string.IsNullOrEmpty(context.Request["Job"]) ? "" : context.Request["Job"];
        //Session = string.IsNullOrEmpty(context.Request["Session"]) ? "" : context.Request["Session"];
        //Referrer = string.IsNullOrEmpty(context.Request["Referrer"]) ? "" : context.Request["Referrer"];
        //utm_source = string.IsNullOrEmpty(context.Request["utm_source"]) ? "" : context.Request["utm_source"];
        //utm_medium = string.IsNullOrEmpty(context.Request["utm_medium"]) ? "" : context.Request["utm_medium"];
        //utm_campaign = string.IsNullOrEmpty(context.Request["utm_campaign"]) ? "" : context.Request["utm_campaign"];

        if (context.Request.RequestType == "POST")
        {

            new_lastname = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["new_lastname"]) ? "" : context.Request["new_lastname"]);
            new_firstname = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["new_firstname"]) ? "" : context.Request["new_firstname"]);
            new_birthday = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["new_birthday"]) ? "" : context.Request["new_birthday"]);
            new_gender = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["new_gender"]) ? "" : context.Request["new_gender"]);
            mobilephone = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["new_mobilephone"]) ? "" : context.Request["new_mobilephone"]);
            new_mobilephone = EncryptHelper.sha256Encrypt(mobilephone);
            new_email = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["new_email"]) ? "" : context.Request["new_email"]);
            new_monthlyincome = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["new_monthlyincome"]) ? "" : context.Request["new_monthlyincome"]);
            new_jobtitle = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["new_jobtitle"]) ? "" : context.Request["new_jobtitle"]);
            new_sessions = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["new_sessions"]) ? "" : context.Request["new_sessions"]);
            referencemobilephone = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["new_referencemobilephone"]) ? "" : context.Request["new_referencemobilephone"]);
            new_referencemobilephone = EncryptHelper.sha256Encrypt(referencemobilephone);
            new_source = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["new_source"]) ? "" : context.Request["new_source"]);
            new_content = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["new_content"]) ? "" : context.Request["new_content"]);
            new_medium = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["new_medium"]) ? "" : context.Request["new_medium"]);
            new_campaign = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["new_campaign"]) ? "" : context.Request["new_campaign"]);
            new_whiskybrand = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["new_whiskybrand"]) ? "" : context.Request["new_whiskybrand"]);
            new_whiskyfrequency = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["new_whiskyfrequency"]) ? "" : context.Request["new_whiskyfrequency"]);

            SqlConnection fmelconn_1;     //系統用
            SqlCommand fmelexec_1;        //系統用
            string Sql = "";

            //檢查參數
            if (_errmsg == "")
            {
                if (new_lastname == "")
                    _errmsg += "請輸入姓\\n";

                if (new_firstname == "")
                    _errmsg += "請輸入名\\n";

                if (new_birthday == "")
                    _errmsg += "請輸入生日\\n";

                if (new_gender == "")
                    _errmsg += "請選擇性別\\n";

                if (mobilephone == "")
                    _errmsg += "請輸入手機\\n";
                else if (!ValidateHelper.IsMobile(mobilephone))
                    _errmsg += "手機號碼格式錯誤\\n";

                if (new_email == "")
                    _errmsg += "請輸入Email\\n";
                else if (!ValidateHelper.IsEmail(new_email))
                    _errmsg += "Email格式錯誤\\n";

                if (new_monthlyincome == "")
                    _errmsg += "請輸入收入\\n";

                if (new_jobtitle == "")
                    _errmsg += "請輸入職業\\n";

                if (new_sessions == "")
                    _errmsg += "請選擇場次\\n";

                if (referencemobilephone != "")
                    if (!ValidateHelper.IsMobile(referencemobilephone))
                        _errmsg += "推薦人手機號碼格式錯誤\\n";


                if (!ValidateHelper.IsLenMToN(new_source, 0, 50)) _errmsg += "utm_source不得超過50字\\n";
                if (!ValidateHelper.IsLenMToN(new_medium, 0, 50)) _errmsg += "utm_medium不得超過50字\\n";
                if (!ValidateHelper.IsLenMToN(new_campaign, 0, 50)) _errmsg += "utm_campaign不得超過50字\\n";


            }

            //Do SQL
            try
            {
                if (_errmsg == "")
                {
                    //建立資料庫連結
                    fmelconn_1 = new SqlConnection(conn);
                    fmelconn_1.Open();
                    fmelexec_1 = new SqlCommand();
                    fmelexec_1.Connection = fmelconn_1;
                    fmelexec_1.Parameters.Clear();
                    Sql = @"INSERT INTO [dbo].[backup]
                                   ([new_lastname]
                                   ,[new_firstname]
                                   ,[new_birthday]
                                   ,[new_gender]
                                   ,[mobilephone]
                                   ,[new_mobilephone]
                                   ,[new_email]
                                   ,[new_monthlyincome]
                                   ,[new_jobtitle]
                                   ,[new_sessions]
                                   ,[referencemobilephone]
                                   ,[new_referencemobilephone]
                                   ,[new_source]
                                   ,[new_medium]
                                   ,[new_campaign]
                                   ,[isTransfered])
                                VALUES
                                (@new_lastname,@new_firstname,@new_birthday,@new_gender,@mobilephone,@new_mobilephone,@new_email,
                                @new_monthlyincome,@new_jobtitle,@new_sessions,@referencemobilephone,@new_referencemobilephone,
                                @new_source,@new_medium,@new_campaign,@isTransfered);";
                    //SQL 輸入方

                    fmelexec_1.Parameters.Add("@new_lastname", SqlDbType.NVarChar, 10);
                    fmelexec_1.Parameters.Add("@new_firstname", SqlDbType.NVarChar, 16);
                    fmelexec_1.Parameters.Add("@new_birthday", SqlDbType.DateTime);
                    fmelexec_1.Parameters.Add("@new_gender", SqlDbType.VarChar, 2);
                    fmelexec_1.Parameters.Add("@mobilephone", SqlDbType.Char, 10);
                    fmelexec_1.Parameters.Add("@new_mobilephone", SqlDbType.Char, 64);
                    fmelexec_1.Parameters.Add("@new_email", SqlDbType.VarChar, 200);
                    fmelexec_1.Parameters.Add("@new_monthlyincome", SqlDbType.VarChar, 2);
                    fmelexec_1.Parameters.Add("@new_jobtitle", SqlDbType.VarChar, 2);
                    fmelexec_1.Parameters.Add("@new_sessions", SqlDbType.NVarChar, 200);
                    fmelexec_1.Parameters.Add("@referencemobilephone", SqlDbType.Char, 10);
                    fmelexec_1.Parameters.Add("@new_referencemobilephone", SqlDbType.Char, 64);
                    fmelexec_1.Parameters.Add("@new_source", SqlDbType.NVarChar, 200);
                    fmelexec_1.Parameters.Add("@new_medium", SqlDbType.NVarChar, 200);
                    fmelexec_1.Parameters.Add("@new_campaign", SqlDbType.NVarChar, 200);
                    fmelexec_1.Parameters.Add("@isTransfered", SqlDbType.VarChar, 2);


                    fmelexec_1.Parameters["@new_lastname"].Value = new_lastname;
                    fmelexec_1.Parameters["@new_firstname"].Value = new_firstname;
                    fmelexec_1.Parameters["@new_birthday"].Value = new_birthday;
                    fmelexec_1.Parameters["@new_gender"].Value = new_gender;
                    fmelexec_1.Parameters["@mobilephone"].Value = mobilephone;
                    fmelexec_1.Parameters["@new_mobilephone"].Value = new_mobilephone;
                    fmelexec_1.Parameters["@new_email"].Value = new_email;
                    fmelexec_1.Parameters["@new_monthlyincome"].Value = new_monthlyincome;
                    fmelexec_1.Parameters["@new_jobtitle"].Value = new_jobtitle;
                    fmelexec_1.Parameters["@new_sessions"].Value = new_sessions;
                    fmelexec_1.Parameters["@referencemobilephone"].Value = referencemobilephone;
                    fmelexec_1.Parameters["@new_referencemobilephone"].Value = new_referencemobilephone;
                    fmelexec_1.Parameters["@new_source"].Value = new_source;
                    fmelexec_1.Parameters["@new_medium"].Value = new_medium;
                    fmelexec_1.Parameters["@new_campaign"].Value = new_campaign;
                    fmelexec_1.Parameters["@isTransfered"].Value = "0";


                    //SQL 輸出方

                    //SQL 準備
                    fmelexec_1.CommandText = Sql;
                    //取資料
                    try
                    {
                        fmelexec_1.ExecuteNonQuery();
                    }
                    catch (Exception o)
                    {
                        _errmsg += "SQL Error";
                        throw (o);
                    }
                    finally
                    {
                        fmelexec_1.Dispose();
                        fmelconn_1.Close();
                        fmelconn_1.Dispose();
                    }
                    using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                    {
                        string postData = new JavaScriptSerializer().Serialize(new
                        {
                            //new_lastname = FamilyName,
                            //new_firstname = GivenName,
                            //new_birthday = Birthday,
                            //new_gender = FormatHelper.formatSex(Sex),
                            //new_mobilephone = Phone,
                            //new_email = Email,
                            //new_monthlyincome = FormatHelper.formatMonthlyIncome(Income),
                            //new_jobtitle = FormatHelper.formatJobTitle(Job),
                            //new_sessions = Session,
                            //new_referencemobilephone = "",
                            //new_source = utm_source,
                            //new_content = "",
                            //new_medium = utm_medium,
                            //new_campaign = utm_campaign,
                            //new_whiskybrand = "1",
                            //new_whiskyfrequency = "1",

                            //new_lastname,
                            //new_firstname,
                            //new_birthday,
                            new_mobilephone,
                            new_gender,
                            //new_email,
                            new_monthlyincome,
                            new_jobtitle,
                            new_sessions,
                            new_referencemobilephone,
                            new_source,
                            new_content,
                            new_medium,
                            new_campaign,
                            new_whiskybrand,
                            new_whiskyfrequency,
                            checkCode = EncryptHelper.Encrypt("ESi" + (DateTime.Now.ToString("yyyyMMddHHmm").ToString())),
                        });

                        System.Diagnostics.Debug.WriteLine(postData);
                        streamWriter.Write(postData);
                    }

                    // Get the response.      
                    var response = (HttpWebResponse)request.GetResponse();
                    using (var streamReader = new StreamReader(response.GetResponseStream()))
                    {
                        var result = streamReader.ReadToEnd();
                        dynamic data = JObject.Parse(result);
                        context.Response.Write(data);
                    }
                }
                else
                {
                    _errmsg += "資料未存入 請重新確認 \\n";
                    context.Response.Write("{\"itemKey\": \"\", \"returnCode\": \"11\", \"returnMsg\": \""+_errmsg+"\"}");
                }



            }
            catch (Exception all_error)
            {

                _errmsg += "資料未存入 請重新確認 \\n";
                context.Response.Write("{\"itemKey\": \"\", \"returnCode\": \"11\", \"returnMsg\": \""+all_error+"\"}");
                //throw (all_error);
            }
        }
        else
        {
            _errmsg = "Unsupported HTTP method";
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    private static bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)

    {

        return true;

    }

}