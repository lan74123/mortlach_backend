﻿<%@ WebHandler Language="C#" Class="QueryApplyRecord" %>

using System;
using System.Web;
using System.Net;
using System.Text;
using System.IO;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;


using System.Net.Security;



public class QueryApplyRecord : IHttpHandler
{
    public string new_mobilephone;
    //output
    private string _errcode = "";
    private string _errmsg = "";

    public void ProcessRequest(HttpContext context)
    {
        SslProtocols protocol = SslProtocols.Ssl2 | SslProtocols.Ssl3;
        context.Response.ContentType = "application/json";
        string url = context.Request.Url.AbsoluteUri;
        // ---- url ==> "http://localhost:1234/api/save_data.ashx"
        string baseUrl = context.Request.Url.Authority;
        // ---- baseUrl ==> "localhost:1234"

        //var request = (HttpWebRequest)WebRequest.Create("http://" + baseUrl + "/api/save_data.ashx");
        var request = (HttpWebRequest)WebRequest.Create("https://diageocrmapi.azurewebsites.net/DiageoApi/QueryApplyRecord");
        ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(CheckValidationResult);
        ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;// SecurityProtocolType.Tls1.2;

        //request.ContentType = "application/x-www-form-urlencoded";
        request.ContentType = "application/json";
        request.Method = "POST";
        //request.ContentLength = byteArray.Length;
        //data 從網頁抓

        //string postData = "Phone=0912345678&Email=hello@mail.com";
        //byte[] byteArray = Encoding.UTF8.GetBytes(postData);

        new_mobilephone = string.IsNullOrEmpty(context.Request["new_mobilephone"]) ? "" : context.Request["new_mobilephone"];
        //string getdata = new StreamReader(context.Request.InputStream).ReadToEnd();
        //JObject mydata = JObject.Parse(getdata);
        //System.Diagnostics.Debug.WriteLine(mydata["new_mobilephone"]);


        using (var streamWriter = new StreamWriter(request.GetRequestStream()))
        {

            string postData = new JavaScriptSerializer().Serialize(new
            {
                new_mobilephone,
                checkCode = EncryptHelper.Encrypt("ESi"+(DateTime.Now.ToString("yyyyMMddHHmm").ToString())),
            });

            System.Diagnostics.Debug.WriteLine(postData);
            streamWriter.Write(postData);
        }

        // Get the request stream.
        //using (Stream dataStream = request.GetRequestStream())
        //{
        //    // Write the data to the request stream.
        //    dataStream.Write(byteArray, 0, byteArray.Length);
        //}




        // Get the response.      
        try
        {
            var response = (HttpWebResponse)request.GetResponse();
            using (var streamReader = new StreamReader(response.GetResponseStream()))
            {
                var result = streamReader.ReadToEnd();
                dynamic data = JObject.Parse(result);
                context.Response.Write(data);
            }
        }catch(Exception O)
        {
            context.Response.Write(O);
        }
        // rest of the logic
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    private static bool CheckValidationResult(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)

    {

        return true;

    }

}