﻿<%@ WebHandler Language="C#" Class="save_data" %>

using System;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Text.RegularExpressions;
using System.Collections;
using System.Web.SessionState;
using System.Net.Mail;
using System.Collections.Generic;
using System.Net.Mime;

public class save_data : IHttpHandler, IRequiresSessionState
{
    //private string conn = ConfigurationManager.ConnectionStrings["TConnString"].ToString();
    //測試正式皆是用ConnString
    private string conn = Config.ConnString;
    private string last_modify = DateTime.Now.GetDateTimeFormats('r')[0].ToString();
    private string expires = DateTime.Now.AddHours(1).GetDateTimeFormats('r')[0].ToString();

    //input
    public string FamilyName, GivenName, Sex, Birthday, Phone, Email, Income, Job, Session, Referrer,
            utm_source, utm_medium, utm_campaign, ip = "";
    //output
    private string _errcode = "";
    private string _errmsg = "";

    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "application/json";

        SqlConnection fmelconn_1;     //系統用
        SqlCommand fmelexec_1;        //系統用
        string Sql = "";

        if (context.Request.RequestType == "POST")
        {
            FamilyName = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["FamilyName"]) ? "" : context.Request["FamilyName"]);
            GivenName = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["GivenName"]) ? "" : context.Request["GivenName"]);
            Sex = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["Sex"]) ? "" : context.Request["Sex"]);
            Birthday = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["Birthday"]) ? "" : context.Request["Birthday"]);
            Phone = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["Phone"]) ? "" : context.Request["Phone"]);
            Email = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["Email"]) ? "" : context.Request["Email"]);
            Income = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["Income"]) ? "" : context.Request["Income"]);
            Job = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["Job"]) ? "" : context.Request["Job"]);
            Session = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["Session"]) ? "" : context.Request["Session"]);
            Referrer = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["Referrer"]) ? "" : context.Request["Referrer"]);
            utm_source = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["utm_source"]) ? "" : context.Request["utm_source"]);
            utm_medium = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["utm_medium"]) ? "" : context.Request["utm_medium"]);
            utm_campaign = ValidateHelper.FilteXSSValue(string.IsNullOrEmpty(context.Request["utm_campaign"]) ? "" : context.Request["utm_campaign"]);
            ip = IPNetworking.GetClientIPv4();


            if (IsReg(Phone,Session))
                _errmsg += "您已報名此場次，每場品酩會將隨機選出參加者，我們將在專人通知確認後，發送入場憑證簡訊，感謝您。\\n";
            if (IsExpired(Session))
                _errmsg += "此時段報名已截止，感謝您的支持 !\\n";
            //檢查參數
            if (_errmsg == "")
            {
                if (FamilyName == "")
                    _errmsg += "請輸入姓\\n";

                if (GivenName == "")
                    _errmsg += "請輸入名\\n";

                if (Sex == "")
                    _errmsg += "請選擇性別\\n";

                if (Birthday == "")
                    _errmsg += "請輸入生日\\n";

                if (Phone == "")
                    _errmsg += "請輸入手機\\n";
                else if (!ValidateHelper.IsMobile(Phone))
                    _errmsg += "手機號碼格式錯誤\\n";

                if (Email == "")
                    _errmsg += "請輸入Email\\n";
                else if (!ValidateHelper.IsEmail(Email))
                    _errmsg += "Email格式錯誤\\n";

                if (Income == "")
                    _errmsg += "請輸入收入\\n";

                if (Job == "")
                    _errmsg += "請輸入職業\\n";

                if (Session == "")
                    _errmsg += "請選擇場次\\n";


                if (!ValidateHelper.IsLenMToN(utm_source, 0, 50)) _errmsg += "utm_source不得超過50字\\n";
                if (!ValidateHelper.IsLenMToN(utm_medium, 0, 50)) _errmsg += "utm_medium不得超過50字\\n";
                if (!ValidateHelper.IsLenMToN(utm_campaign, 0, 50)) _errmsg += "utm_campaign不得超過50字\\n";


            }

            //Do SQL
            if (_errmsg == "")
            {
                //建立資料庫連結
                fmelconn_1 = new SqlConnection(conn);
                fmelconn_1.Open();
                fmelexec_1 = new SqlCommand();
                fmelexec_1.Connection = fmelconn_1;
                fmelexec_1.Parameters.Clear();
                Sql = @"INSERT INTO [dbo].[guest]
                                   ([FamilyName]
                                   ,[GivenName]
                                   ,[Sex]
                                   ,[Birthday]
                                   ,[Phone]
                                   ,[Email]
                                   ,[Income]
                                   ,[Job]
                                   ,[Session]
                                   ,[Referrer]
                                   ,[utm_source]
                                   ,[utm_medium]
                                   ,[utm_campaign]
                                   ,[ip]
                                   ,[CDate]
                                   ,[MDate])
                                VALUES
                                (@FamilyName,@GivenName,@Sex,@Birthday,@Phone,@Email,@Income,@Job,@Session,@Referrer,
                                @utm_source,@utm_medium,@utm_campaign,@ip,@CDate,@MDate);
                                SET @serno = @@IDENTITY;";
                //SQL 輸入方

                fmelexec_1.Parameters.Add("@FamilyName", SqlDbType.NVarChar, 50);
                fmelexec_1.Parameters.Add("@GivenName", SqlDbType.NVarChar, 50);

                fmelexec_1.Parameters.Add("@Sex", SqlDbType.NVarChar, 5);
                fmelexec_1.Parameters.Add("@Birthday", SqlDbType.DateTime);
                fmelexec_1.Parameters.Add("@Phone", SqlDbType.NVarChar, 20);
                fmelexec_1.Parameters.Add("@Email", SqlDbType.VarChar, 100);
                fmelexec_1.Parameters.Add("@Income", SqlDbType.NVarChar, 30);
                fmelexec_1.Parameters.Add("@Job", SqlDbType.NVarChar, 20);
                fmelexec_1.Parameters.Add("@Session", SqlDbType.NVarChar, 70);
                fmelexec_1.Parameters.Add("@Referrer", SqlDbType.NVarChar, 50);

                fmelexec_1.Parameters.Add("@utm_source", SqlDbType.NVarChar, 50);
                fmelexec_1.Parameters.Add("@utm_medium", SqlDbType.NVarChar, 50);
                fmelexec_1.Parameters.Add("@utm_campaign", SqlDbType.NVarChar, 50);
                fmelexec_1.Parameters.Add("@ip", SqlDbType.VarChar, 15);
                fmelexec_1.Parameters.Add("@CDate", SqlDbType.DateTime);
                fmelexec_1.Parameters.Add("@MDate", SqlDbType.DateTime);
                SqlParameter retValParam = fmelexec_1.Parameters.Add("@serno", SqlDbType.Int);
                retValParam.Direction = ParameterDirection.Output;

                fmelexec_1.Parameters["@FamilyName"].Value = FamilyName;
                fmelexec_1.Parameters["@GivenName"].Value = GivenName;
                fmelexec_1.Parameters["@Sex"].Value = Sex;
                fmelexec_1.Parameters["@Birthday"].Value = Birthday;
                fmelexec_1.Parameters["@Phone"].Value = Phone;
                fmelexec_1.Parameters["@Email"].Value = Email;
                fmelexec_1.Parameters["@Income"].Value = Income;
                fmelexec_1.Parameters["@Job"].Value = Job;
                fmelexec_1.Parameters["@Session"].Value = Session;
                fmelexec_1.Parameters["@Referrer"].Value = Referrer;
                fmelexec_1.Parameters["@utm_source"].Value = utm_source;
                fmelexec_1.Parameters["@utm_medium"].Value = utm_medium;
                fmelexec_1.Parameters["@utm_campaign"].Value = utm_campaign;
                fmelexec_1.Parameters["@ip"].Value = ip;
                fmelexec_1.Parameters["@CDate"].Value = DateTime.Now;
                fmelexec_1.Parameters["@MDate"].Value = DateTime.Now;

                //SQL 輸出方

                //SQL 準備
                fmelexec_1.CommandText = Sql;
                //取資料
                try
                {
                    fmelexec_1.ExecuteNonQuery();
                }
                catch (Exception o)
                {
                    _errmsg += "SQL Error";
                    throw (o);
                }
                finally
                {
                    fmelexec_1.Dispose();
                    fmelconn_1.Close();
                    fmelconn_1.Dispose();
                }
            }
            //回傳
            context.Response.AddHeader("Expires", expires);
            context.Response.AddHeader("Last-Modified", last_modify);
            context.Response.AddHeader("Cache-Control", "no-cache, must-revalidate");
            context.Response.AddHeader("Pragma", "no-cache");
            context.Response.AddHeader("x-frame-options", "DENY");
            context.Response.AddHeader("servertime", DateTime.Now.ToString("yyyyMMddHHmmss"));
            if (string.IsNullOrEmpty(_errmsg))
            {
                context.Response.Write("{\"Result\": true, \"ErrorMsg\": \"\"}");
            }
            else
            {
                context.Response.Write("{\"Result\": false, \"ErrorMsg\": \"" + _errmsg + "\"}");
            }
        }
        else
        {
            _errmsg = "Unsupported HTTP method";
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
    public bool IsReg(string _phone, string _session)
    {
        Database db = DBHelper.GetDatabase();
        string conditionSQL = "SELECT Count(*) FROM guest WHERE Phone = @phone AND Session = @session";
        DbCommand dbComm = db.GetSqlStringCommand(conditionSQL);
        db.AddInParameter(dbComm, "@Phone", DbType.String, Phone);
        db.AddInParameter(dbComm, "@Session", DbType.String, Session);
        DataTable dt = new DataTable();
        try
        {
            dt = db.ExecuteDataSet(dbComm).Tables[0];
        }
        catch (Exception ex)
        {
            throw (ex);
        }

        if (Convert.ToInt32(dt.Rows[0][0]) >= 1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public bool IsExpired(string _session)
    {
        //Set expire time
        Regex objGetDateFromString = new Regex("([0-9].{9})");
        Match match = objGetDateFromString.Match(_session);
        DateTime session_date = DateTime.ParseExact(match.Value, "yyyy/MM/dd", System.Globalization.CultureInfo.InvariantCulture);
        if (DateTime.Now.AddDays(2) >= session_date)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}