﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="list.aspx.cs" Inherits="backend_maltssociety_list" %>

<%@ Register Src="~/backend/uc/uc_leftMenu.ascx" TagPrefix="uc1" TagName="uc_leftMenu" %>
<%@ Register Src="~/backend/uc/uc_header.ascx" TagPrefix="uc1" TagName="uc_header" %>
<%@ Register Src="~/backend/uc/uc_pageheader.ascx" TagPrefix="uc1" TagName="uc_pageheader" %>
<%@ Register Src="~/backend/uc/uc_head.ascx" TagPrefix="uc1" TagName="uc_head" %>
<%@ Register Src="~/backend/uc/uc_foot.ascx" TagPrefix="uc1" TagName="uc_foot" %>



<!DOCTYPE html>
<html lang="en">
<head>
    <uc1:uc_head runat="server" ID="uc_head" />
    <style>
        /*在bootstrap.min.css 的table margin-bottom設為20px*/
        .table {
            margin-bottom: 0px;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            (function () {
                //複製Gridview_TopInfo Start
                var $infoBar = $("#gridview_topInfo");
                if (!!$infoBar.length) {
                    $('#gridview_bottomInfo').append($infoBar.clone(true).html());
                }
                //複製Gridview_TopInfo End
            })();
        });

        //複製的情況會造成…Form Data會傳送同name但不同value，會以第一個為主，所以要透過onChange事件修正第一個value
        function showcountChange(obj) {
            $("select[name='drpShowCount']").each(function (i) {
                $(this).val($(obj).val());
            });
        }
        $(function () {
            $("[id$=qStartDate]").datepicker({ dateFormat: 'yy/mm/dd' });
        });
        $(function () {
            $("[id$=qEndDate]").datepicker({ dateFormat: 'yy/mm/dd' });
        });
    </script>
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
    </div>

    <section>

        <uc1:uc_leftMenu runat="server" ID="uc_leftMenu" />
        <!-- leftpanel -->

        <div class="mainpanel">

            <uc1:uc_header runat="server" ID="uc_header" />
            <!-- headerbar -->

            <uc1:uc_pageheader runat="server" ID="uc_pageheader" />

            <div class="contentpanel">
                <form runat="server" id="listForm" class="form-inline">
                    <div class="well well-sm search-panel">
                        <i class="fa fa-pencil"></i>操作<br />
                        <div class="form-group">
                            <label class="">時段:</label>
                            <asp:DropDownList ID="qSession" runat="server" CssClass="form-control input-sm"
                                DataTextField="Session" DataValueField="Session" DataSourceID="ObjectDataSource2"  OnDataBound="qSession_DataBound">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <label class="">姓名:</label>
                            <asp:TextBox ID="qGivenName" runat="server" CssClass="form-control input-sm" placeholder="姓名"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label class="">Email:</label>
                            <asp:TextBox ID="qEmail" runat="server" CssClass="form-control input-sm" placeholder="Email"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label class="">排序:</label>
                            <asp:DropDownList ID="qSort" runat="server" CssClass="form-control input-sm">
                                <asp:ListItem Value="asc">時間先->後</asp:ListItem>
                                <asp:ListItem Value="desc">時間後->先</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <label class="">建立日期:</label>
                            <asp:TextBox ID="qStartDate" runat="server" CssClass="form-control input-sm" MaxLength="10" placeholder="建立日期(起)" autocomplete="off"></asp:TextBox>
                            <asp:Label ID="Label1" runat="server" Text="~"></asp:Label>
                            <asp:TextBox ID="qEndDate" runat="server" CssClass="form-control input-sm" MaxLength="10" placeholder="建立日期(迄)" autocomplete="off"></asp:TextBox>
                        </div>
                        <asp:Button ID="btnQuery" runat="server" CssClass="btn btn-success btn-sm" Text="搜尋" />
                        <asp:Button ID="exportExcel" runat="server" CssClass="btn btn-primary btn-sm"  DataSourceID="ObjectDataSource_excel"  OnClick="exportExcel_Click"  Text="匯出Excel" />
                        <asp:Button ID="exportClientExcel" runat="server" CssClass="btn btn-warning btn-sm"  DataSourceID="ObjectDataSource_client_excel"  OnClick="exportClientExcel_Click"  Text="匯出客戶Excel" />
                        <%--<asp:Button ID="btnExcel" runat="server" CssClass="btn btn-primary btn-sm " Text="匯出Excel" OnClick="btnExcel_Click" />--%>
                        <%--<asp:Button ID="btnExcelGenerally" runat="server" CssClass="btn btn-primary btn-sm" Text="匯出Excel to 客戶" OnClick="btnExcelGenerally_Click" />--%>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div id="gridview_topInfo">
                                每頁顯示筆數
                                <asp:DropDownList runat="server" ID="drpShowCount" AutoPostBack="True" OnSelectedIndexChanged="drpShowCount_SelectedIndexChanged" onChange="showcountChange(this);">
                                    <asp:ListItem>10</asp:ListItem>
                                    <asp:ListItem>20</asp:ListItem>
                                    <asp:ListItem>30</asp:ListItem>
                                </asp:DropDownList>
                                搜尋資料總筆數
                                <asp:Label runat="server" ID="lblTotalCount"></asp:Label>
                            </div>
                            <div style="overflow-x:auto;">
                                <asp:GridView ID="GridView1" runat="server" CssClass="table table-primary responsive" DataKeyNames="Serno" GridLines="None"
                                    AllowPaging="true" PageSize="10" AllowSorting="true" OnRowCreated="GridView1_RowCreated" OnPageIndexChanging="GridView1_PageIndexChanging" OnPreRender="GridView1_PreRender"
                                    AutoGenerateColumns="false" DataSourceID="ObjectDataSource1" OnRowUpdating="GridView1_RowUpdating" PagerStyle-CssClass="bs-pagination text-right">
                                    <EmptyDataTemplate>查無資料</EmptyDataTemplate>
                                    <PagerTemplate>
                                        <asp:PlaceHolder ID="placeholderPager" runat="server"></asp:PlaceHolder>
                                    </PagerTemplate>
                                    <Columns>
                                        <asp:BoundField DataField="Serno" HeaderText="#"></asp:BoundField>
                                        <asp:BoundField DataField="FamilyName" HeaderText="姓"></asp:BoundField>
                                        <asp:BoundField DataField="GivenName" HeaderText="名"></asp:BoundField>
                                        <asp:BoundField DataField="Sex" HeaderText="性別"></asp:BoundField>
                                        <asp:BoundField DataField="Birthday" DataFormatString="{0:yyyy/MM/dd}" HeaderText="生日"></asp:BoundField>
                                        <asp:BoundField DataField="Phone" HeaderText="手機號碼"></asp:BoundField>
                                        <asp:BoundField DataField="Email" HeaderText="Email"></asp:BoundField>
                                        <asp:BoundField DataField="Income" HeaderText="收入"></asp:BoundField>
                                        <asp:BoundField DataField="Job" HeaderText="職業"></asp:BoundField>
                                        <asp:BoundField DataField="Session" HeaderText="時段"></asp:BoundField>
                                        <asp:BoundField DataField="Referrer" HeaderText="推薦人"></asp:BoundField>
                                        <asp:BoundField DataField="CDate" DataFormatString="{0:yyyy/MM/dd HH:mm}" HeaderText="建立日期"></asp:BoundField>
                                        <asp:BoundField DataField="utm_source" HeaderText="utm_source"></asp:BoundField>
                                        <asp:BoundField DataField="utm_medium" HeaderText="utm_medium"></asp:BoundField>
                                        <asp:BoundField DataField="utm_campaign" HeaderText="utm_campaign"></asp:BoundField>
                                        <%--<asp:BoundField DataField="ip" HeaderText="IP"></asp:BoundField>--%>
                                        <%--<asp:BoundField DataField="MDate" DataFormatString="{0:yyyy/MM/dd HH:mm}" HeaderText="更新日期"></asp:BoundField>--%>
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div id="gridview_bottomInfo">
                                <%--用來放尾巴資訊--%>
                            </div>
                            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="Backend_DBMaltsSociety2018" EnablePaging="true"
                                MaximumRowsParameterName="maxrows"
                                StartRowIndexParameterName="startrows"
                                SelectMethod="GetList"
                                SelectCountMethod="GetCounts">

                                <SelectParameters>
                                    <asp:ControlParameter ControlID="qSession" Name="qSession" PropertyName="Text" Type="String" />
                                    <asp:ControlParameter ControlID="qGivenName" Name="qGivenName" PropertyName="Text" Type="String" />
                                    <asp:ControlParameter ControlID="qEmail" Name="qEmail" PropertyName="Text" Type="String" />
                                    <asp:ControlParameter ControlID="qStartDate" Name="sDate" PropertyName="Text" Type="String" />
                                    <asp:ControlParameter ControlID="qEndDate" Name="eDate" PropertyName="Text" Type="String" />
                                    <asp:ControlParameter ControlID="qSort" Name="qSort" PropertyName="Text" Type="String" />
                                </SelectParameters>
                                <%--OnSelected="ObjectDataSource1_Selected"--%>
<%--                                <SelectParameters>
                                    <asp:ControlParameter ControlID="qSMS" Name="sms" PropertyName="SelectedValue" Type="String" />
                                </SelectParameters>--%>
                            </asp:ObjectDataSource>
                            <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" TypeName="Backend_DBMaltsSociety2018" SelectMethod="GetSessionList">
                                <SelectParameters></SelectParameters>
                            </asp:ObjectDataSource>
                            <asp:ObjectDataSource ID="ObjectDataSource_excel" runat="server" TypeName="Backend_DBMaltsSociety2018" SelectMethod="GetExcel">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="qSession" Name="qSession" PropertyName="Text" Type="String" />
                                    <asp:ControlParameter ControlID="qGivenName" Name="qGivenName" PropertyName="Text" Type="String" />
                                    <asp:ControlParameter ControlID="qEmail" Name="qEmail" PropertyName="Text" Type="String" />
                                    <asp:ControlParameter ControlID="qStartDate" Name="sDate" PropertyName="Text" Type="String" />
                                    <asp:ControlParameter ControlID="qEndDate" Name="eDate" PropertyName="Text" Type="String" />
                                    <asp:ControlParameter ControlID="qSort" Name="qSort" PropertyName="Text" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                            <asp:ObjectDataSource ID="ObjectDataSource_client_excel" runat="server" TypeName="Backend_DBMaltsSociety2018" SelectMethod="GetClientExcel">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="qSession" Name="qSession" PropertyName="Text" Type="String" />
                                    <asp:ControlParameter ControlID="qGivenName" Name="qGivenName" PropertyName="Text" Type="String" />
                                    <asp:ControlParameter ControlID="qEmail" Name="qEmail" PropertyName="Text" Type="String" />
                                    <asp:ControlParameter ControlID="qStartDate" Name="sDate" PropertyName="Text" Type="String" />
                                    <asp:ControlParameter ControlID="qEndDate" Name="eDate" PropertyName="Text" Type="String" />
                                    <asp:ControlParameter ControlID="qSort" Name="qSort" PropertyName="Text" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </div>
                    </div>
                </form>
                <!-- row -->
            </div>
            <!-- contentpanel -->

        </div>
        <!-- mainpanel -->
    </section>
    <uc1:uc_foot runat="server" ID="uc_foot" />
    <script type="text/javascript" src='<%= ResolveUrl("~/backend/asset/js/bs.pagination.js") %>'></script>
</body>
</html>

