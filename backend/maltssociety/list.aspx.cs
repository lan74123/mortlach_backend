﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backend_maltssociety_list : BackendBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnInsert_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("edit.aspx");
    }

    protected void GridView1_RowUpdating(object sender, System.Web.UI.WebControls.GridViewUpdateEventArgs e)
    {

    }
    protected void drpShowCount_SelectedIndexChanged(object sender, EventArgs e)
    {
        //在GridView.PageIndex 無法正確被計算出的情況，切換顯示數目會有問題，所以切換至第一頁就沒問題
        DropDownList ddlpagesize = (DropDownList)sender;
        GridView1.PageSize = Convert.ToInt16(ddlpagesize.SelectedValue);
        GridView1.PageIndex = 0;
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Pager)
        {
            // 取得控制項
            GridView gv = sender as GridView;
            PlaceHolder placeholder = e.Row.FindControl("placeholderPager") as PlaceHolder;

            //一次要顯示的頁數
            int showRange = 5;
            //分頁的數量
            int pageCount = gv.PageCount;
            //目前頁碼 (起始值為0)
            int pageIndex = gv.PageIndex;
            //顯示的第一個頁碼
            int startIndex = (pageIndex + 1 < showRange) ?
                0 : (pageIndex + 1 + showRange / 2 >= pageCount) ? pageCount - showRange : pageIndex - showRange / 2;
            //顯示的最後一個頁碼
            int endIndex = (startIndex >= pageCount - showRange) ? pageCount : startIndex + showRange;

            LinkButton lbtnfirst = new LinkButton();
            LinkButton lbtnlast = new LinkButton();

            LinkButton lbtnprev = new LinkButton();
            LinkButton lbtnnext = new LinkButton();

            LinkButton lbtnpage;
            TableRow tr;
            TableCell td;

            Table table = new Table();

            //新增第一頁 Start
            lbtnfirst.CommandName = "Page";
            lbtnfirst.CommandArgument = "1";
            lbtnfirst.Text = "&lt;&lt;";
            //新增第一頁 End

            //新增上一頁 Start
            lbtnprev.Text = "&lt;";
            //新增上一頁 End

            #region 往前處理 (可否點選以及加入至Table)
            if (gv.PageIndex > 0)
            {
                lbtnprev.Click += (obj, args) =>
                {
                    gv.PageIndex = gv.PageIndex - 1;
                };
            }
            else
            {
                lbtnprev.Enabled = false;
                lbtnprev.CssClass = "disabled";
                lbtnfirst.Enabled = false;
                lbtnfirst.CssClass = "disabled";
            }

            tr = new TableRow();
            td = new TableCell();

            td.Controls.Add(lbtnfirst);
            tr.Cells.Add(td);
            table.Rows.Add(tr);

            tr = new TableRow();
            td = new TableCell();

            td.Controls.Add(lbtnprev);
            tr.Cells.Add(td);
            table.Rows.Add(tr);
            #endregion

            #region 新增頁碼數字
            for (int i = startIndex; i < endIndex; i++)
            {
                lbtnpage = new LinkButton();
                lbtnpage.Text = (i + 1).ToString();
                lbtnpage.CommandName = "Page";
                lbtnpage.CommandArgument = (i + 1).ToString();
                lbtnpage.Font.Overline = false;
                if (i == pageIndex)
                {
                    lbtnpage.Font.Bold = true;
                    lbtnpage.Enabled = false;
                }
                else
                {
                    lbtnpage.Font.Bold = false;
                }

                tr = new TableRow();
                td = new TableCell();

                td.Controls.Add(lbtnpage);
                tr.Cells.Add(td);
                table.Rows.Add(tr);
            }
            #endregion

            //新增下一頁 Start            
            lbtnnext.Text = "&gt;";
            //新增下一頁 End

            //新增末頁 Start
            lbtnlast.CommandName = "Page";
            lbtnlast.CommandArgument = GridView1.PageCount.ToString();
            lbtnlast.Text = "&gt;&gt;";
            //新增末頁 End

            #region 往後處理 (可否點選以及加入至Table)
            if (gv.PageIndex < gv.PageCount)
            {
                lbtnnext.Click += (obj, args) =>
                {
                    gv.PageIndex = gv.PageIndex + 1;
                };
            }

            if (gv.PageIndex + 1 == gv.PageCount)
            {
                lbtnnext.Enabled = false;
                lbtnnext.CssClass = "disabled";
                lbtnlast.Enabled = false;
                lbtnlast.CssClass = "disabled";
            }

            tr = new TableRow();
            td = new TableCell();

            td.Controls.Add(lbtnnext);
            tr.Cells.Add(td);
            table.Rows.Add(tr);

            tr = new TableRow();
            td = new TableCell();

            td.Controls.Add(lbtnlast);
            tr.Cells.Add(td);
            table.Rows.Add(tr);
            #endregion

            // 動態加入控制項
            //phdPageNumber.Controls.Add(
            //    new LiteralControl(string.Format("總頁數  {0} / {1}", pageIndex + 1, pageCount)));
            placeholder.Controls.Add(table);
        }
    }
    protected void ObjectDataSource1_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        //依序進入的事件
        //SelectMethod
        //SelectCountMethod
        //利用型別判斷將第一次非數字的回傳值繞過
        if (e.ReturnValue.GetType() != typeof(DataTable))
        {
            string intCount = e.ReturnValue.ToString();
            lblTotalCount.Text = "共" + intCount + "筆";
        }
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
    }
    protected void GridView1_PreRender(object sender, EventArgs e)
    {
        //為了產生 Table 的 thead
        GridView1.UseAccessibleHeader = true;
        if (GridView1.HeaderRow != null)
        {
            GridView1.HeaderRow.TableSection = TableRowSection.TableHeader;
        }
    }

	protected void btnExcel_Click(object sender, EventArgs e)
	{
		DataTable dt = Backend_DBMaltsSociety2018.Export();
		string t = DateTime.Now.ToString("yyyyMMddHHmm");
		ExcelHelper.ToExcel(dt, "maltssociety_export_" + t + ".xls", Page.Response);
	}

	protected void btnExcelGenerally_Click(object sender, EventArgs e)
	{
		DataTable dt = Backend_DBMaltsSociety2018.ExportGenerally();
		string t = DateTime.Now.ToString("yyyyMMddHHmm");
		ExcelHelper.ToExcel(dt, "maltssociety_export_generally_" + t + ".xls", Page.Response);
	}

    protected void qSession_DataBound(object sender, EventArgs e)
    {
        var ctrl = (DropDownList)sender;
        ctrl.Items.Insert(0, new ListItem("不拘", ""));
    }

    protected void exportExcel_Click(object sender, EventArgs e)
    {
        var ds = new DataSet();
        var dt3 = ((DataView)ObjectDataSource_excel.Select()).ToTable();
        ds.Tables.Add(dt3);
        string t = DateTime.Now.ToString("yyyyMMddHHmm");
        foreach (DataTable dt in ds.Tables)
        {
            dt.TableName = "Data";
        }
        ExcelHelper.ToExcel(ds, "maltssociety_export_" + t + ".xls", Page.Response);
    }

    protected void exportClientExcel_Click(object sender, EventArgs e)
    {
        var ds = new DataSet();
        var dt3 = ((DataView)ObjectDataSource_client_excel.Select()).ToTable();
        ds.Tables.Add(dt3);
        string t = DateTime.Now.ToString("yyyyMMddHHmm");
        foreach (DataTable dt in ds.Tables)
        {
            dt.TableName = "Data";
        }
        ExcelHelper.ToExcel(ds, "for_client_maltssociety_export_" + t + ".xls", Page.Response);
    }
}