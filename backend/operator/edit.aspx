﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="edit.aspx.cs" Inherits="backend_operator_edit" %>

<%@ Register Src="~/backend/uc/uc_leftMenu.ascx" TagPrefix="uc1" TagName="uc_leftMenu" %>
<%@ Register Src="~/backend/uc/uc_header.ascx" TagPrefix="uc1" TagName="uc_header" %>
<%@ Register Src="~/backend/uc/uc_pageheader.ascx" TagPrefix="uc1" TagName="uc_pageheader" %>
<%@ Register Src="~/backend/uc/uc_head.ascx" TagPrefix="uc1" TagName="uc_head" %>
<%@ Register Src="~/backend/uc/uc_foot.ascx" TagPrefix="uc1" TagName="uc_foot" %>






<!DOCTYPE html>
<html lang="en">
<head>
    <uc1:uc_head runat="server" ID="uc_head" />
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
    </div>

    <section>

        <uc1:uc_leftMenu runat="server" ID="uc_leftMenu" />
        <!-- leftpanel -->

        <div class="mainpanel">

            <uc1:uc_header runat="server" ID="uc_header" />
            <!-- headerbar -->

            <uc1:uc_pageheader runat="server" ID="uc_pageheader" />

            <div class="contentpanel">

                <div class="row">
                    <div class="col-md-12">
                        <form id="EditForm" runat="server" class="form-horizontal">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><%= html_panelTitle %></h4>
                                </div>
                                <div class="panel-body">
                                    <asp:DetailsView ID="DetailsView1" runat="server" DataKeyNames="serno" CssClass="table"
                                        AutoGenerateRows="false" DefaultMode="Edit" DataSourceID="ObjectDataSource1"
                                        OnItemUpdating="DetailsView1_ItemUpdating" OnItemUpdated="DetailsView1_ItemUpdated"
                                        OnItemInserted="DetailsView1_ItemInserted" OnItemInserting="DetailsView1_ItemInserting"
                                        OnDataBound="DetailsView1_DataBound" GridLines="None">
                                        <Fields>
                                            <asp:TemplateField>
                                                <InsertItemTemplate>
                                                    <div style="text-align: right">
                                                        <asp:Button ID="btnInsert" runat="server" Text="確定新增" CommandName="Insert" CssClass="btn btn-primary btn-xs" />
                                                        <asp:Button ID="btnBackTo" runat="server" Text="回查詢" OnClick="btnCancel_Click" CausesValidation="false" CssClass="btn btn-default btn-xs" />
                                                    </div>
                                                </InsertItemTemplate>
                                                <EditItemTemplate>
                                                    <div style="text-align: right">
                                                        <asp:Button ID="btnUpdate" runat="server" Text="確定修改" CommandName="Update" CssClass="btn btn-primary btn-xs" />
                                                        <asp:Button ID="btnBackTo" runat="server" Text="回查詢" OnClick="btnCancel_Click" CausesValidation="false" CssClass="btn btn-default btn-xs" />
                                                    </div>
                                                </EditItemTemplate>
                                            </asp:TemplateField>

                                            <asp:BoundField DataField="serno" HeaderText="操作員代碼" ReadOnly="True" InsertVisible="False" HeaderStyle-CssClass="col-md-2" />

                                            <asp:TemplateField HeaderText="操作員姓名" HeaderStyle-CssClass="col-md-2">
                                                <EditItemTemplate>
                                                    <div class="col-md-3 nopadding">
                                                        <asp:TextBox ID="name" runat="server" Text='<%# Bind("name") %>' MaxLength="20" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <div class="col-md-3 nopadding">
                                                        <asp:TextBox ID="name" runat="server" Text='<%# Bind("name") %>' MaxLength="20" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </InsertItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="帳號">
                                                <EditItemTemplate>
                                                    <asp:Label ID="username" runat="server" Text='<%# Eval("username") %>'></asp:Label>
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <div class="col-md-3 nopadding">
                                                        <asp:TextBox ID="username" runat="server" Text='<%# Bind("username") %>' MaxLength="20" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </InsertItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="密碼">
                                                <EditItemTemplate>
                                                    <div class="col-md-3 nopadding">
                                                        <asp:TextBox ID="password" runat="server" TextMode="Password" MaxLength="20" Text="" CssClass="form-control" />
                                                    </div>
                                                    <asp:TextBox ID="pass" runat="server" Visible="false" Text='<%# ValidateHelper.DecryptString(Eval("password").ToString()) %>' />
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <div class="col-md-3 nopadding">
                                                        <asp:TextBox ID="password" runat="server" TextMode="Password" MaxLength="20" Text="" CssClass="form-control" />
                                                    </div>
                                                </InsertItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="有效否">
                                                <EditItemTemplate>
                                                    <asp:CheckBox ID="valid" runat="server" Checked='<%# Eval("valid").ToString()=="0" ? false :true %>' />
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                    <asp:CheckBox ID="valid" runat="server" Checked='true' />
                                                </InsertItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="CDate" HeaderText="建立日期" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"
                                                ReadOnly="True" InsertVisible="False"></asp:BoundField>
                                            <asp:BoundField DataField="MDate" HeaderText="最後修改日期" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}"
                                                ReadOnly="True" InsertVisible="False"></asp:BoundField>
                                            <asp:TemplateField HeaderText="最後修改人員" InsertVisible="false">
                                                <EditItemTemplate>
                                                    <asp:Label ID="fOid" runat="server" Text='<%# Backend_DBOperator.GetNameBySerno(Eval("oid").ToString()) %>' />
                                                </EditItemTemplate>
                                                <InsertItemTemplate>
                                                </InsertItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <InsertItemTemplate>
                                                    <div style="text-align: right">
                                                        <asp:Button ID="btnInsert_foot" runat="server" Text="確定新增" CommandName="Insert" CssClass="btn btn-primary btn-xs" />
                                                        <asp:Button ID="btnBackTo_foot" runat="server" Text="回查詢" OnClick="btnCancel_Click" CausesValidation="false" CssClass="btn btn-default btn-xs" />
                                                    </div>
                                                </InsertItemTemplate>
                                                <EditItemTemplate>
                                                    <div style="text-align: right">
                                                        <asp:Button ID="btnUpdate_foot" runat="server" Text="確定修改" CommandName="Update" CssClass="btn btn-primary btn-xs" />
                                                        <asp:Button ID="btnBackTo_foot" runat="server" Text="回查詢" OnClick="btnCancel_Click" CausesValidation="false" CssClass="btn btn-default btn-xs" />
                                                    </div>
                                                </EditItemTemplate>
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>
                                </div>
                                <!-- panel-body -->
                            </div>
                            <!-- panel-default -->

                            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="Backend_DBOperator" SelectMethod="GetListBySerno" InsertMethod="Insert" UpdateMethod="Update">
                                <SelectParameters>
                                    <asp:QueryStringParameter Name="serno" QueryStringField="serno" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </form>
                    </div>
                </div>
                <!-- row -->
            </div>
            <!-- contentpanel -->

        </div>
        <!-- mainpanel -->
    </section>

    <uc1:uc_foot runat="server" ID="uc_foot" />

</body>
</html>

