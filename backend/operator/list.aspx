﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="list.aspx.cs" Inherits="backend_operator_list" %>

<%@ Register Src="~/backend/uc/uc_leftMenu.ascx" TagPrefix="uc1" TagName="uc_leftMenu" %>
<%@ Register Src="~/backend/uc/uc_header.ascx" TagPrefix="uc1" TagName="uc_header" %>
<%@ Register Src="~/backend/uc/uc_pageheader.ascx" TagPrefix="uc1" TagName="uc_pageheader" %>
<%@ Register Src="~/backend/uc/uc_head.ascx" TagPrefix="uc1" TagName="uc_head" %>
<%@ Register Src="~/backend/uc/uc_foot.ascx" TagPrefix="uc1" TagName="uc_foot" %>



<!DOCTYPE html>
<html lang="en">
<head>
    <uc1:uc_head runat="server" ID="uc_head" />
    <style>
        /*在bootstrap.min.css 的table margin-bottom設為20px*/
        .table {
            margin-bottom: 0px;
        }
    </style>
    <script type="text/javascript">
        $(function () {
            (function () {
                //複製Gridview_TopInfo Start
                var $infoBar = $("#gridview_topInfo");
                if (!!$infoBar.length) {
                    $('#gridview_bottomInfo').append($infoBar.clone(true).html());
                }
                //複製Gridview_TopInfo End
            })();
        });

        //複製的情況會造成…Form Data會傳送同name但不同value，會以第一個為主，所以要透過onChange事件修正第一個value
        function showcountChange(obj) {
            $("select[name='drpShowCount']").each(function (i) {
                $(this).val($(obj).val());
            });
        }
    </script>
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
    </div>

    <section>

        <uc1:uc_leftMenu runat="server" ID="uc_leftMenu" />
        <!-- leftpanel -->

        <div class="mainpanel">

            <uc1:uc_header runat="server" ID="uc_header" />
            <!-- headerbar -->

            <uc1:uc_pageheader runat="server" ID="uc_pageheader" />

            <div class="contentpanel">
                <form runat="server" id="listForm" class="form-inline">
                    <div class="well well-sm search-panel">
                        <i class="glyphicon glyphicon-search"></i>搜尋<br />
                        <div class="form-group">
                            <asp:TextBox ID="qName" runat="server" CssClass="form-control input-sm" placeholder="操作員名"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label class="">有效否:</label>
                            <asp:DropDownList ID="qValid" runat="server" CssClass="form-control input-sm">
                                <asp:ListItem Value="1">是</asp:ListItem>
                                <asp:ListItem Value="0">否</asp:ListItem>
                                <asp:ListItem Value="">不拘</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <asp:Button ID="btnQuery" runat="server" CssClass="btn btn-default btn-sm" Text="查詢" />
                        <asp:Button ID="btnInsert" runat="server" CssClass="btn btn-success btn-sm pull-right" Text="建立新資料" OnClick="btnInsert_Click" />
                    </div>
                    <div class="row">
                        <div class="col-sm-12 col-md-12">
                            <div id="gridview_topInfo">
                                每頁顯示筆數
                                <asp:DropDownList runat="server" ID="drpShowCount" AutoPostBack="True" OnSelectedIndexChanged="drpShowCount_SelectedIndexChanged" onChange="showcountChange(this);">
                                    <asp:ListItem>10</asp:ListItem>
                                    <asp:ListItem>20</asp:ListItem>
                                    <asp:ListItem>30</asp:ListItem>
                                </asp:DropDownList>
                                搜尋資料總筆數
                                <asp:Label runat="server" ID="lblTotalCount"></asp:Label>
                            </div>
                            <div class="table-responsive">
                                <asp:GridView ID="GridView1" runat="server" CssClass="table table-success" DataKeyNames="serno" GridLines="None"
                                    AllowPaging="true" PageSize="10" AllowSorting="true" OnRowCreated="GridView1_RowCreated" OnPageIndexChanging="GridView1_PageIndexChanging" OnPreRender="GridView1_PreRender"
                                    AutoGenerateColumns="false" DataSourceID="ObjectDataSource1" OnRowUpdating="GridView1_RowUpdating" PagerStyle-CssClass="bs-pagination text-right">
                                    <EmptyDataTemplate>查無資料</EmptyDataTemplate>
                                    <PagerTemplate>
                                        <asp:PlaceHolder ID="placeholderPager" runat="server"></asp:PlaceHolder>
                                    </PagerTemplate>
                                    <Columns>
                                        <asp:BoundField DataField="serno" HeaderText="#" HeaderStyle-CssClass="col-md-1"></asp:BoundField>
                                        <asp:BoundField DataField="name" HeaderText="操作員名">
                                            <ControlStyle Width="60px"></ControlStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="username" HeaderText="帳號">
                                            <ControlStyle Width="100px"></ControlStyle>
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="有效否">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="fValid" runat="server" Enabled="false" Checked='<%# DBHelper.GetTrueOrFalse(Eval("Valid").ToString()) %>'></asp:CheckBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="cdate" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HeaderText="建立日期">
                                            <ControlStyle Width="200px"></ControlStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="mdate" DataFormatString="{0:yyyy/MM/dd HH:mm:ss}" HeaderText="最後修改日期">
                                            <ControlStyle Width="200px"></ControlStyle>
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="" ShowHeader="false" ItemStyle-VerticalAlign="Middle" ItemStyle-HorizontalAlign="Left" HeaderStyle-CssClass="col-md-2">
                                            <ItemTemplate>
                                                <a class="btn btn-warning btn-xs" href='edit.aspx?serno=<%# Eval("Serno") %>' target="_self">編輯</a>
                                                <asp:Button ID="btnUpdate" runat="server" CssClass="btn btn-danger btn-xs" Text="無效" Enabled='<%# DBHelper.GetTrueOrFalse(Eval("Valid").ToString()) %>' OnClientClick='return confirm("您確定要將此筆記錄變成無效嗎?")' CommandName="Update" CausesValidation="true"></asp:Button>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div id="gridview_bottomInfo">
                                <%--用來放尾巴資訊--%>
                            </div>
                            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" TypeName="Backend_DBOperator" EnablePaging="true"
                                MaximumRowsParameterName="maxrows"
                                StartRowIndexParameterName="startrows"
                                SelectMethod="GetList"
                                SelectCountMethod="GetCounts"
                                OnSelected="ObjectDataSource1_Selected"
                                UpdateMethod="UpdateValid">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="qName" Name="name" PropertyName="Text" Type="String" />
                                    <asp:ControlParameter ControlID="qValid" Name="valid" PropertyName="SelectedValue" Type="String" />
                                </SelectParameters>
                            </asp:ObjectDataSource>
                        </div>
                    </div>
                </form>
                <!-- row -->
            </div>
            <!-- contentpanel -->

        </div>
        <!-- mainpanel -->
    </section>
    <uc1:uc_foot runat="server" ID="uc_foot" />
    <script type="text/javascript" src='<%= ResolveUrl("~/backend/asset/js/bs.pagination.js") %>'></script>
</body>
</html>

