﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="password.aspx.cs" Inherits="backend_operator_password" %>

<%@ Register Src="~/backend/uc/uc_leftMenu.ascx" TagPrefix="uc1" TagName="uc_leftMenu" %>
<%@ Register Src="~/backend/uc/uc_header.ascx" TagPrefix="uc1" TagName="uc_header" %>
<%@ Register Src="~/backend/uc/uc_pageheader.ascx" TagPrefix="uc1" TagName="uc_pageheader" %>
<%@ Register Src="~/backend/uc/uc_head.ascx" TagPrefix="uc1" TagName="uc_head" %>
<%@ Register Src="~/backend/uc/uc_foot.ascx" TagPrefix="uc1" TagName="uc_foot" %>



<!DOCTYPE html>
<html lang="en">
<head>
    <uc1:uc_head runat="server" ID="uc_head" />
</head>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
    </div>

    <section>

        <uc1:uc_leftMenu runat="server" ID="uc_leftMenu" />
        <!-- leftpanel -->

        <div class="mainpanel">

            <uc1:uc_header runat="server" ID="uc_header" />
            <!-- headerbar -->

            <uc1:uc_pageheader runat="server" ID="uc_pageheader" />

            <div class="contentpanel">

                <div class="row">

                    <div class="col-sm-12 col-md-12">

                        <form runat="server" id="PasswordForm" class="form-horizontal">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">修改密碼</h4>
                                    <%--<p>Basic form with a class name of <code>.form-horizontal</code>.</p>--%>
                                </div>
                                <div class="panel-body">

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">新的密碼:</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">再一次確認密碼:</label>
                                        <div class="col-sm-4">
                                            <asp:TextBox ID="txtPassword2" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                                <!-- panel-body -->
                                <div class="panel-footer" style="text-align:right">
                                    <asp:Button ID="btnOK" runat="server" Text="修改" OnClick="btnOK_Click" CssClass="btn btn-primary btn-xs"></asp:Button>
                                </div>
                                <!-- panel-footer -->
                            </div>
                            <!-- panel-default -->
                        </form>
                    </div>
                </div>

                <!-- row -->
            </div>
            <!-- contentpanel -->

        </div>
        <!-- mainpanel -->
    </section>
    <uc1:uc_foot runat="server" ID="uc_foot" />
</body>
</html>

