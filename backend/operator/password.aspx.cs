﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backend_operator_password : BackendBasePage
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnOK_Click(object sender, EventArgs e)
    {
        if (!IsRefresh)
        {
            string in_txtPassword = ValidateHelper.FilteXSSValue(txtPassword.Text);
            string in_txtPassword2 = ValidateHelper.FilteXSSValue(txtPassword2.Text);

            if (string.IsNullOrEmpty(in_txtPassword))
            {
                sys_msg += "請輸入「新的密碼」!\\r";
            }

            if (string.IsNullOrEmpty(in_txtPassword2))
            {
                sys_msg += "請輸入「再一次確認密碼」!\\r";
            }

            if (in_txtPassword != in_txtPassword2)
            {
                sys_msg += "輸入的「密碼」不一致!\\r";
            }

            if (!string.IsNullOrEmpty(sys_msg))
            {
                JSOutPut(sys_msg);
            }
            else
            {
                Backend_DBOperator DBOperator = new Backend_DBOperator();
                DBOperator.UpdatePassword(ValidateHelper.EncryptString(in_txtPassword));
                JSOutPut("修改成功");
            }
        }
    }
}