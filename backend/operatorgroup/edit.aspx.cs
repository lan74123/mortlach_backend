﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class backend_operatorgroup_edit : BackendBasePage
{
    public string html_panelTitle { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (string.IsNullOrEmpty(Request.Params["serno"]))
            {
                //---------------------------------------------------------新增
                html_panelTitle = "新增";
                DetailsView1.ChangeMode(DetailsViewMode.Insert);
            }
            else
            {
                //---------------------------------------------------------編輯
                html_panelTitle = "編輯";
                DetailsView1.ChangeMode(DetailsViewMode.Edit);
            }
        }
    }

    protected void btnCancel_Click(object sender, System.EventArgs e)
    {
        Response.Redirect("list.aspx");
    }

    
    protected void DetailsView1_ItemInserting(object sender, System.Web.UI.WebControls.DetailsViewInsertEventArgs e)
    {
        if (!IsRefresh)
        {
            //前置
            sys_msg = string.Empty;

            //控制項
            TextBox txtname = (TextBox)DetailsView1.FindControl("name");
            CheckBox cbvalid = (CheckBox)DetailsView1.FindControl("valid");

            //變數
            string in_name = ValidateHelper.FilteXSSValue(txtname.Text);
            string in_funlist = ValidateHelper.FilteXSSValue(hidFunc.Value);
            string in_usrlist = ValidateHelper.FilteXSSValue(hidUsr.Value);
            string in_valid = cbvalid.Checked ? "1" : "0";

            //檢查
            //必填欄位檢查
            if (string.IsNullOrEmpty(in_name))
            {
                sys_msg += "請輸入「操作員姓名」!\\r";
            }
            if (string.IsNullOrEmpty(in_funlist))
            {
                sys_msg += "請選擇該群組擁有的「功能」!\\r";
            }
            if (string.IsNullOrEmpty(in_usrlist))
            {
                sys_msg += "請選擇該群組擁有的「對象」!\\r";
            }

            //提交
            if (!string.IsNullOrEmpty(sys_msg))
            {
                e.Cancel = true;
                JSOutPut(sys_msg);
            }
            else
            {
                e.Values["name"] = in_name;
                e.Values["funlist"] = in_funlist;
                e.Values["usrlist"] = in_usrlist;
                e.Values["valid"] = in_valid;
            }
        }
        else
        {
            e.Cancel = true;
        }
    }

    protected void DetailsView1_ItemInserted(object sender, System.Web.UI.WebControls.DetailsViewInsertedEventArgs e)
    {
        if (e.Exception == null)
        {
            e.KeepInInsertMode = true;
            JSOutPutAndRedirect("新增成功", "list.aspx");
        }
        else
        {
            e.ExceptionHandled = true;
            e.KeepInInsertMode = true;
            throw (e.Exception);
        }
    }

    protected void DetailsView1_ItemUpdating(object sender, System.Web.UI.WebControls.DetailsViewUpdateEventArgs e)
    {
        if (!IsRefresh)
        {
            //前置
            sys_msg = string.Empty;

            //控制項
            TextBox txtname = (TextBox)DetailsView1.FindControl("name");
            CheckBox cbvalid = (CheckBox)DetailsView1.FindControl("valid");

            //變數
            string in_name = ValidateHelper.FilteXSSValue(txtname.Text);
            string in_valid = cbvalid.Checked ? "1" : "0";
            string in_funlist = ValidateHelper.FilteXSSValue(hidFunc.Value);
            string in_usrlist = ValidateHelper.FilteXSSValue(hidUsr.Value);

            //檢查
            //必填欄位檢查
            if (string.IsNullOrEmpty(in_name))
            {
                sys_msg += "請輸入「操作員姓名」!\\r";
            }
            if (string.IsNullOrEmpty(in_funlist))
            {
                sys_msg += "請選擇該群組擁有的「功能」!\\r";
            }
            if (string.IsNullOrEmpty(in_usrlist))
            {
                sys_msg += "請選擇該群組擁有的「對象」!\\r";
            }

            //提交
            if (!string.IsNullOrEmpty(sys_msg))
            {
                e.Cancel = true;
                JSOutPut(sys_msg);
            }
            else
            {
                e.NewValues["name"] = in_name;
                e.NewValues["funlist"] = in_funlist;
                e.NewValues["usrlist"] = in_usrlist;
                e.NewValues["valid"] = in_valid;
            }
        }
        else
        {
            e.Cancel = true;
        }
    }

    protected void DetailsView1_ItemUpdated(object sender, System.Web.UI.WebControls.DetailsViewUpdatedEventArgs e)
    {
        if (e.Exception == null)
        {
            e.KeepInEditMode = true;
            JSOutPut("修改成功!");
        }
        else
        {
            e.ExceptionHandled = true;
            e.KeepInEditMode = true;
            throw (e.Exception);
        }
    }
    protected void odsFunList_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        DataTable dttmp = e.ReturnValue as DataTable;
        foreach (DataRow dr in dttmp.Rows)
        {
            hidFunc.Value += dr[0].ToString() + ",";
        }
    }
    protected void odsUsrList_Selected(object sender, ObjectDataSourceStatusEventArgs e)
    {
        DataTable dttmp = e.ReturnValue as DataTable;
        foreach (DataRow dr in dttmp.Rows)
        {
            hidUsr.Value += dr[0].ToString() + ",";
        }
    }
    protected void odsFunList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        hidFunc.Value = "";
    }
    protected void odsUsrList_Selecting(object sender, ObjectDataSourceSelectingEventArgs e)
    {
        hidUsr.Value = "";
    }
}