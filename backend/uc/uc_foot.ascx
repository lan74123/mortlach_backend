﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_foot.ascx.cs" Inherits="backend_uc_uc_foot" %>



<script src='<%= ResolveUrl("~/backend/asset/js/modernizr.min.js") %>'></script>
<script src='<%= ResolveUrl("~/backend/asset/js/jquery.sparkline.min.js") %>'></script>


<script src='<%= ResolveUrl("~/backend/asset/js/toggles.min.js") %>'></script>
<script src='<%= ResolveUrl("~/backend/asset/js/retina.min.js") %>'></script>


<script src='<%= ResolveUrl("~/backend/asset/js/flot/jquery.flot.min.js") %>'></script>
<script src='<%= ResolveUrl("~/backend/asset/js/flot/jquery.flot.resize.min.js") %>'></script>
<script src='<%= ResolveUrl("~/backend/asset/js/flot/jquery.flot.spline.min.js") %>'></script>
<script src='<%= ResolveUrl("~/backend/asset/js/morris.min.js") %>'></script>
<script src='<%= ResolveUrl("~/backend/asset/js/raphael-2.1.0.min.js") %>'></script>
<script src='<%= ResolveUrl("~/backend/asset/js/jquery.validate.min.js") %>'></script>
<script src='<%= ResolveUrl("~/backend/asset/js/jquery.gritter.min.js") %>'></script>

<script src='<%= ResolveUrl("~/backend/asset/js/custom.js") %>'></script>
<%--<script src="<%= ResolveUrl("/backend/js/dashboard.js") %>"></script>--%>
<script>
    
    function checkLogin() {
        $.ajax({
            url: "/backend_api/ChkLogin.ashx",
            type: "GET",
            dataType: "json",

            success: function (msg) {
                var data = msg;
                if (data.login == "false") {
                    if (!inLoginPage()) {
                        document.location.href = "/backend/login.aspx";
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
            }
        });
    }

    function inLoginPage() {
        var nowUrl = location.href;
        var regex = /.*login.*/;
        if (nowUrl.match(regex) == null) {
            return false;
        } else {
            return true;
        }
    }

    $(document).ready(function () {
        //checkLogin();
    })
    //setInterval(function () { checkLogin(); }, 60000);
</script>