﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_head.ascx.cs" Inherits="backend_uc_uc_head" %>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">


<link rel="shortcut icon" href='<%= ResolveUrl("~/backend/asset/images/favicon_new.png") %>' type="image/png" />
<title><%= Config.Site %></title>

<link href='<%= ResolveUrl("~/backend/asset/css/style.ogilvy.css") %>' rel="stylesheet" />
<link href='<%= ResolveUrl("~/backend/asset/css/custom.css") %>' rel="stylesheet" />
<link href='<%= ResolveUrl("~/backend/asset/css/jquery.gritter.css") %>' rel="stylesheet" />
<link href='<%= ResolveUrl("~/backend/asset/css/fontawesome-iconpicker.min.css") %>' rel="stylesheet" />
<link href='<%= ResolveUrl("~/backend/asset/css/flipSwitch.css") %>' rel="stylesheet" />
<link href='<%= ResolveUrl("~/backend/asset/css/responsive-tables.css") %>' rel="stylesheet" />

<script src='<%= ResolveUrl("~/backend/asset/js/jquery-1.11.1.min.js") %>'></script>
<script src='<%= ResolveUrl("~/backend/asset/js/jquery-migrate-1.2.1.min.js") %>'></script>
<script src='<%= ResolveUrl("~/backend/asset/js/jquery-ui-1.10.3.min.js") %>'></script>
<script src='<%= ResolveUrl("~/backend/asset/js/jquery.cookies.js") %>'></script>
<script src='<%= ResolveUrl("~/backend/asset/js/bootstrap.min.js") %>'></script>
<script src='<%= ResolveUrl("~/backend/asset/js/fontawesome-iconpicker.min.js") %>'></script>
<script src='<%= ResolveUrl("~/backend/asset/js/responsive-tables.js") %>'></script>




<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src='<%= ResolveUrl("~/backend/asset/js/html5shiv.js") %>'></script>
  <script src='<%= ResolveUrl("~/backend/asset/js/respond.min.js") %>'></script>
<![endif]-->
