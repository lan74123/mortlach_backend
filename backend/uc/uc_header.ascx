﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_header.ascx.cs" Inherits="backend_uc_uc_header" %>
<div class="headerbar"">


    <a class="menutoggle"><i class="fa fa-bars"></i></a>
    <label class="hidden-xs" style="float:left; margin-left:5px"><h4><%= BackendBasePage.GetUserName() %></h4></label>

    
    <%--<div class=""></div>--%>
    
    <div class="header-right">
        <ul class="headermenu">
            <li>
                <div class="btn-group">
                    <form action="<%= ResolveUrl("~/backend/logout.aspx") %>" method="post">
                        <button type="submit" class="btn dropdown-toggle">
                            <i class="glyphicon glyphicon-log-out"></i> 登出
                        </button>
                    </form>
                </div>
            </li>
        </ul>
    </div>

    <!-- header-right -->

</div>
