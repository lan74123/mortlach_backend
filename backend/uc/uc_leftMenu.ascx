﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="uc_leftmenu.ascx.cs" Inherits="backend_uc_uc_leftmenu" %>
<script type="text/javascript">
    $(function () {
        (function () {
            //左邊選單
            if(!!$.cookie('websiteBread'))
            {
                var bread = $.cookie('websiteBread').split(';');
                var $node = $('#' + bread[0]);
                $node.addClass('active');
                if (!!bread[1]) {
                    var $ul = $node.parent();
                    var $parentli = $ul.parent();
                    var $parenthyper = $parentli.find('a:first');
                    var $childhyper = $node.find('a:first');
                    var $parentName = $parenthyper.find('span').html();
                    var $childName = $childhyper.find('span').html();

                    $ul.css('display', 'block');
                    $parentli.addClass('nav-active active');
                    
                    if ($node.attr('data-level') === "two")
                    {
                        $(".breadcrumb").append("<li>" + $parentName + "</li><li><a href='" + $childhyper.attr('href') + "'>" + $childName + "</a></li>");
                    }
                    else
                    {
                        $(".breadcrumb").append("<li><a href='" + $childhyper.attr('href') + "'>" + $childName + "</a></li>");
                    }
                    <%--<li><a href="index.aspx"><%= Config.Site %></a></li>--%>
                }
            }
            else {
                $(".breadcrumb").append("<li class='active'>首頁</li>");
            }
        })();

        $("li[data-level='two']").on('click', function () {
            //有父層的節點
            var $this = $(this);
            $.cookie('websiteBread', $this.attr('id') + ';true', { expires: 1, path: '/' });
        });

        $("li[data-level='one']").on('click', function () {
            //單一節點
            var $this = $(this);
            $.cookie('websiteBread', $this.attr('id')+';false', { expires: 1, path: '/' });
        });
    });
</script>

<div class="leftpanel">

    <div class="logopanel">
        <h1> <%= Config.Site %> </h1>
    </div>
    <!-- logopanel -->

    <div class="leftpanelinner">

        <!-- This is only visible to small devices -->
        <div class="visible-xs hidden-sm hidden-md hidden-lg">
            <div class="media userlogged nomargin">
                <%--<img alt="" src="images/photos/loggeduser.png" class="media-object">--%>
                <div class="media-body" style="margin: 0">
                    <h4><%= BackendBasePage.GetUserName() %></h4>
                    <span>登入</span>
                </div>
            </div>

            <%--<h5 class="sidebartitle actitle">Account</h5>--%>
            <ul class="nav nav-pills nav-stacked nav-bracket nomargin">
                <%--<li><a href="profile.html"><i class="fa fa-user"></i><span>Profile</span></a></li>
                <li><a href=""><i class="fa fa-cog"></i><span>Account Settings</span></a></li>
                <li><a href=""><i class="fa fa-question-circle"></i><span>Help</span></a></li>
                <li><a href="signout.html"><i class="fa fa-sign-out"></i><span>登出</span></a></li>--%>
            </ul>
        </div>
        <%--<h5 class="sidebartitle">功能列</h5>--%>
        <ul id="navLeftMenu" class="nav nav-pills nav-stacked nav-bracket">
            <li id='node0' data-level='one'><a href="<%= ResolveUrl("~/backend/index.aspx") %>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <%= Server.HtmlDecode(html_menu) %>
        </ul>
    </div>
    <!-- leftpanelinner -->
</div>

