agePage = function () {
  //private menbers

  //private methods
  function init() {
    console.log('agePage is loaded.');

    var isOver18 = Cookies.get('isOver18');

    console.log('isOver18 = ' + isOver18);

    if (isOver18) {
      $('.age').fadeOut();
    } else {

      if ($(".age").html() == '' || $(".age").html() == undefined) {
        console.log('no age');  
        var _age = $('<div></div>').addClass('age').appendTo($('body'));  
        $('.age').load("_age.html .age", function () {  
          //Birthday 出生年月日
          birthday();
          CheckData();
          initBirthdaySelect();
          $('.age').css('display', 'flex'); 
        });
      }else{
        birthday();
        CheckData();
        initBirthdaySelect();
        $('.age').css('display', 'flex'); 
      }
    }

    
  }

  //Birthday 出生年月日
  function birthday() {
    var d = new Date();
    var endDate = new Date(1900 + (d.getYear() - 18), d.getMonth(), d.getDate());

    $('#birthday').datepicker({
      startDate: "1900-1-1",
      language: "zh-TW",
      endDate: endDate,
      autoclose: true,
      weekStart: 7,
      startView: 0
    });

    $('#formBirthday').datepicker({
      startDate: "1900-1-1",
      language: "zh-TW",
      endDate: endDate,
      autoclose: true,
      weekStart: 7,
      startView: 0
    });
  }

  // CheckData
  function CheckData() {

		/*if ((window.location.href).indexOf('127.0.0.1') > 0 || (window.location.href).indexOf('localhost') > 0) {
			$('.age').fadeOut();		
		}*/

    $('.age__btn-submit').on('click', function () {

      var str = "";

      var yy = $(".age__birthday").find('.data__birthday__year').val();
      var mm = $(".age__birthday").find('.data__birthday__month').val();
      var dd = $(".age__birthday").find('.data__birthday__day').val();

      if (yy == null || mm == null || dd == null) {
        alert("請選擇您的出生年月日");
      } else {
        $('.age').fadeOut();
        Cookies.set('isOver18', true);
      }
    })

  }

  function initBirthdaySelect() {

    for (var y = 2000; y > 1900; y--) {
      var _option = $('<option></option>').appendTo($('.data__birthday__year'));
      _option.attr('value', y);
      _option.text(y);
    }

    for (var m = 1; m < 13; m++) {
      var _option = $('<option></option>').appendTo($('.data__birthday__month'));
      _option.attr('value', getNum(m));
      _option.text(getNum(m));
    }

    for (var d = 1; d < 32; d++) {
      var _option = $('<option></option>').appendTo($('.data__birthday__day'));
      _option.attr('value', getNum(d));
      _option.text(getNum(d));
    }
  }

  function getNum(_num) {
    var _string;
    if (parseInt(_num) / 10 < 1) {
      _string = '0' + _num.toString();
    } else {
      _string = _num.toString();
    }
    return _string;
  }

  //constructor
  {
    $(document).ready(function () {
      init();
    });
  }

  //public
  return {

  };
}

var agePage = new agePage();