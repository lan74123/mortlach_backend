/**
 *
 * @authors Eric Hsiao
 *
 */

animateObj = function (){

	//private menbers
	var $canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;

	//private methods
	function init(_namespace,$container) {
			var key = Object.keys(this[_namespace].compositions)[0];
			var comp = this[_namespace].getComposition(key);
			var lib=comp.getLibrary();

			$canvas = $('<canvas></canvas>').attr({
				width: lib.properties.width,
				height: lib.properties.height
			}).appendTo($container);

			var loader = new createjs.LoadQueue(false);
			loader.addEventListener("fileload", function(evt){handleFileLoad(evt,comp)});
			loader.addEventListener("complete", function(evt){handleComplete(evt,comp)});
			loader.loadManifest(lib.properties.manifest);
		}

		function handleFileLoad(evt, comp) {
			var images=comp.getImages();
			if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
		}

		function handleComplete(evt,comp) {
			var lib=comp.getLibrary();
			var ss=comp.getSpriteSheet();
			var queue = evt.target;
			var ssMetadata = lib.ssMetadata;
			for(i=0; i<ssMetadata.length; i++) {
				ss[ssMetadata[i].name] = new createjs.SpriteSheet( {"images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames} )
			}
			exportRoot = new lib[_namespace]();
			stage = new lib.Stage($canvas[0]);
			fnStartAnimation = function() {
				stage.addChild(exportRoot);
				createjs.Ticker.setFPS(lib.properties.fps);
				createjs.Ticker.addEventListener("tick", stage);
			}

			this[_namespace].compositionLoaded(lib.properties.id);
			fnStartAnimation();
		}

	//constructor

	{

	}

	//public
	return {
		init : function(_namespace,$container){
			init(_namespace,$container);
		}
	};
}
