(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.cut1_bg = function() {
	this.initialize(img.cut1_bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,845,389);


(lib.cut1_light_1 = function() {
	this.initialize(img.cut1_light_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,134,166);


(lib.cut1_light_2 = function() {
	this.initialize(img.cut1_light_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,100,189);


(lib.cut1_light_3 = function() {
	this.initialize(img.cut1_light_3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,145,218);


(lib.cut1_light_4 = function() {
	this.initialize(img.cut1_light_4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,94,246);


(lib.cut1_light_5 = function() {
	this.initialize(img.cut1_light_5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,90,127);


(lib.cut1_light_6 = function() {
	this.initialize(img.cut1_light_6);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,105,185);


(lib.cut1_light_7 = function() {
	this.initialize(img.cut1_light_7);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,115,217);


(lib.cut1_wine_1 = function() {
	this.initialize(img.cut1_wine_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,94,379);


(lib.cut1_wine_2 = function() {
	this.initialize(img.cut1_wine_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,112,338);


(lib.cut1_wine_3 = function() {
	this.initialize(img.cut1_wine_3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,122,340);


(lib.cut1_wine_4 = function() {
	this.initialize(img.cut1_wine_4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,117,396);


(lib.cut1_wine_5 = function() {
	this.initialize(img.cut1_wine_5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,120,319);


(lib.cut1_wine_6 = function() {
	this.initialize(img.cut1_wine_6);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,96,368);


(lib.cut1_wine_7 = function() {
	this.initialize(img.cut1_wine_7);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,129,263);


(lib.cut1_wine_new = function() {
	this.initialize(img.cut1_wine_new);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,166,509);


(lib.cut1_wine_7_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cut1_wine_7();
	this.instance.parent = this;
	this.instance.setTransform(-129,-263);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-129,-263,129,263);


(lib.cut1_wine_6_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cut1_wine_6();
	this.instance.parent = this;
	this.instance.setTransform(-96,-368);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-96,-368,96,368);


(lib.cut1_wine_5_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cut1_wine_5();
	this.instance.parent = this;
	this.instance.setTransform(-120,-319);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-120,-319,120,319);


(lib.cut1_wine_4_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cut1_wine_4();
	this.instance.parent = this;
	this.instance.setTransform(-117,-396);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-117,-396,117,396);


(lib.cut1_wine_3_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cut1_wine_3();
	this.instance.parent = this;
	this.instance.setTransform(-122,-340);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-122,-340,122,340);


(lib.cut1_wine_2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cut1_wine_2();
	this.instance.parent = this;
	this.instance.setTransform(-112,-338);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-112,-338,112,338);


(lib.cut1_wine_1_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cut1_wine_1();
	this.instance.parent = this;
	this.instance.setTransform(-94,-379);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-94,-379,94,379);


(lib.cut1_light_7_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cut1_light_7();
	this.instance.parent = this;
	this.instance.setTransform(-115,-217);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-115,-217,115,217);


(lib.cut1_light_6_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cut1_light_6();
	this.instance.parent = this;
	this.instance.setTransform(-105,-185);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-105,-185,105,185);


(lib.cut1_light_5_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cut1_light_5();
	this.instance.parent = this;
	this.instance.setTransform(-90,-127);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-90,-127,90,127);


(lib.cut1_light_4_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cut1_light_4();
	this.instance.parent = this;
	this.instance.setTransform(-94,-246);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-94,-246,94,246);


(lib.cut1_light_3_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cut1_light_3();
	this.instance.parent = this;
	this.instance.setTransform(-145,-218);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-145,-218,145,218);


(lib.cut1_light_2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cut1_light_2();
	this.instance.parent = this;
	this.instance.setTransform(-100,-189);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-100,-189,100,189);


(lib.cut1_light_1_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cut1_light_1();
	this.instance.parent = this;
	this.instance.setTransform(-134,-166);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-134,-166,134,166);


// stage content:
(lib.ani_1400x730_cut1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// 圖層_1
	this.instance = new lib.cut1_wine_new();
	this.instance.parent = this;
	this.instance.setTransform(617,212);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(498));

	// cut1_light_7.png
	this.instance_1 = new lib.cut1_light_7_1("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(530.5,433.5,1,1,0,0,0,-57.5,-108.5);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(48).to({_off:false},0).to({alpha:0.199},13).wait(364).to({startPosition:0},0).to({alpha:1},19,cjs.Ease.cubicInOut).wait(16).to({startPosition:0},0).to({alpha:0.199},20,cjs.Ease.cubicInOut).wait(18));

	// cut1_light_6.png
	this.instance_2 = new lib.cut1_light_6_1("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(876.5,467.5,1,1,0,0,0,-52.5,-92.5);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(48).to({_off:false},0).to({alpha:0.199},13).wait(303).to({startPosition:0},0).to({alpha:1},19,cjs.Ease.cubicInOut).wait(16).to({startPosition:0},0).to({alpha:0.199},20,cjs.Ease.cubicInOut).wait(61).to({startPosition:0},0).wait(18));

	// cut1_light_5.png
	this.instance_3 = new lib.cut1_light_5_1("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(579,335.5,1,1,0,0,0,-45,-63.5);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(48).to({_off:false},0).to({alpha:0.199},13).wait(243).to({startPosition:0},0).to({alpha:1},19,cjs.Ease.cubicInOut).wait(16).to({startPosition:0},0).to({alpha:0.199},20,cjs.Ease.cubicInOut).wait(121).to({startPosition:0},0).wait(18));

	// cut1_light_4.png
	this.instance_4 = new lib.cut1_light_4_1("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(801,397,1,1,0,0,0,-47,-123);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(48).to({_off:false},0).to({alpha:0.199},13).wait(182).to({startPosition:0},0).to({alpha:1},19,cjs.Ease.cubicInOut).wait(16).to({startPosition:0},0).to({alpha:0.199},20,cjs.Ease.cubicInOut).wait(182).to({startPosition:0},0).wait(18));

	// cut1_light_3.png
	this.instance_5 = new lib.cut1_light_3_1("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(602.5,436,1,1,0,0,0,-72.5,-109);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(48).to({_off:false},0).to({alpha:0.199},13).wait(122).to({startPosition:0},0).to({alpha:1},19,cjs.Ease.cubicInOut).wait(16).to({startPosition:0},0).to({alpha:0.199},20,cjs.Ease.cubicInOut).wait(242).to({startPosition:0},0).wait(18));

	// cut1_light_2.png
	this.instance_6 = new lib.cut1_light_2_1("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(448,465.5,1,1,0,0,0,-50,-94.5);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(48).to({_off:false},0).to({alpha:0.191},13).wait(61).to({alpha:0.199},0).to({alpha:1},19,cjs.Ease.cubicInOut).wait(16).to({startPosition:0},0).to({alpha:0.199},20,cjs.Ease.cubicInOut).wait(303).to({startPosition:0},0).wait(18));

	// cut1_light_1.png
	this.instance_7 = new lib.cut1_light_1_1("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(957,477,1,1,0,0,0,-67,-83);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(48).to({_off:false},0).to({alpha:0.191},13).to({startPosition:0},1).wait(1).to({alpha:0.201},0).wait(1).to({alpha:0.215},0).wait(1).to({alpha:0.236},0).wait(1).to({alpha:0.266},0).wait(1).to({alpha:0.307},0).wait(1).to({alpha:0.365},0).wait(1).to({alpha:0.444},0).wait(1).to({alpha:0.549},0).wait(1).to({alpha:0.665},0).wait(1).to({alpha:0.767},0).wait(1).to({alpha:0.844},0).wait(1).to({alpha:0.898},0).wait(1).to({alpha:0.937},0).wait(1).to({alpha:0.963},0).wait(1).to({alpha:0.981},0).wait(1).to({alpha:0.992},0).wait(1).to({alpha:0.998},0).wait(1).to({alpha:1},0).wait(16).to({startPosition:0},0).to({alpha:0.199},20,cjs.Ease.cubicInOut).wait(364).to({startPosition:0},0).wait(18));

	// mask (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EhCAAeZMAAAg8xMCEBAAAMAAAA8xg");
	var mask_graphics_497 = new cjs.Graphics().p("EhCAAeZMAAAg8xMCEBAAAMAAAA8xg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:700.5,y:365.5}).wait(497).to({graphics:mask_graphics_497,x:700.5,y:365.5}).wait(1));

	// cut1_wine_1.png
	this.instance_8 = new lib.cut1_wine_1_1("synched",0);
	this.instance_8.parent = this;
	this.instance_8.setTransform(718.8,503.5,1,1,0,0,0,-47,-189.5);

	this.shape = new cjs.Shape();
	this.shape.graphics.f().s("#000000").ss(1,1,1).p("EhCAgeYMCEBAAAMAAAA8xMiEBAAAg");
	this.shape.setTransform(700.5,365.5);

	var maskedShapeInstanceList = [this.instance_8,this.shape];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape},{t:this.instance_8,p:{x:718.8}}]},14).to({state:[{t:this.instance_8,p:{x:872}}]},19).to({state:[{t:this.instance_8,p:{x:872}}]},464).wait(1));

	// cut1_wine_2.png
	this.instance_9 = new lib.cut1_wine_2_1("synched",0);
	this.instance_9.parent = this;
	this.instance_9.setTransform(717.6,467,1,1,0,0,0,-56,-169);
	this.instance_9._off = true;

	var maskedShapeInstanceList = [this.instance_9];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(16).to({_off:false},0).to({x:524},19,cjs.Ease.quadInOut).wait(462).to({startPosition:0},0).wait(1));

	// cut1_wine_3.png
	this.instance_10 = new lib.cut1_wine_3_1("synched",0);
	this.instance_10.parent = this;
	this.instance_10.setTransform(717.7,452,1,1,0,0,0,-61,-170);
	this.instance_10._off = true;

	var maskedShapeInstanceList = [this.instance_10];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(18).to({_off:false},0).to({x:629,y:449},19,cjs.Ease.quadInOut).wait(460).to({startPosition:0},0).wait(1));

	// cut1_wine_4.png
	this.instance_11 = new lib.cut1_wine_4_1("synched",0);
	this.instance_11.parent = this;
	this.instance_11.setTransform(720.5,474,1,1,0,0,0,-58.5,-198);
	this.instance_11._off = true;

	var maskedShapeInstanceList = [this.instance_11];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(20).to({_off:false},0).to({x:800.5},19,cjs.Ease.quadInOut).wait(458).to({startPosition:0},0).wait(1));

	// cut1_wine_5.png
	this.instance_12 = new lib.cut1_wine_5_1("synched",0);
	this.instance_12.parent = this;
	this.instance_12.setTransform(717,462.5,1,1,0,0,0,-60,-159.5);
	this.instance_12._off = true;

	var maskedShapeInstanceList = [this.instance_12];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(22).to({_off:false},0).to({x:947},19,cjs.Ease.quadInOut).wait(456).to({startPosition:0},0).wait(1));

	// cut1_wine_6.png
	this.instance_13 = new lib.cut1_wine_6_1("synched",0);
	this.instance_13.parent = this;
	this.instance_13.setTransform(720,441,1,1,0,0,0,-48,-184);
	this.instance_13._off = true;

	var maskedShapeInstanceList = [this.instance_13];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(24).to({_off:false},0).to({x:580},19,cjs.Ease.quadInOut).wait(454).to({startPosition:0},0).wait(1));

	// cut1_wine_7.png
	this.instance_14 = new lib.cut1_wine_7_1("synched",0);
	this.instance_14.parent = this;
	this.instance_14.setTransform(721.5,467.5,1,1,0,0,0,-64.5,-131.5);
	this.instance_14._off = true;

	var maskedShapeInstanceList = [this.instance_14];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(26).to({_off:false},0).to({x:471.5},19,cjs.Ease.quadInOut).wait(452).to({startPosition:0},0).wait(1));

	// cut1_bg.jpg
	this.instance_15 = new lib.cut1_bg();
	this.instance_15.parent = this;
	this.instance_15.setTransform(278,171);

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(488).to({_off:true},1).wait(9));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(978,536,845,550);
// library properties:
lib.properties = {
	id: 'B8170198AA2A4AD0BA13AFA47D2CB075',
	width: 1400,
	height: 730,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/ani_1400x730_cut1/cut1_bg.jpg?1540821854752", id:"cut1_bg"},
		{src:"images/ani_1400x730_cut1/cut1_light_1.png?1540821854752", id:"cut1_light_1"},
		{src:"images/ani_1400x730_cut1/cut1_light_2.png?1540821854752", id:"cut1_light_2"},
		{src:"images/ani_1400x730_cut1/cut1_light_3.png?1540821854752", id:"cut1_light_3"},
		{src:"images/ani_1400x730_cut1/cut1_light_4.png?1540821854752", id:"cut1_light_4"},
		{src:"images/ani_1400x730_cut1/cut1_light_5.png?1540821854752", id:"cut1_light_5"},
		{src:"images/ani_1400x730_cut1/cut1_light_6.png?1540821854752", id:"cut1_light_6"},
		{src:"images/ani_1400x730_cut1/cut1_light_7.png?1540821854752", id:"cut1_light_7"},
		{src:"images/ani_1400x730_cut1/cut1_wine_1.png?1540821854752", id:"cut1_wine_1"},
		{src:"images/ani_1400x730_cut1/cut1_wine_2.png?1540821854752", id:"cut1_wine_2"},
		{src:"images/ani_1400x730_cut1/cut1_wine_3.png?1540821854752", id:"cut1_wine_3"},
		{src:"images/ani_1400x730_cut1/cut1_wine_4.png?1540821854752", id:"cut1_wine_4"},
		{src:"images/ani_1400x730_cut1/cut1_wine_5.png?1540821854752", id:"cut1_wine_5"},
		{src:"images/ani_1400x730_cut1/cut1_wine_6.png?1540821854752", id:"cut1_wine_6"},
		{src:"images/ani_1400x730_cut1/cut1_wine_7.png?1540821854752", id:"cut1_wine_7"},
		{src:"images/ani_1400x730_cut1/cut1_wine_new.png?1540821854752", id:"cut1_wine_new"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['B8170198AA2A4AD0BA13AFA47D2CB075'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, ani_1400x730_cut1 = ani_1400x730_cut1||{});
var createjs, ani_1400x730_cut1;