(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib._1 = function() {
	this.initialize(img._1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,542,271);


(lib._10 = function() {
	this.initialize(img._10);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,280,334);


(lib._11 = function() {
	this.initialize(img._11);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,361,398);


(lib._12 = function() {
	this.initialize(img._12);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,241,258);


(lib._13 = function() {
	this.initialize(img._13);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,328,191);


(lib._2 = function() {
	this.initialize(img._2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,450,351);


(lib._3 = function() {
	this.initialize(img._3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,549,452);


(lib._4 = function() {
	this.initialize(img._4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,428,341);


(lib._5 = function() {
	this.initialize(img._5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,504,449);


(lib._6 = function() {
	this.initialize(img._6);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,376,410);


(lib._7 = function() {
	this.initialize(img._7);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,363,496);


(lib._8 = function() {
	this.initialize(img._8);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,363,408);


(lib._9 = function() {
	this.initialize(img._9);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,417,448);


(lib.cut2_cask = function() {
	this.initialize(img.cut2_cask);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,297,412);


(lib.cut2_object = function() {
	this.initialize(img.cut2_object);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,147,279);


(lib.cut2_object_1 = function() {
	this.initialize(img.cut2_object_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,164,250);


(lib.cut2_object_2 = function() {
	this.initialize(img.cut2_object_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,132,167);


(lib.cut2_object_3 = function() {
	this.initialize(img.cut2_object_3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,143,208);


(lib.cut2_object_4 = function() {
	this.initialize(img.cut2_object_4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,149,178);


(lib.cut2_object_5 = function() {
	this.initialize(img.cut2_object_5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,101,165);


(lib.cut2_sun = function() {
	this.initialize(img.cut2_sun);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1920,1416);


(lib.cut2_title = function() {
	this.initialize(img.cut2_title);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,400,121);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.sun13 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib._13();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.sun13, new cjs.Rectangle(0,0,328,191), null);


(lib.sun12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib._12();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.sun12, new cjs.Rectangle(0,0,241,258), null);


(lib.sun11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib._11();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.sun11, new cjs.Rectangle(0,0,361,398), null);


(lib.sun10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib._10();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.sun10, new cjs.Rectangle(0,0,280,334), null);


(lib.sun9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib._9();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.sun9, new cjs.Rectangle(0,0,417,448), null);


(lib.sun8 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib._8();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.sun8, new cjs.Rectangle(0,0,363,408), null);


(lib.sun7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib._7();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.sun7, new cjs.Rectangle(0,0,363,496), null);


(lib.sun6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib._6();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.sun6, new cjs.Rectangle(0,0,376,410), null);


(lib.sun5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib._5();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.sun5, new cjs.Rectangle(0,0,504,449), null);


(lib.sun4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib._4();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.sun4, new cjs.Rectangle(0,0,428,341), null);


(lib.sun3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib._3();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.sun3, new cjs.Rectangle(0,0,549,452), null);


(lib.sun2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib._2();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.sun2, new cjs.Rectangle(0,0,450,351), null);


(lib.sun1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib._1();
	this.instance.parent = this;
	this.instance.setTransform(-542,-271);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-542,-271,542,271);


(lib.cut2_title_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cut2_title();
	this.instance.parent = this;
	this.instance.setTransform(-400,-121);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-400,-121,400,121);


(lib.cut2_cask_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cut2_cask();
	this.instance.parent = this;
	this.instance.setTransform(-297,-412);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-297,-412,297,412);


(lib.sun = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cut2_sun();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.sun, new cjs.Rectangle(0,0,1920,1416), null);


(lib.cut2_object5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cut2_object_5();
	this.instance.parent = this;
	this.instance.setTransform(-101,-165);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-101,-165,101,165);


(lib.cut2_object4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cut2_object_4();
	this.instance.parent = this;
	this.instance.setTransform(-149,-178);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-149,-178,149,178);


(lib.cut2_object3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cut2_object_3();
	this.instance.parent = this;
	this.instance.setTransform(-143,-208);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-143,-208,143,208);


(lib.cut2_object2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cut2_object_2();
	this.instance.parent = this;
	this.instance.setTransform(-132,-167);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-132,-167,132,167);


(lib.cut2_object1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cut2_object_1();
	this.instance.parent = this;
	this.instance.setTransform(-164,-250);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-164,-250,164,250);


(lib.cut2_object_6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer_1
	this.instance = new lib.cut2_object();
	this.instance.parent = this;
	this.instance.setTransform(-147,-279);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-147,-279,147,279);


// stage content:
(lib.ani_1400x730_cut2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_224 = function() {
		var _this = this;
		/*
		在時間軸上移動磁頭至指定的影格編號，並從該影格繼續播放。
		亦可用於主時間軸或影片片段時間軸。
		*/
		_this.gotoAndPlay(90);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(224).call(this.frame_224).wait(5));

	// cut2_title.png
	this.instance = new lib.cut2_title_1("synched",0);
	this.instance.parent = this;
	this.instance.setTransform(727.2,588.6,1.536,1.536,0,0,0,-199.8,-60.4);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(26).to({_off:false},0).to({regX:-200,regY:-60.5,scaleX:1,scaleY:1,x:727,y:588.5,alpha:1},26,cjs.Ease.cubicInOut).wait(171).to({startPosition:0},0).to({_off:true},1).wait(5));

	// cut2_cask.png
	this.instance_1 = new lib.cut2_cask_1("synched",0);
	this.instance_1.parent = this;
	this.instance_1.setTransform(735.4,408,1.338,1.338,0,0,0,-148.5,-206);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(16).to({_off:false},0).to({scaleX:1,scaleY:1,x:735.5,alpha:1},26,cjs.Ease.cubicInOut).wait(181).to({startPosition:0},0).to({_off:true},1).wait(5));

	// cut2_object.png
	this.instance_2 = new lib.cut2_object_6("synched",0);
	this.instance_2.parent = this;
	this.instance_2.setTransform(717.5,458.5,1,1,0,0,0,-73.5,-139.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({x:536.5},24,cjs.Ease.cubicInOut).wait(204).to({startPosition:0},0).wait(1));

	// cut2_object_1.png
	this.instance_3 = new lib.cut2_object1("synched",0);
	this.instance_3.parent = this;
	this.instance_3.setTransform(734,487,1,1,0,0,0,-82,-125);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).to({x:922,y:484,alpha:1},29,cjs.Ease.cubicInOut).wait(193).to({startPosition:0},0).wait(1));

	// cut2_object_2.png
	this.instance_4 = new lib.cut2_object2("synched",0);
	this.instance_4.parent = this;
	this.instance_4.setTransform(544,488.5,1,1,0,0,0,-66,-83.5);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(7).to({_off:false},0).to({x:604,alpha:1},23,cjs.Ease.cubicInOut).wait(198).to({startPosition:0},0).wait(1));

	// cut2_object_3.png
	this.instance_5 = new lib.cut2_object3("synched",0);
	this.instance_5.parent = this;
	this.instance_5.setTransform(921.5,446,1,1,0,0,0,-71.5,-104);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(12).to({_off:false},0).to({x:852.5,y:447,alpha:1},23,cjs.Ease.cubicInOut).wait(193).to({startPosition:0},0).wait(1));

	// cut2_object_4.png
	this.instance_6 = new lib.cut2_object4("synched",0);
	this.instance_6.parent = this;
	this.instance_6.setTransform(552.5,448,1,1,0,0,0,-74.5,-89);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(6).to({_off:false},0).to({x:483.5,y:451,alpha:1},23,cjs.Ease.cubicInOut).wait(199).to({startPosition:0},0).wait(1));

	// cut2_object_5.png
	this.instance_7 = new lib.cut2_object5("synched",0);
	this.instance_7.parent = this;
	this.instance_7.setTransform(905.5,424.5,1,1,0,0,0,-50.5,-82.5);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(1).to({_off:false},0).to({x:948.5,alpha:1},23,cjs.Ease.cubicInOut).wait(204).to({startPosition:0},0).wait(1));

	// cut2_sun (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_44 = new cjs.Graphics().p("EAy3AjLQgYgYgOgbQgGgLgFgMQgFgNgDgOQgDgNgBgNQgCgLAAgMQAAgQACgQQACgOAEgOQAEgMAFgMIALgXQAIgNAKgNIARgSIASgRQALgJANgIQAQgIAQgHQAMgEANgDQAMgDAOgCIAZgBIAXABIAPADIASADQAOAEAOAFQAMAFALAHIAVAOQALAJALALIARASQAKANAIANQAHAOAGANIAIAYQADAMABANQACAPAAAQIgBAYIgEAYQgDANgEANQgQApgiAjQg+A+hYAAQhYAAg+g+g");
	var mask_graphics_45 = new cjs.Graphics().p("EAwiAjkQgmglgXgrQgJgSgHgSQgIgUgGgWQgEgVgCgVQgCgTAAgTQAAgZADgYQADgXAGgVQAGgUAIgTQAIgSAJgRQANgWAQgTQAMgPAPgPQAOgOAOgMQASgPAVgMQAYgOAagKQATgHAUgFQAUgFAVgCQATgCAVAAQATAAASACIAYAEIAbAFQAXAGAWAJQATAIARALIAhAWQATAOARARQAOAPAMAPQAQATANAWQALAUAJAWIANAmQAFATACAUQADAXAAAZQAAATgBATQgDAVgEATQgFAVgHATQgYBCg2A2QhjBjiLAAQiLAAhihjg");
	var mask_graphics_46 = new cjs.Graphics().p("EAuMAj9Qgzgzgfg6QgNgYgKgaQgLgbgHgeQgHgcgCgeQgDgZAAgaQAAgiAEgiQAFgeAIgdQAIgbALgaQAKgZANgYQASgdAXgbQAQgVATgTQAUgUATgQQAZgVAcgQQAigTAigOQAagKAdgHQAbgGAdgDQAagDAcAAQAaAAAZADIAgAFIAmAIQAgAHAdANQAaALAYAOQAWAOAXAQQAZAUAYAYQATATAQAVQAWAbARAdQARAcAMAdQAJAaAIAaQAGAaAEAcQAEAgAAAiQAAAagCAaQgDAdgGAZQgGAdgKAbQgiBahJBJQiICHi+AAQi/AAiGiHg");
	var mask_graphics_47 = new cjs.Graphics().p("EAr3AkWQhBhAgnhKQgRgfgMggQgOgjgKgmQgIgkgDglQgEggAAghQAAgsAGgqQAFgnALglQAKgiAOghQANggARgeQAWglAdgiQAUgaAZgZQAZgZAYgUQAggbAkgUQAqgZAsgSQAhgMAkgIQAigJAlgEQAhgCAkAAQAhAAAgACQAWADATAEQAZAEAXAGQAoAJAlARQAhAOAeASQAdARAdAVQAfAZAfAeQAYAZAVAaQAbAiAWAlQAVAkAPAlQAMAhAKAhQAIAhAEAkQAGAnAAAsQAAAhgDAhQgEAkgHAhQgIAkgMAiQgrByhdBdQisCsjyAAQjyAAiqisg");
	var mask_graphics_48 = new cjs.Graphics().p("EApiAkwQhOhOgxhaQgUglgPgoQgQgqgMguQgKgrgDgtQgFgnAAgoQAAg1AGg0QAHguANgtQAMgqARgoQAQgmAUglQAbgtAjgpQAZggAegeQAegeAegZQAmgfArgZQA0geA1gWQAogPArgKQAqgKAtgFQAogDArAAQAoAAAmADQAbADAXAFQAeAFAdAHQAwAMAtAUQAoAQAkAWQAjAWAjAZQAnAeAkAkQAeAeAZAgQAiApAaAtQAZAsASAtQAPAoAMAoQAKAnAFAsQAHAwAAA1QAAAogEAoQgFAsgIAnQgKAsgPApQg0CLhxBxQjQDQklAAQklAAjOjQg");
	var mask_graphics_49 = new cjs.Graphics().p("EAnNAlJQhchcg5hpQgYgsgRguQgUgxgNg3QgMgzgEg1QgGgtAAgvQAAg+AIg9QAIg3APg1QAOgwAUgvQATgtAYgrQAfg1ApgxQAdglAkgjQAjgkAjgdQAtglAzgeQA8gjA/gZQAvgSAzgMQAxgLA0gGQAvgEAzAAQAvAAAtAEQAgAEAbAGQAjAGAhAHQA5AOA1AYQAvATArAaQApAZApAdQAtAjArAsQAjAjAeAlQAnAxAfA1QAdAyAWA1QARAvAOAvQAMAvAGAzQAIA5AAA+QAAAvgEAvQgGAzgKAvQgMAzgRAxQg9CiiFCFQj0D1lZAAQlYAAjyj1g");
	var mask_graphics_50 = new cjs.Graphics().p("EAk3AliQhphphCh6QgbgxgUg2QgWg4gQg/QgNg6gFg9QgHg0AAg2QAAhIAJhFQAJg/ASg9QAQg4AXg2QAWg0AbgxQAkg9Avg4QAigrAogoQApgpAoghQA0grA6giQBGgoBIgdQA2gVA6gNQA4gOA9gGQA2gFA6AAQA2AAA0AFQAkAEAfAHQApAHAmAJQBBAPA9AbQA2AXAxAdQAwAdAvAiQAzAoAyAyQAoAoAiArQAtA4AkA9QAiA6AYA9QAVA2APA2QAOA2AGA6QAJBBAABIQAAA2gEA2QgHA7gLA1QgNA7gVA4QhFC7iZCYQkZEZmMAAQmLAAkXkZg");
	var mask_graphics_51 = new cjs.Graphics().p("EAiiAl8Qh3h4hKiJQgeg4gXg8QgahAgRhHQgQhCgFhFQgHg6AAg9QAAhRAKhPQAKhHAUhEQAShAAZg9QAag6Aeg4QAphEA1hAQAmgwAuguQAuguAtgmQA7gwBCgmQBOguBSghQA9gWBCgQQA/gPBFgIQA9gFBCAAQA8AAA7AFQAoAGAkAHQAuAIArAKQBJASBFAeQA9AaA4AhQA1AhA1AmQA7AtA4A4QAtAuAmAwQAzBAApBEQAmBCAcBFQAXA9ARA8QAQA9AHBCQAKBKAABRQAAA9gFA9QgHBCgNA9QgPBCgXA/QhPDTisCtQk+E9m+AAQm/AAk7k9g");
	var mask_graphics_52 = new cjs.Graphics().p("EAgNAmVQiFiFhSiZQgig+gahEQgchHgUhPQgRhJgFhNQgJhBAAhEQAAhaALhYQAMhPAWhNQAUhGAchEQAdhBAig/QAthMA7hHQArg2AzgzQAzgyAzgrQBBg2BJgqQBYgzBbglQBDgZBKgRQBHgRBMgJQBEgFBKAAQBEAABBAFQAtAGAoAIQAyAJAxALQBSAUBMAiQBEAcA+AlQA8AlA7AqQBBAzA+A+QAzAzArA2QA4BHAuBMQAqBKAfBMQAaBEATBEQARBEAJBKQALBSAABaQAABEgFBEQgJBKgOBDQgRBKgaBHQhXDrjBDAQlhFinyAAQnyAAlflig");
	var mask_graphics_53 = new cjs.Graphics().p("EAd3AmuQiSiShbipQglhFgchKQgfhOgWhYQgThRgGhUQgKhIAAhLQAAhkANhhQAMhXAZhUQAWhOAfhLQAghIAlhFQAyhUBChOQAug7A4g4QA5g4A4gvQBIg8BRguQBhg5BjgoQBLgcBRgTQBOgTBVgJQBLgGBRAAQBLAABHAGQAyAGAsAKQA4AJA1AMQBbAWBUAmQBLAfBFApQBBAoBCAvQBHA4BFBFQA4A4AvA7QA+BOAyBUQAvBSAjBUQAcBLAWBLQASBLAKBRQAMBaAABkQAABLgGBLQgKBRgPBLQgTBRgcBOQhhEDjUDUQmGGGolAAQomAAmDmGg");
	var mask_graphics_54 = new cjs.Graphics().p("EAbiAnIQigihhji4QgphLgfhSQgihVgYhgQgUhYgHhdQgKhOAAhSQAAhtANhqQAOhfAbhdQAYhVAihSQAihOAphLQA3hcBIhWQAzhBA9g9QA+g9A9g0QBPhABYg0QBqg9BtgsQBSgfBZgVQBVgUBcgKQBSgHBZAAQBSAABOAHQA3AHAwAKQA9AKA6AOQBjAYBcApQBSAiBLAsQBIAsBHA0QBPA9BLBLQA9A9AzBBQBFBWA2BcQAzBYAmBdQAfBRAYBSQAUBSAKBZQAOBjAABtQAABSgHBSQgKBZgRBRQgVBZgeBVQhqEcjoDoQmrGqpYAAQpZAAmnmqg");
	var mask_graphics_55 = new cjs.Graphics().p("EAZNAnhQiuiuhrjIQgthSghhYQglhdgahoQgWhggIhkQgLhVAAhZQAAh2APhzQAPhoAdhkQAahdAlhYQAlhWAthRQA7hkBOhdQA3hGBDhDQBDhCBCg4QBVhGBhg4QBzhBB2gxQBZghBggWQBdgWBkgLQBZgIBgAAQBZAABVAIQA7AHA0ALQBDALA/APQBrAaBkAsQBZAkBRAxQBOAwBOA3QBVBDBSBRQBCBDA4BGQBKBdA7BkQA4BgAoBkQAiBZAaBZQAWBZALBgQAPBsAAB2QAABZgIBZQgLBggSBZQgWBggiBdQhzEzj7D8QnPHPqMAAQqMAAnLnPg");
	var mask_graphics_56 = new cjs.Graphics().p("EAW4An6Qi8i7h0jYQgwhYgkhgQgohkgchwQgYhngIhsQgMhcAAhgQAAiAAQh8QAQhwAghrQAchkAohgQAohcAwhYQBAhsBUhkQA8hLBIhIQBIhIBIg8QBbhLBog8QB8hICAg0QBggkBogYQBjgYBsgMQBggIBoAAQBgAABcAIQBAAIA4AMQBIAMBDAQQB0AcBsAwQBgAoBYA0QBUA0BUA8QBcBHBXBYQBIBIA8BLQBQBkBABsQA8BoAsBsQAkBgAcBgQAYBfAMBoQAQB0AACAQAABggIBgQgMBogUBfQgYBogkBkQh8FMkQEPQnzH0q/AAQq/AAnvn0g");
	var mask_graphics_57 = new cjs.Graphics().p("EAUiAoUQjJjKh8jnQg0hfgmhmQgrhsgeh4QgahvgIh0QgNhiAAhnQAAiJARiFQARh4Aih0QAehrArhnQArhiAzhfQBFhzBahrQBAhSBNhNQBOhMBNhAQBihSBwhAQCFhNCJg4QBngnBvgZQBrgaB0gNQBngIBvAAQBnAABjAIQBEAJA8ANQBNAMBJASQB8AeB0AzQBnArBeA4QBaA3BaBBQBjBNBeBdQBNBNBBBSQBVBrBFBzQBABwAvB0QAnBmAeBnQAaBnAMBwQASB8AACJQAABngJBnQgNBvgVBnQgaBvgnBrQiEFkkkEkQoYIXryAAQryAAoUoXg");
	var mask_graphics_58 = new cjs.Graphics().p("EASNAotQjXjXiFj3Qg3hlgphuQgthyghiAQgbh3gJh8QgOhpAAhuQAAiTASiNQATiBAkh7QAghzAuhtQAuhqA3hkQBJh8BghyQBFhWBShTQBThSBShFQBphXB3hEQCOhTCTg7QBtgpB3gcQBzgbB7gOQBugJB3AAQBuAABpAJQBKAJBAAOQBSANBOATQCFAgB7A3QBuAuBlA7QBgA8BgBEQBpBTBlBkQBSBTBFBWQBbByBKB8QBEB3AzB7QApBuAgBuQAbBuAOB3QASCEAACTQAABugJBuQgNB3gXBtQgcB3gpBzQiOF8k3E3Qo8I8smAAQslAAo4o8g");
	var mask_graphics_59 = new cjs.Graphics().p("EAP4ApGQjljliNkGQg6hsgsh0Qgxh6giiIQgdh/gKiDQgPhwAAh1QAAicAUiXQATiIAniEQAih5Axh1QAxhwA6hrQBOiEBmh4QBJhdBYhXQBYhYBXhJQBwhdB/hJQCXhXCbg/QB1gsB/geQB6gdCDgOQB1gKB+AAQB1AABwAKQBOAJBEAPQBYAPBTATQCNAiCDA7QB1AwBrBAQBmA/BnBJQBwBXBrBsQBXBXBJBdQBiB4BOCEQBJB+A1CEQAsB1AiB0QAdB1APB/QATCNAACcQAAB1gJB0QgPB/gYB1QgeB+grB6QiXGUlLFLQphJhtZAAQtYAApcphg");
	var mask_graphics_60 = new cjs.Graphics().p("EANjApgQjzjziVkXQg+hxgvh8QgziBgkiQQgfiGgLiMQgPh2AAh8QAAilAUigQAViRApiLQAliBAzh8QA0h2A+hyQBSiKBsiBQBOhiBdhdQBdhdBchNQB3hiCGhOQCghdClhDQB8guCGgfQCBgfCMgQQB7gKCGAAQB8AAB3AKQBTALBIAPQBdAQBXAUQCWAkCLA+QB8A0ByBDQBsBDBsBNQB3BdBxByQBdBdBOBiQBnCBBSCKQBOCGA5CLQAuB8AkB8QAfB8AQCGQAUCWAAClQAAB8gKB7QgQCHgZB7QgfCHgvCBQigGslfFfQqEKEuNAAQuMAAp/qEg");
	var mask_graphics_61 = new cjs.Graphics().p("EALNAp5QkAkAieknQhBh4gxiCQg3iJgmiYQghiOgKiTQgRh9AAiDQAAivAWipQAWiYAriTQAniJA2iCQA3h+BBh3QBXiTBziIQBRhoBihiQBjhiBihSQB9hnCOhSQCphiCuhHQCDgxCOghQCIggCTgRQCDgLCOAAQCCAAB+ALQBXALBMAQQBiARBdAWQCeAmCTBBQCDA3B4BHQByBGBzBSQB9BiB4B4QBiBiBSBoQBtCIBXCTQBSCNA8CTQAxCDAmCDQAhCDAQCNQAWCeAACvQAACDgLCCQgQCOgbCDQghCOgxCIQipHElzFzQqpKpu/AAQvAAAqkqpg");
	var mask_graphics_62 = new cjs.Graphics().p("EAI4AqSQkOkOimk2QhFh+g0iKQg4iPgoihQgjiVgLibQgRiFAAiJQAAi4AXiyQAWihAuibQApiPA4iKQA5iDBFh/QBcibB5iPQBWhtBnhoQBohnBnhWQCEhtCVhWQCyhoC4hKQCKg0CVgiQCQgjCbgRQCJgLCWAAQCKAACEALQBbALBRASQBnARBiAXQCmAoCbBFQCKA5B+BLQB5BLB5BWQCEBnB+B+QBnBoBWBtQBzCPBcCbQBWCWA/CaQA0CKAoCJQAjCKARCVQAXCnAAC4QAACJgMCKQgRCWgdCJQgiCWg0CPQiyHdmGGGQrOLOvyAAQvzAArIrOg");
	var mask_graphics_63 = new cjs.Graphics().p("EAGjAqrQkckbiulGQhIiFg2iQQg9iXgqipQgkidgMijQgSiLAAiQQAAjBAYi7QAYipAwijQAriXA8iQQA8iLBJiEQBfijB/iXQBahzBthsQBthtBshaQCLhzCdhaQC7htDBhOQCRg3CcgkQCXgkCjgSQCRgMCdAAQCRAACKAMQBhAMBUASQBtASBnAYQCuAqCjBJQCRA8CFBPQB/BOB+BaQCLBtCFCFQBsBsBbBzQB4CXBhCjQBaCdBDCiQA2CQAqCRQAlCRASCdQAYCvAADBQAACQgMCRQgSCdgfCRQgkCdg2CXQi7H0maGaQryLzwmAAQwmAArsrzg");
	var mask_graphics_64 = new cjs.Graphics().p("EAEOArFQkokpi4lWQhMiLg5iYQg/iegsixQgmikgNirQgTiSAAiXQAAjLAajEQAZixAyirQAtidA/iYQA/iRBMiLQBlirCEieQBfh4ByhyQByhyBxhfQCSh4CkhfQDEhxDLhTQCXg5ClgmQCeglCrgTQCYgNCkAAQCYAACRANQBlAMBZATQByATBrAZQC4AtCqBMQCYA/CLBSQCFBSCFBfQCRByCLCLQByByBfB4QB/CeBlCrQBfCkBFCrQA5CXAsCYQAmCXATClQAaC3AADLQAACXgNCYQgTClggCXQgmClg4CeQjFINmuGuQsWMWxZAAQxZAAsQsWg");
	var mask_graphics_65 = new cjs.Graphics().p("EAB4AreQk1k3jAllQhQiSg7ieQhCimgvi5QgnisgOizQgTiYAAieQAAjUAajNQAbi5A0izQAvikBCifQBCiYBPiSQBqiyCLimQBih+B3h3QB4h3B3hjQCYh+CshjQDNh3DUhWQCeg7CsgoQCmgoCygUQCfgNCsAAQCfAACYANQBqAOBcATQB3AUBxAbQDAAuCyBPQCfBDCSBWQCLBWCLBjQCYB3CRCRQB3B3BkB+QCECmBqCyQBjCsBJCzQA7CfAvCdQAnCfAUCsQAbDAAADUQAACegOCfQgUCsghCfQgnCsg8ClQjNIlnCHCQs7M7yMAAQyMAAs1s7g");
	var mask_graphics_66 = new cjs.Graphics().p("EgAcAr3QlElEjJl1QhTiYg+imQhFisgwjCQgqizgNi7QgVifAAilQAAjdAcjXQAbjBA3i5QAxitBFimQBFifBTiYQBui6CRitQBoiDB8h8QB8h9B8hnQCfiDCzhoQDWh8DdhaQCmg+C0gqQCsgpC7gVQCmgOCzAAQCmAACfAOQBuAOBhAVQB8AUB2AcQDIAwC6BTQCmBFCYBaQCRBaCRBnQCfB9CYCYQB8B8BoCDQCKCtBvC6QBnC0BMC6QA+CmAxCmQApClAVCzQAbDJAADdQAAClgNCmQgVC0gjClQgpC0g+CtQjWI9nWHVQtfNgzAAAQy/AAtYtgg");
	var mask_graphics_67 = new cjs.Graphics().p("EgCxAsRQlSlTjRmEQhWifhBisQhIi0gzjKQgri7gOjCQgWimAAitQAAjmAdjfQAdjKA5jBQAzi0BIitQBIilBWieQBzjDCXi0QBsiICCiCQCCiCCAhsQCmiIC7hsQDfiCDmhdQCthBC7grQC0gsDCgVQCtgPC7AAQCtAACmAPQBzAOBkAWQCCAVB6AdQDRAzDCBWQCtBICfBeQCXBdCXBsQClCCCfCeQCBCCBsCIQCQC0BzDDQBsC7BQDCQBACtAzCtQArCrAVC7QAdDRAADmQAACtgOCtQgWC7gkCtQgrC7hBC0QjfJVnpHqQuEODzzAAQzzAAt7uDg");
	var mask_graphics_68 = new cjs.Graphics().p("EgFGAsqQlglgjZmUQhailhEi0QhLi7g0jSQgtjCgPjLQgWisAAi0QAAjvAejoQAejRA7jKQA1i7BLi0QBLisBailQB3jKCei8QBwiOCHiHQCHiHCGhwQCsiODChwQDoiHDwhiQC0hDDDgtQC7gtDKgWQC0gPDCAAQC0AACsAPQB4APBpAWQCHAXB/AeQDZA0DKBaQC0BLClBhQCdBhCeBxQCsCHClCkQCGCHBxCOQCVC8B4DKQBxDDBSDKQBDCzA1C0QAtC0AWDCQAeDZAADvQAAC0gPC0QgWDDgmCzQgtDDhDC7QjoJun9H9QupOo0mAAQ0mAAufuog");
	var mask_graphics_69 = new cjs.Graphics().p("EgHcAtDQltltjimkQhdishGi6QhOjDg2jaQgvjKgQjSQgXizAAi7QAAj5AfjxQAfjZA+jSQA3jCBOi7QBOizBdirQB8jSCkjDQB0iTCMiMQCNiMCMh1QCziUDJh1QDxiMD5hlQC7hGDKgvQDCguDSgYQC7gPDKAAQC7AACzAPQB9AQBtAXQCMAYCEAfQDhA2DTBeQC6BNCrBmQCkBlCjB0QCzCMCrCsQCMCMB1CTQCcDDB8DSQB1DKBVDSQBGC7A3C7QAvC7AXDJQAfDhAAD5QAAC7gQC7QgXDKgnC7QguDKhHDDQjxKFoRIRQvMPN1aAAQ1ZAAvEvNg");
	var mask_graphics_70 = new cjs.Graphics().p("EgJxAtdQl7l8jqmzQhhiyhJjCQhQjKg5jiQgwjRgQjaQgZi6AAjCQAAkCAhj5QAgjiBAjaQA5jKBRjCQBQi5BhiyQCBjaCqjJQB5iaCRiRQCSiRCRh5QC5iaDRh5QD6iREDhpQDBhJDSgwQDKgxDagYQDCgQDSAAQDBAAC6AQQCBAQBxAZQCRAYCKAgQDqA5DaBgQDBBRCyBpQCpBpCqB5QC5CRCyCyQCRCRB5CaQCiDJCBDaQB5DSBZDaQBIDCA5DBQAwDCAZDSQAgDpAAECQAADCgQDCQgYDSgpDBQgwDShJDKQj6KeolIlQvxPx2MAAQ2NAAvovxg");
	var mask_graphics_71 = new cjs.Graphics().p("EgMGAt2QmJmJjznEQhki4hLjIQhUjRg6jqQgzjagQjiQgZjAAAjJQAAkLAhkDQAijqBCjhQA7jSBUjIQBTjBBli4QCFjhCwjRQB9ifCXiXQCXiWCWh+QDAifDah9QECiXEMhsQDIhMDagyQDRgyDigZQDIgRDaAAQDIAADBARQCGARB1AZQCWAZCOAhQDzA7DiBkQDIBUC4BtQCwBsCvB+QDBCWC4C4QCWCXB+CfQCnDRCGDhQB9DaBcDiQBLDIA7DJQAyDJAZDZQAiDyAAELQAADJgRDJQgZDZgqDJQgyDZhLDRQkEK2o4I5QwWQV2/AAQ3AAAwMwVg");
	var mask_graphics_72 = new cjs.Graphics().p("EgObAuPQmXmWj7nUQhoi+hOjQQhWjYg9jyQg0jhgRjqQgajHAAjQQAAkVAjkLQAijyBFjqQA9jYBXjQQBWjHBoi+QCKjqC2jYQCCilCcibQCcicCbiCQDHikDhiCQEMicEUhxQDQhNDhg0QDYg0DqgaQDQgSDhAAQDQAADHASQCKARB5AaQCcAaCTAjQD7A8DqBoQDPBXC/BwQC1BxC2CBQDHCcC/C/QCbCbCCClQCtDYCLDqQCBDhBgDpQBODQA8DQQA0DPAaDhQAjD6AAEVQAADQgSDQQgaDhgrDPQg0DhhODZQkMLOpMJMQw6Q63zAAQ3zAAwww6g");
	var mask_graphics_73 = new cjs.Graphics().p("EgQ1AupQmkmlkEnjQhrjFhRjWQhZjgg/j6Qg1jpgSjxQgbjOAAjXQAAkeAkkUQAjj7BIjxQA/jgBZjXQBajNBrjFQCPjyC8jfQCGiqChihQChihChiGQDOiqDoiGQEWihEdh1QDXhQDog2QDgg2DxgaQDXgSDoAAQDXAADOASQCPARB9AbQChAbCYAkQEEA/DxBrQDXBZDFB1QC7B0C8CGQDOChDFDFQChChCGCqQCzDfCPDyQCGDoBjDyQBQDXA/DWQA1DXAbDpQAkECAAEeQAADXgSDXQgbDogsDXQg2DohRDgQkVLmpgJhQxeRe4nAAQ4mAAxUxeg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(44).to({graphics:mask_graphics_44,x:361.7,y:231.3}).wait(1).to({graphics:mask_graphics_45,x:367.9,y:237.5}).wait(1).to({graphics:mask_graphics_46,x:374.1,y:243.6}).wait(1).to({graphics:mask_graphics_47,x:380.3,y:249.8}).wait(1).to({graphics:mask_graphics_48,x:386.5,y:256}).wait(1).to({graphics:mask_graphics_49,x:392.7,y:262.2}).wait(1).to({graphics:mask_graphics_50,x:398.8,y:268.3}).wait(1).to({graphics:mask_graphics_51,x:405,y:274.5}).wait(1).to({graphics:mask_graphics_52,x:411.2,y:280.7}).wait(1).to({graphics:mask_graphics_53,x:417.4,y:286.8}).wait(1).to({graphics:mask_graphics_54,x:423.6,y:293}).wait(1).to({graphics:mask_graphics_55,x:429.8,y:299.2}).wait(1).to({graphics:mask_graphics_56,x:436,y:305.4}).wait(1).to({graphics:mask_graphics_57,x:442.2,y:311.5}).wait(1).to({graphics:mask_graphics_58,x:448.3,y:317.7}).wait(1).to({graphics:mask_graphics_59,x:454.5,y:323.9}).wait(1).to({graphics:mask_graphics_60,x:460.7,y:330}).wait(1).to({graphics:mask_graphics_61,x:466.9,y:336.2}).wait(1).to({graphics:mask_graphics_62,x:473.1,y:342.4}).wait(1).to({graphics:mask_graphics_63,x:479.3,y:348.6}).wait(1).to({graphics:mask_graphics_64,x:485.5,y:354.7}).wait(1).to({graphics:mask_graphics_65,x:491.7,y:360.9}).wait(1).to({graphics:mask_graphics_66,x:497.8,y:367.1}).wait(1).to({graphics:mask_graphics_67,x:504,y:373.2}).wait(1).to({graphics:mask_graphics_68,x:510.2,y:379.4}).wait(1).to({graphics:mask_graphics_69,x:516.4,y:385.6}).wait(1).to({graphics:mask_graphics_70,x:522.6,y:391.8}).wait(1).to({graphics:mask_graphics_71,x:528.8,y:397.9}).wait(1).to({graphics:mask_graphics_72,x:535,y:404.1}).wait(1).to({graphics:mask_graphics_73,x:540.7,y:410.3}).wait(156));

	// sun13
	this.instance_8 = new lib.sun13();
	this.instance_8.parent = this;
	this.instance_8.setTransform(897,481.5,1,1,0,0,0,164,95.5);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	var maskedShapeInstanceList = [this.instance_8];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(44).to({_off:false},0).wait(1).to({alpha:1},13).wait(7).to({alpha:0},12).wait(27).to({alpha:1},13).wait(7).to({alpha:0},12).wait(17).to({alpha:1},13).wait(7).to({alpha:0},12).wait(38).to({_off:true},1).wait(5));

	// sun12
	this.instance_9 = new lib.sun12();
	this.instance_9.parent = this;
	this.instance_9.setTransform(853.5,448,1,1,0,0,0,120.5,129);
	this.instance_9.alpha = 0;
	this.instance_9._off = true;

	var maskedShapeInstanceList = [this.instance_9];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(44).to({_off:false},0).wait(27).to({alpha:1},13).wait(7).to({alpha:0},12).wait(27).to({alpha:1},13).wait(7).to({alpha:0},12).wait(28).to({alpha:1},13).wait(7).to({alpha:0},12).wait(1).to({_off:true},1).wait(5));

	// sun11
	this.instance_10 = new lib.sun11();
	this.instance_10.parent = this;
	this.instance_10.setTransform(880.5,378,1,1,0,0,0,180.5,199);
	this.instance_10.alpha = 0;
	this.instance_10._off = true;

	var maskedShapeInstanceList = [this.instance_10];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(44).to({_off:false},0).wait(13).to({alpha:1},13).wait(7).to({alpha:0},12).wait(27).to({alpha:1},13).wait(7).to({alpha:0},12).wait(13).to({alpha:1},13).wait(7).to({alpha:0},12).wait(30).to({_off:true},1).wait(5));

	// sun10
	this.instance_11 = new lib.sun10();
	this.instance_11.parent = this;
	this.instance_11.setTransform(764,410,1,1,0,0,0,140,167);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	var maskedShapeInstanceList = [this.instance_11];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(44).to({_off:false},0).wait(18).to({alpha:1},13).wait(7).to({alpha:0},12).wait(12).to({alpha:1},13).wait(7).to({alpha:0},12).wait(15).to({alpha:1},13).wait(7).to({alpha:0},12).wait(38).to({_off:true},1).wait(5));

	// sun9
	this.instance_12 = new lib.sun9();
	this.instance_12.parent = this;
	this.instance_12.setTransform(673.5,353,1,1,0,0,0,208.5,224);
	this.instance_12.alpha = 0;
	this.instance_12._off = true;

	var maskedShapeInstanceList = [this.instance_12];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(44).to({_off:false},0).wait(9).to({alpha:1},13).wait(7).to({alpha:0},12).wait(39).to({alpha:1},13).wait(7).to({alpha:0},12).wait(27).to({alpha:1},13).wait(7).to({alpha:0},12).wait(8).to({_off:true},1).wait(5));

	// sun8
	this.instance_13 = new lib.sun8();
	this.instance_13.parent = this;
	this.instance_13.setTransform(700.5,373,1,1,0,0,0,181.5,204);
	this.instance_13.alpha = 0;
	this.instance_13._off = true;

	var maskedShapeInstanceList = [this.instance_13];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(44).to({_off:false},0).wait(3).to({alpha:1},13).wait(7).to({alpha:0},12).wait(26).to({alpha:1},13).wait(7).to({alpha:0},12).wait(20).to({alpha:1},13).wait(7).to({alpha:0},12).wait(34).to({_off:true},1).wait(5));

	// sun7
	this.instance_14 = new lib.sun7();
	this.instance_14.parent = this;
	this.instance_14.setTransform(700.5,329,1,1,0,0,0,181.5,248);
	this.instance_14.alpha = 0;
	this.instance_14._off = true;

	var maskedShapeInstanceList = [this.instance_14];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(44).to({_off:false},0).wait(6).to({alpha:1},13).wait(7).to({alpha:0},12).wait(10).to({alpha:1},13).wait(7).to({alpha:0},12).wait(19).to({alpha:1},13).wait(7).to({alpha:0},12).wait(16).to({alpha:1},13).wait(7).to({alpha:0},12).to({_off:true},1).wait(5));

	// sun6
	this.instance_15 = new lib.sun6();
	this.instance_15.parent = this;
	this.instance_15.setTransform(694,372,1,1,0,0,0,188,205);
	this.instance_15.alpha = 0;
	this.instance_15._off = true;

	var maskedShapeInstanceList = [this.instance_15];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(44).to({_off:false},0).wait(24).to({alpha:1},13).wait(7).to({alpha:0},12).wait(29).to({alpha:1},13).wait(7).to({alpha:0},12).wait(26).to({alpha:1},13).wait(7).to({alpha:0},12).wait(4).to({_off:true},1).wait(5));

	// sun5
	this.instance_16 = new lib.sun5();
	this.instance_16.parent = this;
	this.instance_16.setTransform(684,353.5,1,1,0,0,0,252,224.5);
	this.instance_16.alpha = 0;
	this.instance_16._off = true;

	var maskedShapeInstanceList = [this.instance_16];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(44).to({_off:false},0).wait(17).to({alpha:1},13).wait(7).to({alpha:0},12).wait(15).to({alpha:1},13).wait(7).to({alpha:0},12).wait(40).to({alpha:1},13).wait(7).to({alpha:0},12).wait(11).to({_off:true},1).wait(5));

	// sun4
	this.instance_17 = new lib.sun4();
	this.instance_17.parent = this;
	this.instance_17.setTransform(720,407.5,1,1,0,0,0,214,170.5);
	this.instance_17.alpha = 0;
	this.instance_17._off = true;

	var maskedShapeInstanceList = [this.instance_17];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(44).to({_off:false},0).wait(4).to({alpha:1},13).wait(7).to({alpha:0},12).wait(18).to({alpha:1},13).wait(7).to({alpha:0},12).wait(24).to({alpha:1},13).wait(7).to({alpha:0},12).wait(37).to({_off:true},1).wait(5));

	// sun3
	this.instance_18 = new lib.sun3();
	this.instance_18.parent = this;
	this.instance_18.setTransform(661.5,486,1,1,0,0,0,274.5,226);
	this.instance_18.alpha = 0;
	this.instance_18._off = true;

	var maskedShapeInstanceList = [this.instance_18];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(44).to({_off:false},0).wait(10).to({alpha:1},13).wait(7).to({alpha:0},12).wait(27).to({alpha:1},13).wait(7).to({alpha:0},12).wait(28).to({alpha:1},13).wait(7).to({alpha:0},12).wait(18).to({_off:true},1).wait(5));

	// sun2
	this.instance_19 = new lib.sun2();
	this.instance_19.parent = this;
	this.instance_19.setTransform(657,535.5,1,1,0,0,0,225,175.5);
	this.instance_19.alpha = 0;
	this.instance_19._off = true;

	var maskedShapeInstanceList = [this.instance_19];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(44).to({_off:false},0).wait(18).to({alpha:1},13).wait(7).to({alpha:0},12).wait(27).to({alpha:1},13).wait(7).to({alpha:0},12).wait(28).to({alpha:1},13).wait(7).to({alpha:0},12).wait(10).to({_off:true},1).wait(5));

	// sun1
	this.instance_20 = new lib.sun1("synched",0);
	this.instance_20.parent = this;
	this.instance_20.setTransform(611,575.5,1,1,0,0,0,-271,-135.5);
	this.instance_20.alpha = 0;
	this.instance_20._off = true;

	var maskedShapeInstanceList = [this.instance_20];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(44).to({_off:false},0).wait(1).to({startPosition:0},0).to({alpha:1},13).wait(7).to({startPosition:0},0).to({alpha:0},12).wait(27).to({startPosition:0},0).to({alpha:1},13).wait(7).to({startPosition:0},0).to({alpha:0},12).wait(20).to({startPosition:0},0).to({alpha:1},13).wait(7).to({startPosition:0},0).to({alpha:0},12).wait(35).to({startPosition:0},0).to({_off:true},1).wait(5));

	// cut2_sun.png
	this.instance_21 = new lib.sun();
	this.instance_21.parent = this;
	this.instance_21.setTransform(727,304,1,1,0,0,0,960,708);
	this.instance_21.alpha = 0.238;
	this.instance_21._off = true;

	var maskedShapeInstanceList = [this.instance_21];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(44).to({_off:false},0).wait(167).to({alpha:1},12).to({_off:true},1).wait(5));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(1344,684,147,279);
// library properties:
lib.properties = {
	id: 'B8170198AA2A4AD0BA13AFA47D2CB075',
	width: 1400,
	height: 730,
	fps: 30,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/ani_1400x730_cut2/_1.png", id:"_1"},
		{src:"images/ani_1400x730_cut2/_10.png", id:"_10"},
		{src:"images/ani_1400x730_cut2/_11.png", id:"_11"},
		{src:"images/ani_1400x730_cut2/_12.png", id:"_12"},
		{src:"images/ani_1400x730_cut2/_13.png", id:"_13"},
		{src:"images/ani_1400x730_cut2/_2.png", id:"_2"},
		{src:"images/ani_1400x730_cut2/_3.png", id:"_3"},
		{src:"images/ani_1400x730_cut2/_4.png", id:"_4"},
		{src:"images/ani_1400x730_cut2/_5.png", id:"_5"},
		{src:"images/ani_1400x730_cut2/_6.png", id:"_6"},
		{src:"images/ani_1400x730_cut2/_7.png", id:"_7"},
		{src:"images/ani_1400x730_cut2/_8.png", id:"_8"},
		{src:"images/ani_1400x730_cut2/_9.png", id:"_9"},
		{src:"images/ani_1400x730_cut2/cut2_cask.png", id:"cut2_cask"},
		{src:"images/ani_1400x730_cut2/cut2_object.png", id:"cut2_object"},
		{src:"images/ani_1400x730_cut2/cut2_object_1.png", id:"cut2_object_1"},
		{src:"images/ani_1400x730_cut2/cut2_object_2.png", id:"cut2_object_2"},
		{src:"images/ani_1400x730_cut2/cut2_object_3.png", id:"cut2_object_3"},
		{src:"images/ani_1400x730_cut2/cut2_object_4.png", id:"cut2_object_4"},
		{src:"images/ani_1400x730_cut2/cut2_object_5.png", id:"cut2_object_5"},
		{src:"images/ani_1400x730_cut2/cut2_sun.png", id:"cut2_sun"},
		{src:"images/ani_1400x730_cut2/cut2_title.png", id:"cut2_title"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['B8170198AA2A4AD0BA13AFA47D2CB075'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, ani_1400x730_cut2 = ani_1400x730_cut2||{});
var createjs, ani_1400x730_cut2;