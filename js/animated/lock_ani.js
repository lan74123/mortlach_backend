(function (cjs, an) {

var p; // shortcut to reference prototypes
var lib={};var ss={};var img={};
lib.ssMetadata = [];


// symbols:



(lib.kock01 = function() {
	this.initialize(img.kock01);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,224,155);


(lib.kock2 = function() {
	this.initialize(img.kock2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,224,155);


(lib.kock3 = function() {
	this.initialize(img.kock3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,224,155);


(lib.lock0 = function() {
	this.initialize(img.lock0);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,224,155);


(lib.shadow = function() {
	this.initialize(img.shadow);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,750,370);// helper functions:

function mc_symbol_clone() {
	var clone = this._cloneProps(new this.constructor(this.mode, this.startPosition, this.loop));
	clone.gotoAndStop(this.currentFrame);
	clone.paused = this.paused;
	clone.framerate = this.framerate;
	return clone;
}

function getMCSymbolPrototype(symbol, nominalBounds, frameBounds) {
	var prototype = cjs.extend(symbol, cjs.MovieClip);
	prototype.clone = mc_symbol_clone;
	prototype.nominalBounds = nominalBounds;
	prototype.frameBounds = frameBounds;
	return prototype;
	}


(lib.元件6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// 圖層_1
	this.instance = new lib.kock01();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.元件6, new cjs.Rectangle(0,0,224,155), null);


(lib.元件4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// 圖層_1
	this.instance = new lib.kock01();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.元件4, new cjs.Rectangle(0,0,224,155), null);


(lib.元件3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// 圖層_1
	this.instance = new lib.lock0();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.元件3, new cjs.Rectangle(0,0,224,155), null);


(lib.元件2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// 圖層_1
	this.instance = new lib.kock2();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.元件2, new cjs.Rectangle(0,0,224,155), null);


(lib.元件1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// 圖層_1
	this.instance = new lib.kock3();
	this.instance.parent = this;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(1));

}).prototype = getMCSymbolPrototype(lib.元件1, new cjs.Rectangle(0,0,224,155), null);


(lib.元件7 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_22 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(22).call(this.frame_22).wait(1));

	// 2.81鎖-上03
	this.instance = new lib.元件1();
	this.instance.parent = this;
	this.instance.setTransform(387,161.5,1,1,0,0,0,112,77.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},3).wait(20));

	// 2.81鎖-上02
	this.instance_1 = new lib.元件2();
	this.instance_1.parent = this;
	this.instance_1.setTransform(387,161.5,1,1,0,0,0,112,77.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(3).to({_off:false},0).to({_off:true},1).wait(19));

	// 2.81鎖-上01
	this.instance_2 = new lib.元件3();
	this.instance_2.parent = this;
	this.instance_2.setTransform(387,161.5,1,1,0,0,0,112,77.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(4).to({_off:false},0).to({_off:true},2).wait(17));

	// 圖層_9
	this.instance_3 = new lib.元件4();
	this.instance_3.parent = this;
	this.instance_3.setTransform(387,161.5,1,1,0,0,0,112,77.5);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(6).to({_off:false},0).wait(2).to({y:162.5},0).to({_off:true},1).wait(14));

	// 圖層_3
	this.instance_4 = new lib.元件6();
	this.instance_4.parent = this;
	this.instance_4.setTransform(387,161.5,1,1,0,0,0,112,77.5);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(9).to({_off:false},0).wait(14));

	// 2.81鎖-下01
	this.instance_5 = new lib.元件6();
	this.instance_5.parent = this;
	this.instance_5.setTransform(387,161.5,1,1,0,0,0,112,77.5);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(10).to({_off:false},0).wait(13));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(275,84,224,155);


// stage content:
(lib.lock_ani = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_22 = function() {
		this.stop();
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).wait(22).call(this.frame_22).wait(1));

	// 圖層_24 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("AbCN/IAA7BIKAAAIAAbBg");
	mask.setTransform(237,89.5);

	// 元件 7
	this.instance = new lib.元件1();
	this.instance.parent = this;
	this.instance.setTransform(352,93.5,1,1,0,0,0,112,77.5);

	this.instance_1 = new lib.元件7("synched",0,false);
	this.instance_1.parent = this;
	this.instance_1.setTransform(261.5,152,1,1,0,0,0,296.5,220);

	var maskedShapeInstanceList = [this.instance,this.instance_1];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance}]}).to({state:[{t:this.instance_1}]},3).wait(20));

	// 圖層_23 (mask)
	var mask_1 = new cjs.Shape();
	mask_1._off = true;
	mask_1.graphics.p("AWvOdIAA8kIJOAAIAAckg");
	mask_1.setTransform(204.5,92.5);

	// 元件 7
	this.instance_2 = new lib.元件1();
	this.instance_2.parent = this;
	this.instance_2.setTransform(352,93.5,1,1,0,0,0,112,77.5);

	this.instance_3 = new lib.元件7("synched",0,false);
	this.instance_3.parent = this;
	this.instance_3.setTransform(261.5,152,1,1,0,0,0,296.5,220);

	var maskedShapeInstanceList = [this.instance_2,this.instance_3];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_1;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_2}]}).to({state:[{t:this.instance_3}]},2).wait(21));

	// 圖層_22 (mask)
	var mask_2 = new cjs.Shape();
	mask_2._off = true;
	mask_2.graphics.p("ASDPPIAA9+IJYAAIAAd+g");
	mask_2.setTransform(175.5,97.5);

	// 元件 7
	this.instance_4 = new lib.元件1();
	this.instance_4.parent = this;
	this.instance_4.setTransform(352,93.5,1,1,0,0,0,112,77.5);

	this.instance_5 = new lib.元件7("synched",0,false);
	this.instance_5.parent = this;
	this.instance_5.setTransform(261.5,152,1,1,0,0,0,296.5,220);

	var maskedShapeInstanceList = [this.instance_4,this.instance_5];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_2;
	}

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_4}]}).to({state:[{t:this.instance_5}]},1).wait(22));

	// 圖層_21 (mask)
	var mask_3 = new cjs.Shape();
	mask_3._off = true;
	mask_3.graphics.p("AMlOnIAA9MIKeAAIAAdMg");
	mask_3.setTransform(147.5,93.5);

	// 元件 7
	this.instance_6 = new lib.元件7("synched",0,false);
	this.instance_6.parent = this;
	this.instance_6.setTransform(261.5,152,1,1,0,0,0,296.5,220);

	var maskedShapeInstanceList = [this.instance_6];

	for(var shapedInstanceItr = 0; shapedInstanceItr < maskedShapeInstanceList.length; shapedInstanceItr++) {
		maskedShapeInstanceList[shapedInstanceItr].mask = mask_3;
	}

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(23));

	// 圖層_15
	this.instance_7 = new lib.shadow();
	this.instance_7.parent = this;
	this.instance_7.setTransform(-35,-68);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(23));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(315,75,750,370);
// library properties:
lib.properties = {
	id: 'DABF261E28D7D2448A3CAF51365F3F8E',
	width: 700,
	height: 286,
	fps: 18,
	color: "#FFFFFF",
	opacity: 1.00,
	manifest: [
		{src:"images/lock_ani/kock01.png?1542867377642", id:"kock01"},
		{src:"images/lock_ani/kock2.png?1542867377642", id:"kock2"},
		{src:"images/lock_ani/kock3.png?1542867377642", id:"kock3"},
		{src:"images/lock_ani/lock0.png?1542867377642", id:"lock0"},
		{src:"images/lock_ani/shadow.png?1542867377642", id:"shadow"}
	],
	preloads: []
};



// bootstrap callback support:

(lib.Stage = function(canvas) {
	createjs.Stage.call(this, canvas);
}).prototype = p = new createjs.Stage();

p.setAutoPlay = function(autoPlay) {
	this.tickEnabled = autoPlay;
}
p.play = function() { this.tickEnabled = true; this.getChildAt(0).gotoAndPlay(this.getTimelinePosition()) }
p.stop = function(ms) { if(ms) this.seek(ms); this.tickEnabled = false; }
p.seek = function(ms) { this.tickEnabled = true; this.getChildAt(0).gotoAndStop(lib.properties.fps * ms / 1000); }
p.getDuration = function() { return this.getChildAt(0).totalFrames / lib.properties.fps * 1000; }

p.getTimelinePosition = function() { return this.getChildAt(0).currentFrame / lib.properties.fps * 1000; }

an.bootcompsLoaded = an.bootcompsLoaded || [];
if(!an.bootstrapListeners) {
	an.bootstrapListeners=[];
}

an.bootstrapCallback=function(fnCallback) {
	an.bootstrapListeners.push(fnCallback);
	if(an.bootcompsLoaded.length > 0) {
		for(var i=0; i<an.bootcompsLoaded.length; ++i) {
			fnCallback(an.bootcompsLoaded[i]);
		}
	}
};

an.compositions = an.compositions || {};
an.compositions['DABF261E28D7D2448A3CAF51365F3F8E'] = {
	getStage: function() { return exportRoot.getStage(); },
	getLibrary: function() { return lib; },
	getSpriteSheet: function() { return ss; },
	getImages: function() { return img; }
};

an.compositionLoaded = function(id) {
	an.bootcompsLoaded.push(id);
	for(var j=0; j<an.bootstrapListeners.length; j++) {
		an.bootstrapListeners[j](id);
	}
}

an.getComposition = function(id) {
	return an.compositions[id];
}



})(createjs = createjs||{}, lock_ani = lock_ani||{});
var createjs, lock_ani;