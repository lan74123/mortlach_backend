/**
 *
 * @authors Eric Hsiao
 *
 */

formPage = function () {
	//private menbers
	var isMobile = false;
	var isAutoScroll = true;
	var swiper;

	//private methods
	function init() {
		console.log('main is loaded.');

		if ($(window).width() < 480) {
			isMobile = true;
			isAutoScroll = false;
    }
    
    var parameterUrl = setParameterByName('page','');
    var parameter = parameterUrl.split('?');
    window.history.replaceState({}, "", "?" + parameter[1]);

		if (!isMobile) {
			/*$('#fullpage').fullpage({
          anchors: ['homePage', 'introPage', 'locationPage', 'chargePage', 'rulePage'],
          menu: '#menu',
          scrollOverflow: true,
          css3: true,
          scrollBar: true,
          autoScrolling: false,
          onLeave: function(index, nextIndex, direction){
						console.log("onLeave--" + "index: " + index + " nextIndex: " + nextIndex + " direction: " +  direction);
					}
        });*/

        if($(window).height() < 800){
          $('.home__CTA').find('img').attr('src','images/page/form/register-card-s.jpg');
          $('.register__bg').css({'padding-top':'45px'});  
          $('.section__card').css({'padding-top':'15%'});    
          $('.header__logo').css({
            'width':'100px',
            'margin':'20px auto'
          });
          TweenMax.set('.thankyou__content__pc',{scale:.7});
        }
        
        $('.thankyou__content__pc').css({'display':'block'});
        $('.thankyou__content__mobile').css({'display':'none'});
        
		}else{
      $('.thankyou__content__pc').css({'display':'none'});
      $('.thankyou__content__mobile').css({'display':'block'});
    }

		$('.btn-submit').click(function (e) {
			e.preventDefault();
			$('.form_data_submit').click();
		});

		$("#mainForm").on("submit", function (e) {
			e.preventDefault();
			if (checkForm()) {
				sendForm();
			}
    });

    $('.thankyou__content').click(function (e) { 
      e.preventDefault();
      TweenMax.to('.pop__thankyou',0.3,{autoAlpha:0});
    });

    swiper = new Swiper('.swiper-container',{
    	spaceBetween: 30
    });

    swiper.on('slideChange', function (e) {
	  console.log(swiper.activeIndex);
	  $('.location__menu__btn-' + swiper.previousIndex).removeClass('location__menu__btn-active');
	  $('.location__menu__btn-' + swiper.activeIndex).addClass('location__menu__btn-active');
	 });

    $('.location__menu__btn-0').click(function (e) { 
      e.preventDefault();
      swiper.slideTo(0);
    });

    $('.location__menu__btn-1').click(function (e) { 
      e.preventDefault();
      swiper.slideTo(1);
    });

    $('.location__menu__btn-2').click(function (e) { 
      e.preventDefault();
      swiper.slideTo(2);
    });

    initBirthdaySelect();

    // showThankYou();

  }

  function initBirthdaySelect(){

      for (var y = 2000; y > 1900; y--) {
        var _option = $('<option></option>').appendTo($('.data__birthday__year'));
        _option.attr('value', y);
        _option.text(y);
      }

      for (var m = 1; m < 13; m++) {
        var _option = $('<option></option>').appendTo($('.data__birthday__month'));
        _option.attr('value', getNum(m));
        _option.text(getNum(m));
      }

      for (var d = 1; d < 32; d++) {
        var _option = $('<option></option>').appendTo($('.data__birthday__day'));
        _option.attr('value', getNum(d));
        _option.text(getNum(d));
      }
    }

    function getNum(_num){
      var _string;
      if(parseInt(_num) / 10 < 1){
        _string = '0' + _num.toString();
      }else{
        _string = _num.toString();
      }
      return _string;
    }
  
  function showThankYou(){
    TweenMax.set('.pop__thankyou',{autoAlpha:0});
    $('.pop__thankyou').css({'display':'block'});
    TweenMax.to('.pop__thankyou',0.5,{autoAlpha:1});
  }

	function checkForm() {
		var _result = false;
		if (!$("#gridCheck1").prop("checked")) {
			alert('您尚未同意本活動辦法、條款與約定。');
		} else {
			_result = true;
		}

		return _result;
	}

	function sendForm() {

		var yy = $("#homePage").find('.data__birthday__year').val();
		var mm = $("#homePage").find('.data__birthday__month').val();
    var dd = $("#homePage").find('.data__birthday__day').val();
    
    var utm_source = '';
    var utm_medium = '';
    var utm_campaign = '';

    if(getParameterByName('utm_source') != null) utm_source = getParameterByName('utm_source');
    if(getParameterByName('utm_medium') != null) utm_medium = getParameterByName('utm_medium');
    if(getParameterByName('utm_campaign') != null) utm_campaign = getParameterByName('utm_campaign');

		var _data = {
			"FamilyName": $("#formFamilyName").val(),
			"GivenName": $("#formGivenName").val(),
			"Sex": $("input[name=inlineRadioOptions]:checked").val(),
			"Birthday": yy + '/' + mm + '/' + dd,
			"Phone": $("#formPhone").val(),
			"Email": $("#formEmail").val(),
			"Income": $("#formIncome").val(),
			"Job": $("#formJob").val(),
			"Session": $("#formSession").val(),
      "Referrer": "",
      "utm_source":utm_source,
      "utm_medium":utm_medium,
      "utm_campaign":utm_campaign
		}

		$.ajax({
			type: "POST",
			url: "api/save_data.ashx",
			data: _data,
			dataType: "json",
			success: function (response) {
				console.log('================== response ===================');
				console.log(response);

				if (response.Result) {
          //alert('報名成功，感謝您的填寫。');
          //alert('感謝您的報名,每堂課將隨機選出20名參加者\n並在專人通知後,發送入場憑證簡訊\n專人將於每堂課報名結束後,開始聯絡確認出席相關事宜。\n以電話待核對報名資料及參加場次，資料無誤後將發送入場憑證簡訊,方視為報名成功。 \n備註：活動當日需憑入場簡訊及證件作為入場依據 \n若無法於課程開始前2天聯繫上，則視同放棄，主辦單位將再選出候補出席名額。\n\n*每人可報名不同課程，但由於名額有限，每人只有一次機會出席參加相同的課程*\n');
          showThankYou();
          document.getElementById("mainForm").reset();
				} else {
					alert(response.ErrorMsg);
				}
			}
		});
	}

	//constructor
	{
		$(document).ready(function () {
			init();
		});
	}

	//public
	return {};
};


menu = function () {
	//private menbers

	//private methods
	function init() {
		console.log('menu is loaded.');
		$('#main-nav .nav-link').on('click', function () {
			$('#main-nav').collapse('hide');
		})
	}

	//constructor
	{
		$(document).ready(function () {
			init();
		});
	}

	//public
	return {

	};
}

var formPage = new formPage();
var menu = new menu();