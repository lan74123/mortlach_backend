/**
 *
 * @authors Eric Hsiao
 *
 */

indexPage = function () {

  //private menbers
  var secret_1_ani_0, secret_1_ani_1, secret_1_ani_2;


  //private methods
  function init() {
    console.log('indexPage is loaded.');
    
    if(getParameterByName('page') == 'form'){
      var _URL_parameter = setParameterByName('page','',URL_parameter);

      setTimeout(function () {        
        location.href = 'form.html' + _URL_parameter;
      }, 300);
    }

    onResize();
    $(window).resize(function () {
      onResize();
    });

    wow = new WOW().init();

    initSecret_1();
    initSecret_2();
    initSecret_3();
    initSecret_4();


    $('.header__btn-img').click(function (e) {
      e.preventDefault();
      alert('header__btn-gotop');
      location.href = 'form.html' + URL_parameter;
    });

    $(window).scroll(function () {
      checkAnimateTop();
    });

    intoPage();
  }

  function intoPage() {
    TweenMax.from('.main__banner', 1, { autoAlpha: 0 });
  }

  function onResize() {
    mainBanner.onResize();

    if (isMobile) {
      var ani_dW = ($(window).width() / 840);
      TweenMax.set('.animated__container', { scale: ani_dW });
      $('.secret__animated').css('height', '300px');

      TweenMax.set('.lock', { scale: 0.7 });
    } else {
      TweenMax.set('.animated__container', { scale: 1 });
      $('.secret__animated').css('height', '600px');
    }
  }

  function initSecret_1() {

    loadAnimate('ani_1400x730_cut3', $('.secret_1__animated__0 .animated__container'),
      function (_exportRoot) {
        secret_1_ani_0 = _exportRoot;
      });

    loadAnimate('ani_1400x730_cut1', $('.secret_1__animated__1 .animated__container'),
      function (_exportRoot) {
        secret_1_ani_1 = _exportRoot;
      });

    loadAnimate('ani_1400x730_cut2', $('.secret_1__animated__2 .animated__container'),
      function (_exportRoot) {
        secret_1_ani_2 = _exportRoot;
        checkAnimateTop();
      });
  }

  function checkAnimateTop() {
    var scrollTop = $(window).scrollTop();

    for (var i = 0; i < 3; i++) {
      var offsetTop = $('.secret_1__animated__' + i).offset().top;
      var offsetCount = Math.floor(scrollTop + $(window).height() - offsetTop);

      if (offsetCount > $(window).height() * 2) {
        if (i == 0) if (secret_1_ani_0) secret_1_ani_0.gotoAndStop(0);
        if (i == 1) if (secret_1_ani_1) secret_1_ani_1.gotoAndStop(0);
        if (i == 2) if (secret_1_ani_2) secret_1_ani_2.gotoAndStop(0);


      } else if (offsetCount > $(window).height() * .2) {
        if (i == 0) if (secret_1_ani_0) secret_1_ani_0.play();
        if (i == 1) if (secret_1_ani_1) secret_1_ani_1.play();
        if (i == 2) if (secret_1_ani_2) secret_1_ani_2.play();
        $('.secret_1__animated__' + i).find('canvas').css('display', 'block');
      } else if (offsetCount < -$(window).height() * .5) {
        if (i == 0) if (secret_1_ani_0) secret_1_ani_0.gotoAndStop(0);
        if (i == 1) if (secret_1_ani_1) secret_1_ani_1.gotoAndStop(0);
        if (i == 2) if (secret_1_ani_2) secret_1_ani_2.gotoAndStop(0);
        $('.secret_1__animated__' + i).find('canvas').css('display', 'none');
      }
    }
  }

  function initSecret_2() {
    $('.main__banner').css('height', '100vh');
    $('body').css('height', 'auto');

    var _time = 2;
    $('.indexPage .secret_2 .pin__container').css('height', (_time + 0.5) * 100 + 'vh');

    $(".pin__content").pin({
      containerSelector: ".pin__container",
      padding: { top: 0 }
    });

    if ($(window).height() < 900 || isMobile) {
      var _distillerScale = $('.distiller__stage').height() / 1200;


      //TweenMax.set($('.distiller__txt').find('.container-txt'), { scale: 0.8, transformOrigin: 'center top' });
      $('.distiller__txt').find('p').css({
        'max-width': '80%',
        'margin': '0 auto',
        'padding-bottom': '20px'
      });

      if (!isMobile) {
        $('.distiller__txt').find('p').css({
          'font-size': '1.05em',
          'padding-bottom': '10px',
          'margin-bottom': '0px'
        });

        _distillerScale = $('.distiller__stage').height() / 900;
        TweenMax.set($('.distiller__container'), { scale: _distillerScale, y: 20 });
      } else {
        TweenMax.set($('.distiller__container'), { scale: _distillerScale, y: -40 });
      }

    } else {
      var _distillerScale = $('.distiller__stage').height() / 1000;
      if (_distillerScale > 1) _distillerScale = 1;
      TweenMax.set($('.distiller__container'), { scale: _distillerScale, y: 30 * _distillerScale });

      $('.distiller__txt').find('p').css({
        'padding-bottom': '0px',
        'margin-bottom': '0px'
      });
    }

    /**********************************/

    var distillerTimeline = new TimelineLite();
    distillerTimeline.set($('.distiller-step-bg'), { autoAlpha: 1 });
    distillerTimeline.set('.distiller__content', { scale: .75, y: -50, transformOrigin: 'center center' });
    distillerTimeline.set($('.distiller__step'), { autoAlpha: 0 });
    distillerTimeline.set($('.distiller-step-bg'), { autoAlpha: 1 });

    distillerTimeline.from('.distiller__content', 0.5, { y: -100 });

    // distillerTimeline.from('.secret_2__step__0', 0.5, {y:100, autoAlpha:0});
    distillerTimeline.from('.distiller__bg', 0.5, { autoAlpha: 0 });

    distillerTimeline.to('.distiller__content', 1, { y: -100, scale: .65 }, '-=0.5');
    distillerTimeline.to('.distiller-step-bg', 0.5, { autoAlpha: 0.5, onComplete: distillerSVGPlay }, '-=0.5');
    // distillerTimeline.from('.distiller-step-0', 0.5, {autoAlpha:0},'-=0.3');
    distillerTimeline.from('.distiller__txt__bg', 0.5, { autoAlpha: 0 }, '-=0.3');


    distillerTimeline.to('.secret_2__step__0', 1, { delay: 1, y: -100, autoAlpha: 0 });
    distillerTimeline.to('.distiller__txt__bg', 0.5, { autoAlpha: 0 }, '-=1');
    // distillerTimeline.to('.distiller-step-0', 0.5, {autoAlpha:0},'-=0.3');

    distillerTimeline.from('.secret_2__step__1', 1, { y: 100, autoAlpha: 0 });
    distillerTimeline.to('.distiller__txt__bg', 0.5, { autoAlpha: 1 }, '-=1');

    // distillerTimeline.from('.distiller-step-1', 0.5, {autoAlpha:0},'-=0.3');
    //distillerTimeline.to('.secret_2__step__1', 0.5, {delay:1,y:-100, autoAlpha:0});
    // distillerTimeline.to('.distiller-step-1', 0.5, {autoAlpha:0},'-=0.3');
    /*distillerTimeline.from('.secret_2__step__2', 0.5, {y:100, autoAlpha:0});
    distillerTimeline.from('.distiller-step-2', 0.5, {autoAlpha:0},'-=0.3');    
    distillerTimeline.to('.distiller-step-2', 0.5, {delay:1,autoAlpha:0});
    distillerTimeline.from('.distiller-step-3', 0.5, {autoAlpha:0},'-=0.3');
    distillerTimeline.to('.distiller-step-3', 0.5, {delay:1,autoAlpha:0});
    distillerTimeline.from('.distiller-step-4', 0.5, {autoAlpha:0},'-=0.3');
    distillerTimeline.to('.secret_2__step__2', 0.5, {delay:0,autoAlpha:1});*/

    distillerTimeline.to('.distiller__txt__bg', 0.5, { delay: 1, autoAlpha: 0 });
    distillerTimeline.to('.distiller__bg', 0.5, { autoAlpha: 0 });
    // distillerTimeline.to('.distiller__content', 0.5, { delay: 1, y: 0 });

    distillerTimeline.stop();

    var SVGTimeline = new TimelineLite({
      onComplete: function () {
        this.restart();
      }
    });

    SVGTimeline.to('.distiller-step-0', 0.5, { autoAlpha: 1, onStart: onSVGStart, onStartParams: [0] });
    SVGTimeline.to('.distiller-step-0', 0.3, { delay: 1.5, autoAlpha: 0 });

    SVGTimeline.to('.distiller-step-1', 0.5, { autoAlpha: 1, onStart: onSVGStart, onStartParams: [1] });
    SVGTimeline.to('.distiller-step-1', 0.3, { delay: 1.2, autoAlpha: 0 });

    SVGTimeline.to('.distiller-step-2', 0.5, { autoAlpha: 1, onStart: onSVGStart, onStartParams: [2] });
    SVGTimeline.to('.distiller-step-2', 0.3, { delay: 1.2, autoAlpha: 0 });

    SVGTimeline.to('.distiller-step-3', 0.5, { autoAlpha: 1, onStart: onSVGStart, onStartParams: [3] });
    // SVGTimeline.to('.distiller-step-3',0.3,{autoAlpha:0});

    SVGTimeline.to('.distiller-step-4', 0, { delay: 1.8, autoAlpha: 1, onStart: onSVGStart, onStartParams: [4] });
    SVGTimeline.to('.distiller-step-3', 0.3, { delay: 3, autoAlpha: 0 });
    SVGTimeline.to('.distiller-step-4', 0.3, { autoAlpha: 0 }, "-=0.3");

    SVGTimeline.stop();

    function distillerSVGPlay() {
      console.log('distillerSVGPlay');

      SVGTimeline.play(0);
    }

    function onSVGStart(_count) {
      //console.log(_count);
      $('#distiller-step-0').contents().find("svg").css('display', 'none');
      $('#distiller-step-1').contents().find("svg").css('display', 'none');
      $('#distiller-step-2').contents().find("svg").css('display', 'none');
      $('#distiller-step-3').contents().find("svg").css('display', 'none');
      $('#distiller-step-4').contents().find("svg").css('display', 'none');

      $('#distiller-step-' + _count).attr('src', $('.distiller-step-' + _count).attr('src'));
      $('#distiller-step-' + _count).contents().find("svg").css('display', 'block');
    }

    $(window).scroll(function () {

      var scrollTop = $(window).scrollTop();
      var distiller = $('.pin__container').offset().top;
      var distillerCount = Math.floor((scrollTop - distiller) / $(window).height() * 100 / _time) / 100 + 0.1;
      // console.log(distillerCount);

      if (distillerCount < 0) distillerCount = 0;
      distillerTimeline.progress(distillerCount);

    });
  }

  function initSecret_3() {
    // var swiper = new Swiper('.product-container', {
    //   slidesPerView: 3,
    //   spaceBetween: 30,
    //   pagination: {
    //     clickable: true,
    //   },
    // });

    TweenMax.set($('.product-slide').find('img'), { scale: 0.9 });
    TweenMax.set($('.product-slide').find('.product-picture'), { scale: 0.9, transformOrigin: 'center 300px' });

    // TweenMax.set(('.product__effect'),{autoAlpha:0});
    // $('.product__effect').css('display','block');
    // $('.product-slide').css('width','30%');

    $('.product-slide').mouseover(function () {
      var _img = $(this).find('img');
      // TweenMax.to($(this),1,{css:{'width':'33%'},ease: Expo.easeOut});
      TweenMax.to(_img, 1, { scale: 1, ease: Expo.easeOut });
      TweenMax.to($(this).find('.product__effect'), 1, { delay: 0.1, autoAlpha: 1, ease: Expo.easeOut });
    });

    $('.product-slide').mouseout(function () {
      var _img = $(this).find('img');
      // TweenMax.to($(this),1,{css:{'width':'30%'},ease: Expo.easeOut});
      TweenMax.to(_img, 0.3, { scale: 0.9 });
      TweenMax.to($(this).find('.product__effect'), 1, { delay: 0.1, autoAlpha: 0, ease: Expo.easeOut });
    });

    TweenMax.set('.product-box', { css: { 'width': '70%' }, autoAlpha: 0, scale: .8 });

    // TweenMax.to('.product-box',3,{css:{'width':'100%'},delay:0.5,autoAlpha:1,scale:1,ease: Expo.easeOut});
    // TweenMax.to('.product__effect',1,{delay:1,autoAlpha:0});

    ScrollReveal().reveal('.product-container', {
      afterReveal: function (el) {
        TweenMax.to('.product-box', 3, { css: { 'width': '100%' }, autoAlpha: 1, scale: 1, ease: Expo.easeOut });
        TweenMax.to('.product__effect', 1, { delay: 1, autoAlpha: 0 });
      }
    });

  }



  function initSecret_4() {

    loadAnimate('lock_ani', $('.lock'),
      function (_exportRoot) {
        lockAni = _exportRoot;

        ScrollReveal().reveal('.lock', {
          reset: true, // 每次都啟動動畫
          delay: 0.3,
          beforeReveal: function (domEl) { }, // 當啟動顯示前，則執行此函式
          beforeReset: function (domEl) { }, // 當重啟前，則執行此函式
          afterReveal: function (domEl) {
            lockAni.play();
          }, // 當啟動後，則執行此函式
          afterReset: function (domEl) { } // 當重啟後，則執行此函式
        });
      });

    /**********************************/

    TweenMax.set($('.btn-form'), { scale: 0.95 });

    $('.btn-form').mouseover(function () {
      TweenMax.to($(this), 0.8, { scale: 1, ease: Expo.easeOut });
    });

    $('.btn-form').mouseout(function () {
      TweenMax.to($(this), 0.3, { scale: 0.95 });
    });
  }

  //constructor

  {
    $(document).ready(function () {
      init();
    });
  }

  //public

  return {

  };
};

var indexPage = new indexPage();
