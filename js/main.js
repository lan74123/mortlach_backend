/**
 *
 * @authors Eric Hsiao
 *
 */

var isMobile = false;
var URL_parameter;

main = function () {

  //private menbers


  //private methods
  function init() {
    console.log('main is loaded.');

    TweenMax.to('.loading', 1, { delay: 0.5, autoAlpha: 0 });

    var _url = location.href;
    if (_url.split("?")[1] != undefined) {
      var _temp = '?' + _url.split("?")[1];
      URL_parameter = _temp.split("#")[0];
    }


    onResize();
    $(window).resize(function () {
      //onResize();
      if (!isMobile) location.reload();
    });
  }

  function onResize() {
    if ($(window).width() <= 720) {
      isMobile = true;
    }
  }

  function initMainMenu() {
    TweenMax.set($('#main-nav'), { autoAlpha: 0 });

    $('.nav-close').click(function (e) {
      e.preventDefault();
      MainMenuClose();
    });

    $('.navbar-toggler').click(function (e) {
      e.preventDefault();
      TweenMax.to($('#main-nav'), 0.5, { autoAlpha: 1 });
    });

    $('a').each(function () {
      var _url = $(this).attr('href').split('#')[0];
      var _hash = $(this).attr('href').split('#')[1];

      if (_url != '' && _url != undefined) {
        if (_hash != '' && _hash != undefined) {
          _hash = '#' + _hash;
        } else {
          _hash = '';
        }

        if (URL_parameter != '' && URL_parameter != undefined) {
          $(this).attr('href', _url + URL_parameter + _hash);
        } else {
          $(this).attr('href', _url + _hash);
        }
      }
    });

    $('a[href*=#]').click(function (e) {
      // console.log(target.split('#'));

      var target = $(this).attr('href').split('#');

      //check IE
      if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
        if (target[1] != '') {
          $('html').animate({
            scrollTop: $('#' + target[1]).offset().top
          }, 1000);
        }
      } else {
        if (target[1] != '') {
          $('html,body').animate({
            scrollTop: $('#' + target[1]).offset().top
          }, 1000);
        }
      }

      MainMenuClose();
      e.preventDefault();
    });
  }

  function MainMenuClose() {
    TweenMax.to($('#main-nav'), 0.5, { autoAlpha: 0, onComplete: MainMenuHide });
  }

  function MainMenuHide() {
    $('#main-nav').collapse('hide');
  }

  //constructor

  {
    $(document).ready(function () {
      if ($(".header").html() == '') {
        $(".header").load("_header.html", function () {
          initMainMenu();
        });
      }
      if ($(".footer").html() == '') $(".footer").load("_footer.html");
      init();
    });
  }

  //public

  return {

  };
};

var main = new main();
