/**
 *
 * @authors Eric Hsiao
 *
 */

mainBanner = function () {

  //private menbers
 var mainBannerSwiper;

  //private methods
  function init() {
    console.log('mainBanner is loaded.');

   mainBannerSwiper = new Swiper('.main__banner__container', {
      loop: true,
      spaceBetween: 100,
      autoplay: {
        delay: 500000,
        disableOnInteraction: false,
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true
      }
    });

  }

  function onResize() {

    if (!isMobile) {
      var _dw = ($(window).width() / 1920);
      var _dh = ($(window).height() / 1000);
      TweenMax.set('.main__banner__container', { scale: _dh, transformOrigin: 'center center' });
     
      //$('.main__banner__container').css('width',$(window).width()*(1/_dh));
      //$('.main__banner__container').css('margin-left',$(window).width()*(1/_dh)*-0.5);

      //TweenMax.set('.container-fixed',{scale:_dh, transformOrigin:'center center'});

      //update banner title
      var _bannerWidth = 1920 * _dh;
      var _widthOffset = ((_bannerWidth - $(window).width()) * .5) * _dw;
      $('.main__banner__title_0').css('margin-left', _widthOffset + 'px');

      var _bannerHeight = $('.main__banner__container').height();
      var _bottomOffset = (_bannerHeight - $(window).height()) * .5 * _dh;

      $('.swiper-pagination').css('margin-bottom', (100 * _dh + _bottomOffset) + 'px');
    }else{

      var _dw = ($(window).width() / 720);
      var _bannerHeight = $('.main__banner__container').height();
      var _bottomOffset = 0;//_bannerHeight - 980*_dw;

      TweenMax.set($('.main__banner__title').find('img'), { scale: _dw*1.4});
      $('.main__banner__title').css('bottom', '12%');
      $('.swiper-pagination').css('bottom', '5%');
      
      //$('.swiper-pagination').css('margin-bottom', (_bottomOffset - $('.main__banner__title').height() - 50*_dw) + 'px');

      // $('.swiper-pagination').css('margin-bottom', (_bottomOffset) + 'px');
      //TweenMax.set('.main__banner__title', { scale: _dw, transformOrigin: 'center center' });

      $('.main__banner').css('max-height','calc(100% - 100px)');
    }

    if(mainBannerSwiper) mainBannerSwiper.update();
  }

  //constructor

  {
    $(document).ready(function () {
      init();
    });
  }

  //public

  return {
    onResize: function () {
      onResize();
    }
  };
};

var mainBanner = new mainBanner();
