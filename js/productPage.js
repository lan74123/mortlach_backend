/**
 *
 * @authors Eric Hsiao
 *
 */

productPage = function (){

	//private menbers


	//private methods
	function init () {
    console.log('productPage is loaded.');

    /*var swiper = new Swiper('.product-container', {
      slidesPerView: 3,
      spaceBetween: 30,
      pagination: {
        clickable: true,
      },
    });*/

    TweenMax.set($('.product-slide').find('img'),{scale:0.9});
    TweenMax.set($('.product-slide').find('.product-picture'),{scale:0.9,transformOrigin:'center 300px'});

    // TweenMax.set(('.product__effect'),{autoAlpha:0});
    $('.product__effect').css('display','block');
    // $('.product-slide').css('width','30%');

    $('.product-slide').mouseover(function () { 
      var _img = $(this).find('img');
      // TweenMax.to($(this),1,{css:{'width':'33%'},ease: Expo.easeOut});
      TweenMax.to(_img,1,{scale:1,ease: Expo.easeOut});     
      TweenMax.to($(this).find('.product__effect'),1,{delay:0.1,autoAlpha:1,ease: Expo.easeOut});
    });

    $('.product-slide').mouseout(function () { 
      var _img = $(this).find('img');
      // TweenMax.to($(this),1,{css:{'width':'30%'},ease: Expo.easeOut});
      TweenMax.to(_img,0.3,{scale:0.9});
      TweenMax.to($(this).find('.product__effect'),1,{delay:0.1,autoAlpha:0,ease: Expo.easeOut});
    });

    /*$('a').click(function (e) {
      var _url = $(this).attr('href');
      // console.log(_url);
      
      e.preventDefault();
      TweenMax.to('.loading', 0.3, { autoAlpha: 1 });
      setTimeout(function(){
        location.href = _url;
      }
      ,300);
    });*/

    intoPage();
  }
  
  function intoPage(){
    TweenMax.from('.product-box',3,{css:{'width':'80%'},delay:0.5,autoAlpha:0,scale:.8,ease: Expo.easeOut});
    TweenMax.to('.product__effect',1,{delay:1,autoAlpha:0});
  }

	//constructor

	{
		$(document).ready(function() {
			init();
		});
	}

	//public

	return {

	};
};

var productPage = new productPage();
