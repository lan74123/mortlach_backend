/**
 *
 * @authors Eric Hsiao
 *
 */

/* utlis */

// if (typeof console == "undefined") {
//     window.console = {
//         log: function () {}
//     };
// }

// window.console = {
//        log: function () {}
// };

function gtag_pageView(_key) {
  //ga('send', 'pageview', key);
  gtag('config', GTAG_TRACKING_ID, {
    'page_title': _key,
    'page_path': _key
  });

  console.log("gtag_pageView: " + _key);
}

function gtag_ButtonClick(_category, _key) {
  //ga('send', 'event', 'Button', 'Click', key);
  gtag('event', 'Click', { 'event_category': _category, 'event_label': _key });

  console.log("gtag_ButtonClick: " + _category + ', ' + _key);
}

function setDefault(_textbox, _value) { // depend on jQuery
  $(_textbox).val(_value).css({ opacity: .4 });
  $(_textbox).focus(
    function () {
      if ($(this).val() == _value) {
        $(this).val('').css({ opacity: 1 });
      }
    })
    .blur(function () {
      if ($(this).val() == '') {
        $(this).val(_value).css({ opacity: .4 });
      }
    });
}

function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function setParameterByName(name, value, url) {
  if (!url) url = window.location.href;
  var re = new RegExp("([?|&])" + name + "=.*?(&|$)", "i");
  separator = url.indexOf('?') !== -1 ? "&" : "?";
  if (url.match(re)) {
    return url.replace(re, '$1' + name + "=" + value + '$2');
  }
  else {
    return url + separator + name + "=" + value;
  }
}

function loadAnimate(_namespace, $container, _callback) {
  var $canvas, stage, exportRoot, anim_container, dom_overlay_container, fnStartAnimation;

    var key = Object.keys(this[_namespace].compositions)[0];
    var comp = this[_namespace].getComposition(key);
    var lib = comp.getLibrary();

    $canvas = $('<canvas></canvas>').attr({
      width: lib.properties.width,
      height: lib.properties.height
    }).appendTo($container);

    var loader = new createjs.LoadQueue(false);
    loader.addEventListener("fileload", function (evt) { handleFileLoad(evt, comp) });
    loader.addEventListener("complete", function (evt) { handleComplete(evt, comp) });
    loader.loadManifest(lib.properties.manifest);

  function handleFileLoad(evt, comp) {
    var images = comp.getImages();
    if (evt && (evt.item.type == "image")) { images[evt.item.id] = evt.result; }
  }
  function handleComplete(evt, comp) {
    var lib = comp.getLibrary();
    var ss = comp.getSpriteSheet();
    var queue = evt.target;
    var ssMetadata = lib.ssMetadata;
    for (i = 0; i < ssMetadata.length; i++) {
      ss[ssMetadata[i].name] = new createjs.SpriteSheet({ "images": [queue.getResult(ssMetadata[i].name)], "frames": ssMetadata[i].frames })
    }
    exportRoot = new lib[_namespace]();
    stage = new lib.Stage($canvas[0]);
    fnStartAnimation = function () {
      stage.addChild(exportRoot);
      createjs.Ticker.setFPS(lib.properties.fps);
      createjs.Ticker.addEventListener("tick", stage);
      exportRoot.stop();
      if(_callback != null){
        _callback(exportRoot);
      }
    }

    this[_namespace].compositionLoaded(lib.properties.id);
    fnStartAnimation();
  }
}


var isIE = false;

utlis = function () {

  //private menbers


  //private methods
  function init() {
    console.log('all is loaded.');
  }

  //constructor

  {
    if ($('html').is('.ie6, .ie7, .ie8')) {
      isIE = true;
      // alert('.ie6, .ie7, .ie8');
    }

    $(document).ready(function () {
      init();
    });
  }

  //public

  return {

  }
}

utlis = new utlis();