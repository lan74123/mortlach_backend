/**
 *
 * @authors Eric Hsiao
 *
 */

welcomePage = function (){

	//private menbers
  var isVideoEnd = false;

	//private methods
	function init () {
    console.log('welcome is loaded.');

    var _count = getParameterByName('pass');
    

    var video = document.getElementById('mainVideo');
    //使用事件监听方式捕捉事件
    video.addEventListener("timeupdate",function(){
        var timeDisplay;
      //用秒数来显示当前播放进度
      timeDisplay = Math.floor(video.currentTime);
      // console.log(Math.floor(video.currentTime))
        //当视频播放到 4s的时候做处理
        if( timeDisplay == 1){
          $('.video__container').css('background-image','url(./images/welcome/_' + _count + '.jpg');
        }
      if( timeDisplay == 7){
        if(!isVideoEnd){
          isVideoEnd = true;
          TweenMax.to('.video__loader',1,{delay:0.3,autoAlpha:0});
          
        }
      }

    },false);

    /*onResize();
    $(window).resize(function () {
      onResize();
    });*/
  }

  function onResize() {
    if($(window).width() < 640){
      var _scale = $(window).width()/640;
      TweenMax.set('.video__container',{scale:_scale});
    }    
  }

	//constructor

	{
		$(document).ready(function() {
			init();
		});
	}

	//public

	return {

	};
};

var welcomePage = new welcomePage();
